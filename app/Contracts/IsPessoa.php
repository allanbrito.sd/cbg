<?php

namespace App\Contracts;

interface IsPessoa
{
    public function isFisica();
    public function isJuridica();
    public function getNomeESobrenomeAttribute();
    public function getPrimeiroNomeAttribute();
    public function getSegundoNomeAttribute();
    public function getFotoAttribute();
    public function endereco();
    // public function enderecos();
    public function contato();
    public function usuario();
    public function dadosBancarios();
}
