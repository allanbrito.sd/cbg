<?php

namespace App\Enumerations;

use Config;

abstract class EnumerationAbstract
{
	private $collection;

	public function __construct(String $enum)
	{
		$this->collection = collect(Config::get('enums.' . $enum));
	}

	public function get($id)
	{
		return $this->all()->get($id);
	}

	public function all()
	{
		return $this->collection;
	}

	public function search($value)
	{
		return $this->all()->search($value);
	}
}
