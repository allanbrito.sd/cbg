<?php

namespace App\Enumerations;

class PessoaTipoEnum extends EnumerationAbstract
{
	public function __construct(String $enum = null)
	{
		parent::__construct('pessoa_tipo');
	}
}
