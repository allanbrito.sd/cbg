<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models;

class AtletaController extends Controller
{

    public function consultar()
    {
        $atletas = Models\Atleta::get();

        return view('atletas.consultar', compact('atletas'));
    }

    public function novo()
    {
        $estados = Models\Estado::all();
        $pessoaTipos = collect(Models\Pessoa::$tipo);

        return view('atletas.novo', compact('estados', 'pessoaTipos'));
    }

    public function visualizar(Models\Atleta $atleta)
    {
        return view('atletas.visualizar', compact('atleta'));
    }

    public function editar(Models\Atleta $atleta)
    {
        $estados = Models\Estado::all();
        $pessoaTipos = collect(Models\Pessoa::$tipo);
        return view('atletas.editar', compact('atleta', 'estados', 'pessoaTipos'));
    }

    public function cadastrar(Requests\FormularioAtleta $request)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {
            $pessoa = new Models\Pessoa($request->input('formulario.pessoa'));

            $endereco = Models\Endereco::create($request->input('formulario.endereco'));
            $pessoa->endereco()->associate($endereco);

            $contato = Models\Contato::create($request->input('formulario.contato'));
            $contato->nome = $pessoa->nome;
            $contato->save();

            $pessoa->contato()->associate($contato);

            $dadosBancarios = Models\DadosBancarios::create($request->input('formulario.dadosBancarios'));
            $dadosBancarios->save();
            $pessoa->dadosBancarios()->associate($dadosBancarios);
            $pessoa->save();

            $atleta = new Models\Atleta;
            $atleta->pessoa()->associate($pessoa);
            $atleta->save();

            $mensagens['success'] = ucfirst(__('atleta')) . ' cadastrado com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao cadastrar ' . __('atleta');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('atleta.visualizar', ['id' => $atleta->id])
            ->with('notifications', $mensagens);
    }

    public function atualizar(Requests\FormularioAtleta $request, Models\Atleta $atleta)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {

            $pessoa = $atleta->pessoa;
            $pessoa->fill($request->input('formulario.pessoa'));

            $endereco = Models\Endereco::find($request->input('formulario.endereco.id')) ?? new Models\Endereco;
            $endereco->fill($request->input('formulario.endereco'));
            $endereco->save();
            $pessoa->endereco()->associate($endereco);

            $contato = Models\Contato::find($request->input('formulario.contato.id')) ?? new Models\Contato;
            $contato->nome = $pessoa->nome;
            $contato->fill($request->input('formulario.contato'));
            $contato->save();
            $pessoa->contato()->associate($contato);

            $dadosBancarios = Models\DadosBancarios::find($request->input('formulario.dadosBancarios.id')) ?? new Models\DadosBancarios;
            $dadosBancarios->fill($request->input('formulario.dadosBancarios'));
            $dadosBancarios->save();
            $pessoa->dadosBancarios()->associate($dadosBancarios);
            $pessoa->save();

            $atleta->save();

            $mensagens['success'] = ucfirst(__('atleta')) . ' atualizado com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao atualizar ' . __('atleta');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('atleta.editar', ['id' => $atleta->id])
            ->with('notifications', $mensagens);
    }

    public function apagar(Models\Atleta $atleta)
    {
        $mensagens = [];
        try {
            $atleta->delete();
            $mensagens['success'] = ucfirst(__('atleta')) . ' apagado com sucesso!';
        } catch (Exception $e) {
            $mensagens['error'] = 'Falha ao apagar ' . __('atleta');
        }

        return redirect()->route('atleta.consultar')
            ->with('notifications', $mensagens);
    }

}
