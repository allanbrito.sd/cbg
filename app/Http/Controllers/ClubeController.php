<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models;

class ClubeController extends Controller
{

    public function consultar()
    {
        $clubes = Models\Clube::get();

        return view('clubes.consultar', compact('clubes'));
    }

    public function novo()
    {
        $estados = Models\Estado::all();
        $pessoaTipos = collect(Models\Pessoa::$tipo);

        return view('clubes.novo', compact('estados', 'pessoaTipos'));
    }

    public function visualizar(Models\Clube $clube)
    {
        return view('clubes.visualizar', compact('clube'));
    }

    public function editar(Models\Clube $clube)
    {
        $estados = Models\Estado::all();
        $pessoaTipos = collect(Models\Pessoa::$tipo);

        return view('clubes.editar', compact('clube', 'estados', 'pessoaTipos'));
    }

    public function cadastrar(Requests\FormularioClube $request)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {
            $pessoa = new Models\Pessoa($request->input('formulario.pessoa'));

            $endereco = Models\Endereco::create($request->input('formulario.endereco'));
            $pessoa->endereco()->associate($endereco);

            $contato = Models\Contato::create($request->input('formulario.contato'));
            $contato->nome = $pessoa->nome;
            $contato->save();

            $pessoa->contato()->associate($contato);

            $dadosBancarios = Models\DadosBancarios::create($request->input('formulario.dadosBancarios'));
            $dadosBancarios->save();
            $pessoa->dadosBancarios()->associate($dadosBancarios);
            $pessoa->save();

            $clube = new Models\Clube;
            $clube->pessoa()->associate($pessoa);
            $clube->save();

            $mensagens['success'] = ucfirst(__('clube')) . ' cadastrado com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao cadastrar ' . __('clube');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('clube.visualizar', ['id' => $clube->id])
            ->with('notifications', $mensagens);
    }

    public function atualizar(Requests\FormularioClube $request, Models\Clube $clube)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {
            $pessoa = $clube->pessoa;
            $pessoa->fill($request->input('formulario.pessoa'));

            $endereco = Models\Endereco::find($request->input('formulario.endereco.id')) ?? new Models\Endereco;
            $endereco->fill($request->input('formulario.endereco'));
            $endereco->save();
            $pessoa->endereco()->associate($endereco);

            $contato = Models\Contato::find($request->input('formulario.contato.id')) ?? new Models\Contato;
            $contato->nome = $pessoa->nome;
            $contato->fill($request->input('formulario.contato'));
            $contato->save();
            $pessoa->contato()->associate($contato);

            $dadosBancarios = Models\DadosBancarios::find($request->input('formulario.dadosBancarios.id')) ?? new Models\DadosBancarios;
            $dadosBancarios->fill($request->input('formulario.dadosBancarios'));
            $dadosBancarios->save();
            $pessoa->dadosBancarios()->associate($dadosBancarios);

            $pessoa->save();

            $clube->save();
            $mensagens['success'] = ucfirst(__('clube')) . ' atualizado com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao atualizar ' . __('clube');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('clube.editar', ['id' => $clube->id])
            ->with('notifications', $mensagens);
    }

    public function apagar(Models\Clube $clube)
    {
        $mensagens = [];
        try {
            $clube->delete();
            $mensagens['success'] = ucfirst(__('clube')) . ' apagado com sucesso!';
        } catch (Exception $e) {
            $mensagens['error'] = 'Falha ao apagar ' . __('clube');
        }

        return redirect()->route('clube.consultar')
            ->with('notifications', $mensagens);
    }

}
