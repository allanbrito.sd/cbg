<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models;

class ContaController extends Controller
{

    public function consultar()
    {
        $contas = Models\Conta::get();

        return view('contas.consultar', compact('contas'));
    }

    public function novo()
    {

        return view('contas.novo');
    }

    public function visualizar(Models\Conta $conta)
    {
        return view('contas.visualizar', compact('conta'));
    }

    public function editar(Models\Conta $conta)
    {
        return view('contas.editar', compact('conta'));
    }

    public function cadastrar(Requests\FormularioConta $request)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {
            $conta = new Models\Conta($request->input('formulario.conta'));

            $dadosBancarios = Models\DadosBancarios::find($request->input('formulario.dadosBancarios.id')) ?? new Models\DadosBancarios;
            $dadosBancarios->fill($request->input('formulario.dadosBancarios'));
            $dadosBancarios->save();
            $conta->dadosBancarios()->associate($dadosBancarios);

            $conta->save();

            $mensagens['success'] = ucfirst(__('modulos.conta')) . ' cadastrada com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao cadastrar ' . __('modulos.conta');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('conta.visualizar', ['id' => $conta->id])
            ->with('notifications', $mensagens);
    }

    public function atualizar(Requests\FormularioConta $request, Models\Conta $conta)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {
            $conta->fill($request->input('formulario.conta'));
 
            $dadosBancarios = Models\DadosBancarios::find($request->input('formulario.dadosBancarios.id')) ?? new Models\DadosBancarios;
            $dadosBancarios->fill($request->input('formulario.dadosBancarios'));
            $dadosBancarios->save();
            $conta->dadosBancarios()->associate($dadosBancarios);

            $conta->save();

            $mensagens['success'] = ucfirst(__('modulos.conta')) . ' atualizada com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao atualizar ' . __('modulos.conta');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('conta.editar', ['id' => $conta->id])
            ->with('notifications', $mensagens);
    }

    public function apagar(Models\Conta $conta)
    {
        $mensagens = [];
        try {
            $conta->delete();
            $mensagens['success'] = ucfirst(__('modulos.conta')) . ' apagada com sucesso!';
        } catch (Exception $e) {
            $mensagens['error'] = 'Falha ao apagar ' . __('modulos.conta');
        }

        return redirect()->route('conta.consultar')
            ->with('notifications', $mensagens);
    }

}
