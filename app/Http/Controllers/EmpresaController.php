<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models;

class EmpresaController extends Controller
{

	public function consultar()
	{
		$empresas = Models\Empresa::get();

		return view('empresas.consultar', compact('empresas'));
	}

	public function novo()
	{
		$estados = Models\Estado::all();
		$pessoaTipos = collect(Models\Pessoa::$tipo);

		return view('empresas.novo', compact('estados', 'pessoaTipos'));
	}

	public function visualizar(Models\Empresa $empresa)
	{
		return view('empresas.visualizar', compact('empresa'));
	}

	public function editar(Models\Empresa $empresa)
	{
		$estados = Models\Estado::all();
		$pessoaTipos = collect(Models\Pessoa::$tipo);

		return view('empresas.editar', compact('empresa', 'estados', 'pessoaTipos'));
	}

	public function cadastrar(Requests\FormularioEmpresa $request)
	{
		$mensagens = [];

		\DB::beginTransaction();
		try {
			$pessoa = new Models\Pessoa($request->input('formulario.pessoa'));

			if (!empty($request->input('formulario.endereco.logradouro'))) {
				$endereco = Models\Endereco::create($request->input('formulario.endereco'));
				$pessoa->endereco()->associate($endereco);
			}

			$pessoa->save();

			$empresa = new Models\Empresa;
			$empresa->pessoa()->associate($pessoa);

			$empresa->save();
			$mensagens['success'] = ucfirst(__('empresa')) . ' cadastrado com sucesso!';
		} catch (Exception $e) {
			\DB::rollBack();
			$mensagens['error'] = 'Falha ao cadastrar ' . __('empresa');
			throw $e;
		}

		\DB::commit();

		return redirect()->route('empresa.visualizar', ['id' => $empresa->id])
			->with('notifications', $mensagens);
	}

	public function atualizar(Requests\FormularioEmpresa $request, Models\Empresa $empresa)
	{
		$mensagens = [];

		\DB::beginTransaction();
		try {
			$pessoa = $empresa->pessoa;
			$pessoa->fill($request->input('formulario.pessoa'));

			if (!empty($request->input('formulario.endereco.logradouro'))) {
				$endereco = Models\Endereco::find($request->input('formulario.endereco.id')) ?? new Models\Endereco;
				$endereco->fill($request->input('formulario.endereco'));
				$endereco->save();
				$pessoa->endereco()->associate($endereco);
			}

			$pessoa->save();

			$empresa->save();
			$mensagens['success'] = ucfirst(__('empresa')) . ' atualizado com sucesso!';
		} catch (Exception $e) {
			\DB::rollBack();
			$mensagens['error'] = 'Falha ao atualizar ' . __('empresa');
			throw $e;
		}

		\DB::commit();

		return redirect()->route('empresa.editar', ['id' => $empresa->id])
			->with('notifications', $mensagens);
	}

	public function apagar(Models\Empresa $empresa)
	{
		$mensagens = [];
		try {
			$empresa->delete();
			$mensagens['success'] = ucfirst(__('empresa')) . ' apagado com sucesso!';
		} catch (Exception $e) {
			$mensagens['error'] = 'Falha ao apagar ' . __('empresa');
		}

		return redirect()->route('empresa.consultar')
			->with('notifications', $mensagens);
	}

}
