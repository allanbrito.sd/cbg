<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models;

class EventoController extends Controller
{

    public function consultar()
    {
        $eventos = Models\Evento::get();

        return view('eventos.consultar', compact('eventos'));
    }

    public function novo()
    {

        return view('eventos.novo');
    }

    public function visualizar(Models\Evento $evento)
    {
        return view('eventos.visualizar', compact('evento'));
    }

    public function editar(Models\Evento $evento)
    {
        return view('eventos.editar', compact('evento'));
    }

    public function cadastrar(Requests\FormularioEvento $request)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {
            $evento = new Models\Evento($request->input('formulario.evento'));
            $evento->save();

            $mensagens['success'] = ucfirst(__('modulos.evento')) . ' cadastrado com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao cadastrar ' . __('modulos.evento');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('evento.visualizar', ['id' => $evento->id])
            ->with('notifications', $mensagens);
    }

    public function atualizar(Requests\FormularioEvento $request, Models\Evento $evento)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {
            $evento->fill($request->input('formulario.evento'));
            $evento->save();

            $mensagens['success'] = ucfirst(__('modulos.evento')) . ' atualizado com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao atualizar ' . __('modulos.evento');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('evento.editar', ['id' => $evento->id])
            ->with('notifications', $mensagens);
    }

    public function apagar(Models\Evento $evento)
    {
        $mensagens = [];
        try {
            $evento->delete();
            $mensagens['success'] = ucfirst(__('modulos.evento')) . ' apagado com sucesso!';
        } catch (Exception $e) {
            $mensagens['error'] = 'Falha ao apagar ' . __('modulos.evento');
        }

        return redirect()->route('evento.consultar')
            ->with('notifications', $mensagens);
    }

}
