<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models;

class FormaPagamentoController extends Controller
{

    public function consultar()
    {
        $formasPagamento = Models\FormaPagamento::get();

        return view('formaPagamentos.consultar', compact('formasPagamento'));
    }

    public function novo()
    {

        return view('formaPagamentos.novo');
    }

    public function visualizar(Models\FormaPagamento $formaPagamento)
    {
        return view('formaPagamentos.visualizar', compact('formaPagamento'));
    }

    public function editar(Models\FormaPagamento $formaPagamento)
    {
        return view('formaPagamentos.editar', compact('formaPagamento'));
    }

    public function cadastrar(Requests\FormularioFormaPagamento $request)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {
            $formaPagamento = new Models\FormaPagamento($request->input('formulario.formaPagamento'));
            $formaPagamento->save();

            $mensagens['success'] = ucfirst(__('modulos.formaPagamento')) . ' cadastrada com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao cadastrar ' . __('modulos.formaPagamento');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('formaPagamento.visualizar', ['id' => $formaPagamento->id])
            ->with('notifications', $mensagens);
    }

    public function atualizar(Requests\FormularioFormaPagamento $request, Models\FormaPagamento $formaPagamento)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {
            $formaPagamento->fill($request->input('formulario.formaPagamento'));
            $formaPagamento->save();

            $mensagens['success'] = ucfirst(__('modulos.formaPagamento')) . ' atualizada com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao atualizar ' . __('modulos.formaPagamento');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('formaPagamento.editar', ['id' => $formaPagamento->id])
            ->with('notifications', $mensagens);
    }

    public function apagar(Models\FormaPagamento $formaPagamento)
    {
        $mensagens = [];
        try {
            $formaPagamento->delete();
            $mensagens['success'] = ucfirst(__('modulos.formaPagamento')) . ' apagada com sucesso!';
        } catch (Exception $e) {
            $mensagens['error'] = 'Falha ao apagar ' . __('modulos.formaPagamento');
        }

        return redirect()->route('formaPagamento.consultar')
            ->with('notifications', $mensagens);
    }

}
