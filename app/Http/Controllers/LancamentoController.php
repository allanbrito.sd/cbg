<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models;

class LancamentoController extends Controller
{

	public function consultar()
	{
		$lancamentos = Models\Lancamento::get();

		return view('lancamentos.consultar', compact('lancamentos'));
	}

	public function novo()
	{
		$estados = Models\Estado::all();
		$pessoaTipos = collect(Models\Pessoa::$tipo);
		$pessoas = Models\Pessoa::doesntHave('usuario')->with('atleta')->with('clube')->get();
		$contas = Models\Conta::all();
		$eventos = Models\Evento::all();
		$eventoItens = Models\EventoItem::all();
		$modalidades = Models\Modalidade::all();
		$planoContas = Models\PlanoContas::all();
		$formasPagamento = Models\FormaPagamento::all();
		$clubes = Models\Clube::all();
		$atletas = Models\Atleta::all();

		return view('lancamentos.novo', compact('estados', 'pessoaTipos', 'pessoas', 'contas', 'planoContas', 'eventos', 'eventoItens', 'modalidades', 'formasPagamento', 'clubes', 'atletas'));
	}

	public function visualizar(Models\Lancamento $lancamento)
	{
		return view('lancamentos.visualizar', compact('lancamento'));
	}

	public function editar(Models\Lancamento $lancamento)
	{
		// dd($lancamento->figLicenses);
		// dd($lancamento->eventoItens->pluck('evento_item_id'));
		$estados = Models\Estado::all();
		$pessoaTipos = collect(Models\Pessoa::$tipo);
		$pessoas = Models\Pessoa::doesntHave('usuario')->with('atleta')->with('clube')->get();
		$contas = Models\Conta::all();
		$clubes = Models\Clube::all();
		$atletas = Models\Atleta::all();
		$eventos = Models\Evento::all();
		$eventoItens = Models\EventoItem::all();
		$modalidades = Models\Modalidade::all();
		$planoContas = Models\PlanoContas::all();
		$formasPagamento = Models\FormaPagamento::all();

		return view('lancamentos.editar', compact('lancamento', 'estados', 'pessoaTipos', 'pessoas', 'contas', 'planoContas', 'eventos', 'eventoItens', 'modalidades', 'formasPagamento', 'atletas', 'clubes'));
	}

	public function cadastrar(Request $request)
	{
		$mensagens = [];

		\DB::beginTransaction();
		try {
			$notaFiscal = new Models\NotaFiscal($request->input('formulario.nota_fiscal'));
			$notaFiscal->save();


			$lancamento = new Models\Lancamento($request->input('formulario.lancamento'));
			$lancamento->credito = (bool) $request->input('formulario.lancamento.credito');
			$lancamento->notaFiscal()->associate($notaFiscal);
			
			$pessoaPagamento = Models\Pessoa::find($request->input('pagamentoPessoa.pessoa.id')) ?? new Models\pessoa;
	        $pessoaPagamento->fill($request->input('pagamentoPessoa.pessoa'));

            $endereco = Models\Endereco::find($request->input('pagamentoPessoa.endereco.id')) ?? new Models\Endereco;
            $endereco->fill($request->input('pagamentoPessoa.endereco'));
            $endereco->save();
            $pessoaPagamento->endereco()->associate($endereco);

            $contato = Models\Contato::find($request->input('pagamentoPessoa.contato.id')) ?? new Models\Contato;
            $contato->nome = $pessoaPagamento->nome;
            $contato->fill($request->input('pagamentoPessoa.contato'));
            $contato->save();
            $pessoaPagamento->contato()->associate($contato);

            $dadosBancarios = Models\DadosBancarios::find($request->input('pagamentoPessoa.dadosBancarios.id')) ?? new Models\DadosBancarios;
            $dadosBancarios->fill($request->input('pagamentoPessoa.dadosBancarios'));
            $dadosBancarios->save();
            $pessoaPagamento->dadosBancarios()->associate($dadosBancarios);

            $pessoaPagamento->save();
     
			if (true) {
		        $pessoaRecibo = $pessoaPagamento;
			} else {
				$pessoaPagamento = Models\Pessoa::find($request->input('reciboPessoa.pessoa.id')) ?? new Models\pessoa;
		        $pessoaPagamento->fill($request->input('reciboPessoa.pessoa'));

	            $endereco = Models\Endereco::find($request->input('reciboPessoa.endereco.id')) ?? new Models\Endereco;
	            $endereco->fill($request->input('reciboPessoa.endereco'));
	            $endereco->save();
	            $pessoaPagamento->endereco()->associate($endereco);

	            $contato = Models\Contato::find($request->input('reciboPessoa.contato.id')) ?? new Models\Contato;
	            $contato->nome = $pessoaPagamento->nome;
	            $contato->fill($request->input('reciboPessoa.contato'));
	            $contato->save();
	            $pessoaPagamento->contato()->associate($contato);

	            $dadosBancarios = Models\DadosBancarios::find($request->input('reciboPessoa.dadosBancarios.id')) ?? new Models\DadosBancarios;
	            $dadosBancarios->fill($request->input('reciboPessoa.dadosBancarios'));
	            $dadosBancarios->save();
	            $pessoaPagamento->dadosBancarios()->associate($dadosBancarios);

	            $pessoaPagamento->save();
			}
			
			$lancamento->pessoa()->associate($pessoaRecibo);
			$lancamento->save();
			
			$pagamento = new Models\Pagamento($request->input('formulario.pagamento'));
			$pagamento->pago = (bool) $request->input('formulario.pagamento.pago');
			$pagamento->devolvido = (bool) $request->input('formulario.pagamento.devolvido');
			$pagamento->lancamento()->associate($lancamento);
			$pagamento->pessoa()->associate($pessoaPagamento);
			$pagamento->save();

			$recibo = new Models\Recibo;
			$recibo->pagamento()->associate($pagamento);
			$recibo->save();

			Models\LancamentoEventoItem::where('lancamento_id', $lancamento->id)->whereNotIn('evento_item_id', (array) $request->input('formulario.lancamento_evento_itens'))->delete();
			if (!empty($request->input('formulario.lancamento_evento_itens'))) {
				foreach ($request->input('formulario.lancamento_evento_itens') as $eventoItemId) {
					$eventoItem = Models\LancamentoEventoItem::where('lancamento_id', $lancamento->id)->where('evento_item_id', $eventoItemId)->first() ?? new Models\LancamentoEventoItem;
					$eventoItem->evento_item_id = $eventoItemId;
					$eventoItem->lancamento()->associate($lancamento);
					$eventoItem->save();
				}
			}

			Models\Anuidade::where('lancamento_id', $lancamento->id)->whereNotIn('modalidade_id', (array) $request->input('formulario.anuidade.modalidade_id'))->delete();
			if (!empty($request->input('formulario.anuidade.modalidade_id'))) {
				foreach ($request->input('formulario.anuidade.modalidade_id') as $modalidadeId) {
					$anuidade = Models\Anuidade::where('lancamento_id', $lancamento->id)->where('modalidade_id', $modalidadeId)->first() ?? new Models\Anuidade;
					$anuidade->modalidade_id = $modalidadeId;
					$anuidade->ano = $request->input('formulario.anuidade.ano');
					$anuidade->lancamento()->associate($lancamento);
					$anuidade->save();
				}
			}

			Models\FigLicense::where('lancamento_id', $lancamento->id)->whereNotIn('id', collect($request->input('formulario.fig_license'))->pluck('id')->filter()->toArray())->delete();
			foreach ($request->input('formulario.fig_license') as $figLicense) {
				if (empty($figLicense['atleta_id']) || empty($figLicense['modalidade_id']) || empty($figLicense['clube_id'])) {
					continue;
				}
				$newFigLicense = Models\FigLicense::find($figLicense['id']) ?? new Models\FigLicense($figLicense);
				$newFigLicense->fill($figLicense);
				$newFigLicense->lancamento()->associate($lancamento);
				$newFigLicense->save();
			}

			Models\Transferencia::where('lancamento_id', $lancamento->id)->whereNotIn('id', collect($request->input('formulario.transferencia'))->pluck('id')->filter()->toArray())->delete();
			foreach ($request->input('formulario.transferencia') as $transferencia) {
				if (empty($transferencia['atleta_id']) || empty($transferencia['modalidade_id']) || empty($transferencia['clube_origem_id']) || empty($transferencia['clube_destino_id'])) {
					continue;
				}
				$newTransferencia = Models\Transferencia::find($transferencia['id']) ?? new Models\transferencia($transferencia);
				$newTransferencia->fill($transferencia);
				$newTransferencia->lancamento()->associate($lancamento);
				$newTransferencia->save();
			}

			$mensagens['success'] = ucfirst(__('lancamento')) . ' cadastrado com sucesso!';
		} catch (Exception $e) {
			\DB::rollBack();
			$mensagens['error'] = 'Falha ao cadastrar ' . __('lancamento');
			throw $e;
		}

		\DB::commit();

		return redirect()->route('lancamento.visualizar', ['id' => $lancamento->id])
			->with('notifications', $mensagens);
	}

	public function atualizar(Request $request, Models\Lancamento $lancamento)
	{

		$mensagens = [];

		\DB::beginTransaction();
		try {
			$notaFiscal = Models\NotaFiscal::find($request->input('formulario.nota_fiscal.id')) ?? new Models\NotaFiscal($request->input('formulario.nota_fiscal'));
			$notaFiscal->save();
			$lancamento = Models\Lancamento::find($request->input('formulario.lancamento.id')) ?? new Models\Lancamento($request->input('formulario.lancamento'));
			$lancamento->fill($request->input('formulario.lancamento'));
			$lancamento->credito = (bool) $request->input('formulario.lancamento.credito');
			$lancamento->notaFiscal()->associate($notaFiscal);
			$pessoaPagamento = Models\Pessoa::find($request->input('pagamentoPessoa.pessoa.id')) ?? new Models\pessoa;
	        $pessoaPagamento->fill($request->input('pagamentoPessoa.pessoa'));
	        $pessoaPagamento->save();
			if (true) {
		        $pessoaRecibo = $pessoaPagamento;
			} else {
				$pessoaRecibo = Models\Pessoa::find($request->input('reciboPessoa.pessoa.id')) ?? new Models\pessoa;
		        $pessoaRecibo->fill($request->input('reciboPessoa.pessoa'));
		        $pessoaRecibo->save();
			}
			
			$lancamento->pessoa()->associate($pessoaRecibo);
			$lancamento->save();

			
			$pagamento = Models\Pagamento::find($request->input('formulario.pagamento.id')) ?? new Models\Pagamento($request->input('formulario.pagamento'));
			$pagamento->fill($request->input('formulario.pagamento'));
			$pagamento->pago = (bool) $request->input('formulario.pagamento.pago');
			$pagamento->devolvido = (bool) $request->input('formulario.pagamento.devolvido');
			$pagamento->lancamento()->associate($lancamento);
			$pagamento->pessoa()->associate($pessoaPagamento);
			$pagamento->save();

			Models\LancamentoEventoItem::where('lancamento_id', $lancamento->id)->whereNotIn('evento_item_id', (array) $request->input('formulario.lancamento_evento_itens'))->delete();
			if (!empty($request->input('formulario.lancamento_evento_itens'))) {
				foreach ($request->input('formulario.lancamento_evento_itens') as $eventoItemId) {
					$eventoItem = Models\LancamentoEventoItem::where('lancamento_id', $lancamento->id)->where('evento_item_id', $eventoItemId)->first() ?? new Models\LancamentoEventoItem;
					$eventoItem->evento_item_id = $eventoItemId;
					$eventoItem->lancamento()->associate($lancamento);
					$eventoItem->save();
				}
			}

			Models\Anuidade::where('lancamento_id', $lancamento->id)->whereNotIn('modalidade_id', (array) $request->input('formulario.anuidade.modalidade_id'))->delete();
			if (!empty($request->input('formulario.anuidade.modalidade_id'))) {
				foreach ($request->input('formulario.anuidade.modalidade_id') as $modalidadeId) {
					$anuidade = Models\Anuidade::where('lancamento_id', $lancamento->id)->where('modalidade_id', $modalidadeId)->first() ?? new Models\Anuidade;
					$anuidade->modalidade_id = $modalidadeId;
					$anuidade->ano = $request->input('formulario.anuidade.ano');
					$anuidade->lancamento()->associate($lancamento);
					$anuidade->save();
				}
			}

			Models\FigLicense::where('lancamento_id', $lancamento->id)->whereNotIn('id', collect($request->input('formulario.fig_license'))->pluck('id')->filter()->toArray())->delete();
			foreach ($request->input('formulario.fig_license') as $figLicense) {
				if (empty($figLicense['atleta_id']) || empty($figLicense['modalidade_id']) || empty($figLicense['clube_id'])) {
					continue;
				}
				$newFigLicense = Models\FigLicense::find($figLicense['id']) ?? new Models\FigLicense($figLicense);
				$newFigLicense->fill($figLicense);
				$newFigLicense->lancamento()->associate($lancamento);
				$newFigLicense->save();
			}

			Models\Transferencia::where('lancamento_id', $lancamento->id)->whereNotIn('id', collect($request->input('formulario.transferencia'))->pluck('id')->filter()->toArray())->delete();
			foreach ($request->input('formulario.transferencia') as $transferencia) {
				if (empty($transferencia['atleta_id']) || empty($transferencia['modalidade_id']) || empty($transferencia['clube_origem_id']) || empty($transferencia['clube_destino_id'])) {
					continue;
				}
				$newTransferencia = Models\Transferencia::find($transferencia['id']) ?? new Models\transferencia($transferencia);
				$newTransferencia->fill($transferencia);
				$newTransferencia->lancamento()->associate($lancamento);
				$newTransferencia->save();
			}

			$mensagens['success'] = ucfirst(__('lancamento')) . ' atualizado com sucesso!';
		} catch (Exception $e) {
			\DB::rollBack();
			$mensagens['error'] = 'Falha ao atualizar ' . __('lancamento');
			throw $e;
		}

		\DB::commit();

		return redirect()->route('lancamento.editar', ['id' => $lancamento->id])
			->with('notifications', $mensagens);
	}

	public function apagar(Models\Lancamento $lancamento)
	{
		$mensagens = [];
		try {
			Models\Recibo::where('pagamento_id', $lancamento->pagamento->id)->delete();
			$lancamento->pagamento->delete();
			$lancamento->delete();
			$mensagens['success'] = ucfirst(__('lancamento')) . ' apagado com sucesso!';
		} catch (Exception $e) {
			$mensagens['error'] = 'Falha ao apagar ' . __('lancamento');
		}

		return redirect()->route('lancamento.consultar')
			->with('notifications', $mensagens);
	}

	public function figLicensePartial() {
		$clubes = Models\Clube::all();
		$atletas = Models\Atleta::all();
		$modalidades = Models\Modalidade::all();
		$prefixo = 'formulario';
		return view('lancamentos.partials.fig_license', compact('clubes', 'atletas', 'modalidades', 'prefixo'));
	}

	public function transferencia() {
		$clubes = Models\Clube::all();
		$atletas = Models\Atleta::all();
		$modalidades = Models\Modalidade::all();
		$prefixo = 'formulario';
		return view('lancamentos.partials.transferencia', compact('clubes', 'atletas', 'modalidades', 'prefixo'));
	}
}
