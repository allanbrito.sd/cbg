<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models;

class ModalidadeController extends Controller
{

    public function consultar()
    {
        $modalidades = Models\Modalidade::get();

        return view('modalidades.consultar', compact('modalidades'));
    }

    public function novo()
    {

        return view('modalidades.novo');
    }

    public function visualizar(Models\Modalidade $modalidade)
    {
        return view('modalidades.visualizar', compact('modalidade'));
    }

    public function editar(Models\Modalidade $modalidade)
    {
        return view('modalidades.editar', compact('modalidade'));
    }

    public function cadastrar(Requests\FormularioModalidade $request)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {
            $modalidade = new Models\Modalidade($request->input('formulario.modalidade'));
            $modalidade->save();

            $mensagens['success'] = ucfirst(__('modulos.modalidade')) . ' cadastrada com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao cadastrar ' . __('modulos.modalidade');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('modalidade.visualizar', ['id' => $modalidade->id])
            ->with('notifications', $mensagens);
    }

    public function atualizar(Requests\FormularioModalidade $request, Models\Modalidade $modalidade)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {
            $modalidade->fill($request->input('formulario.modalidade'));
            $modalidade->save();

            $mensagens['success'] = ucfirst(__('modulos.modalidade')) . ' atualizada com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao atualizar ' . __('modulos.modalidade');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('modalidade.editar', ['id' => $modalidade->id])
            ->with('notifications', $mensagens);
    }

    public function apagar(Models\Modalidade $modalidade)
    {
        $mensagens = [];
        try {
            $modalidade->delete();
            $mensagens['success'] = ucfirst(__('modulos.modalidade')) . ' apagada com sucesso!';
        } catch (Exception $e) {
            $mensagens['error'] = 'Falha ao apagar ' . __('modulos.modalidade');
        }

        return redirect()->route('modalidade.consultar')
            ->with('notifications', $mensagens);
    }

}
