<?php

namespace App\Http\Controllers;

class NavbarController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function collapse()
	{
		return response('')->withCookie(cookie('navbar_collapsed', true));
	}

	public function expand()
	{
		return response('')->withCookie(cookie('navbar_collapsed', false));
	}
}
