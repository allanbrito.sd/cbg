<?php

namespace App\Http\Controllers;

use App\Models;
use Illuminate\Http\Request;

class PedidoController extends Controller
{

	public function consultar()
	{
		$pedidos = Models\Pedido::all();

		return view('pedidos.consultar', ['pedidos' => $pedidos]);
	}

	public function novo()
	{
		// $vendedor = new Models\Vendedor;
		// $vendedor->pessoa()->associate(Auth()->user()->pessoa);
		// $vendedor->save();

		// dd(Auth()->user()->pessoa->vendedor);
		$estados = Models\Estado::all();
		$clientes = Models\Cliente::all();
		$representadas = Models\Representada::all();
		$vendedores = Models\Vendedor::all();
		$tabelasDePreco = Models\TabelaPreco::all();
		$pedidoTipos = collect(Models\Pedido::$tipo);

		return view('pedidos.novo',
			compact('estados', 'clientes', 'representadas', 'vendedores', 'tabelasDePreco', 'pedidoTipos'));
	}

	public function visualizar(Models\Pedido $pedido)
	{
		return view('pedidos.visualizar', compact('pedido'));
	}

	public function editar(Models\Pedido $pedido, Request $request)
	{
		$estados = Models\Estado::all();

		return view('pedidos.editar', compact('pedido', 'estados'));
	}

	public function cadastrar(Request $request)
	{
		$mensagens = [];

		\DB::beginTransaction();
		try {
			$pessoa = new Models\Pessoa($request->input('pedido.pessoa'));

			if (!empty($request->input('pedido.endereco.logradouro'))) {
				$endereco = Models\Endereco::create($request->input('pedido.endereco'));

				$pessoa->endereco()->associate($endereco);
			}

			$pessoa->save();

			$pedido = new Models\Pedido;
			$pedido->pessoa()->associate($pessoa);

			$pedido->save();
			$mensagens['success'] = ucfirst(__('pedido')) . ' cadastrada com sucesso!';
		} catch (Exception $e) {
			\DB::rollBack();
			$mensagens['error'] = 'Falha ao cadastrar ' . __('pedido');
			throw $e;
		}

		\DB::commit();

		return redirect()->route('pedido.visualizar', ['id' => $pedido->id])
			->with('notifications', $mensagens);
	}

	public function atualizar(Request $request, Models\Pedido $pedido)
	{
		$mensagens = [];

		\DB::beginTransaction();
		try {
			$pessoa = $pedido->pessoa;
			$pessoa->fill($request->input('pedido.pessoa'));

			if (!empty($request->input('pedido.endereco.logradouro'))) {
				$endereco = Models\Endereco::find($request->input('pedido.endereco.id')) ?? new Models\Endereco;
				$endereco->fill($request->input('pedido.endereco'));
				$endereco->save();

				$pessoa->endereco()->associate($endereco);
			}

			$pessoa->save();

			$pedido->save();

			$mensagens['success'] = ucfirst(__('pedido')) . ' atualizada com sucesso!';
		} catch (Exception $e) {
			\DB::rollBack();
			$mensagens['error'] = 'Falha ao atualizar ' . __('pedido');
			throw $e;
		}

		\DB::commit();

		return redirect()->route('pedido.editar', ['id' => $pedido->id])
			->with('notifications', $mensagens);
	}

	public function apagar(Models\Pedido $pedido)
	{
		$mensagens = [];

		try {
			$pedido->delete();
			$mensagens['success'] = ucfirst(__('pedido')) . ' apagada com sucesso!';
		} catch (Exception $e) {
			$mensagens['error'] = 'Falha ao apagar ' . __('pedido');
		}

		return redirect()->route('pedido.consultar')
			->with('notifications', $mensagens);
	}

}
