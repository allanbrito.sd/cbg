<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models;

class PessoaController extends Controller
{

    public function consultar()
    {
        $pessoas = Models\Pessoa::avulsas()->get();

        $pessoasFisicas = $pessoas->filter(function ($pessoa, $key) {
            $pessoaTipos = collect(Models\Pessoa::$tipo);

            return $pessoa->tipo == $pessoaTipos->search('Física');
        });

        $pessoasJuridicas = $pessoas->filter(function ($pessoa, $key) {
            $pessoaTipos = collect(Models\Pessoa::$tipo);

            return $pessoa->tipo == $pessoaTipos->search('Jurídica');
        });

        return view('pessoas.consultar', compact('pessoasFisicas', 'pessoasJuridicas'));
    }

    public function novo()
    {
        $estados = Models\Estado::all();
        $pessoaTipos = collect(Models\Pessoa::$tipo);

        return view('pessoas.novo', compact('estados', 'pessoaTipos'));
    }

    public function visualizar($id)
    {
        $pessoa = Models\Pessoa::avulsas()->find($id);
        return view('pessoas.visualizar', compact('pessoa'));
    }

    public function editar($id)
    {
        $pessoa = Models\Pessoa::avulsas()->find($id);
        if (empty($pessoa)) {
            abort(404);
        }
        $estados = Models\Estado::all();
        $pessoaTipos = collect(Models\Pessoa::$tipo);

        return view('pessoas.editar', compact('pessoa', 'estados', 'pessoaTipos'));
    }

    public function cadastrar(Requests\FormularioPessoa $request)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {
            $pessoa = new Models\Pessoa($request->input('formulario.pessoa'));

            $endereco = Models\Endereco::create($request->input('formulario.endereco'));
            $pessoa->endereco()->associate($endereco);

            $contato = Models\Contato::create($request->input('formulario.contato'));
            $contato->nome = $pessoa->nome;
            $contato->save();
            $pessoa->contato()->associate($contato);

            $dadosBancarios = Models\DadosBancarios::create($request->input('formulario.dadosBancarios'));
            $pessoa->dadosBancarios()->associate($dadosBancarios);
            $dadosBancarios->save();

            $pessoa->save();

            $mensagens['success'] = ucfirst(__('pessoa')) . ' cadastrado com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao cadastrar ' . __('pessoa');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('pessoa.visualizar', ['id' => $pessoa->id])
            ->with('notifications', $mensagens);
    }

    public function atualizar(Requests\FormularioPessoa $request, Models\Pessoa $pessoa)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {
            $pessoa->fill($request->input('formulario.pessoa'));

            $endereco = Models\Endereco::find($request->input('formulario.endereco.id')) ?? new Models\Endereco;
            $endereco->fill($request->input('formulario.endereco'));
            $endereco->save();
            $pessoa->endereco()->associate($endereco);

            $contato = Models\Contato::find($request->input('formulario.contato.id')) ?? new Models\Contato;
            $contato->nome = $pessoa->nome;
            $contato->fill($request->input('formulario.contato'));
            $contato->save();
            $pessoa->contato()->associate($contato);

            $dadosBancarios = Models\DadosBancarios::find($request->input('formulario.dadosBancarios.id')) ?? new Models\DadosBancarios;
            $dadosBancarios->fill($request->input('formulario.dadosBancarios'));
            $dadosBancarios->save();
            $pessoa->dadosBancarios()->associate($dadosBancarios);

            $pessoa->save();
            $mensagens['success'] = ucfirst(__('pessoa')) . ' atualizado com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao atualizar ' . __('pessoa');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('pessoa.editar', ['id' => $pessoa->id])
            ->with('notifications', $mensagens);
    }

    public function apagar(Models\Pessoa $pessoa)
    {
        $mensagens = [];
        try {
            $pessoa->delete();
            $mensagens['success'] = ucfirst(__('pessoa')) . ' apagado com sucesso!';
        } catch (Exception $e) {
            $mensagens['error'] = 'Falha ao apagar ' . __('pessoa');
        }

        return redirect()->route('pessoa.consultar')
            ->with('notifications', $mensagens);
    }

    public function carregarFormulario(Models\Pessoa $pessoa, $prefixo = null) {
        $estados = Models\Estado::all();
        $pessoaTipos = collect(Models\Pessoa::$tipo);

        return view('pessoas.partials.carregarFormulario', compact('estados', 'pessoaTipos', 'pessoa', 'prefixo'));
    }
}
