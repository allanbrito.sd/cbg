<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models;

class PlanoContaController extends Controller
{

    public function consultar()
    {
        $planoContas = Models\PlanoContas::get();

        return view('planoContas.consultar', compact('planoContas'));
    }

    public function novo()
    {

        return view('planoContas.novo');
    }

    public function visualizar(Models\PlanoContas $planoConta)
    {
        return view('planoContas.visualizar', compact('planoConta'));
    }

    public function editar(Models\PlanoContas $planoConta)
    {
        return view('planoContas.editar', compact('planoConta'));
    }

    public function cadastrar(Requests\FormularioPlanoContas $request)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {
            $planoConta = new Models\PlanoContas($request->input('formulario.planoConta'));
            $planoConta->save();

            $mensagens['success'] = ucfirst(__('modulos.planoConta')) . ' cadastradoa com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao cadastrar ' . __('modulos.planoConta');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('planoConta.visualizar', ['id' => $planoConta->id])
            ->with('notifications', $mensagens);
    }

    public function atualizar(Requests\FormularioPlanoContas $request, Models\PlanoContas $planoConta)
    {
        $mensagens = [];

        \DB::beginTransaction();
        try {
            $planoConta->fill($request->input('formulario.planoConta'));
            $planoConta->save();

            $mensagens['success'] = ucfirst(__('modulos.planoConta')) . ' atualizado com sucesso!';
        } catch (Exception $e) {
            \DB::rollBack();
            $mensagens['error'] = 'Falha ao atualizar ' . __('modulos.planoConta');
            throw $e;
        }

        \DB::commit();

        return redirect()->route('planoConta.editar', ['id' => $planoConta->id])
            ->with('notifications', $mensagens);
    }

    public function apagar(Models\PlanoContas $planoConta)
    {
        $mensagens = [];
        try {
            $planoConta->delete();
            $mensagens['success'] = ucfirst(__('modulos.planoConta')) . ' apagado com sucesso!';
        } catch (Exception $e) {
            $mensagens['error'] = 'Falha ao apagar ' . __('modulos.planoConta');
        }

        return redirect()->route('planoConta.consultar')
            ->with('notifications', $mensagens);
    }

}
