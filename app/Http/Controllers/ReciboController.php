<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models;

class ReciboController extends Controller
{

	public function consultar()
	{
		$recibos = Models\Recibo::get();

		return view('recibos.consultar', compact('recibos'));
	}

	public function novo()
	{
		$estados = Models\Estado::all();
		$pessoaTipos = collect(Models\Pessoa::$tipo);
		$pessoas = Models\Pessoa::doesntHave('usuario')->with('atleta')->with('clube')->get();
		$contas = Models\Conta::all();
		$eventos = Models\Evento::all();
		$modalidades = Models\Modalidade::all();
		$planoContas = Models\PlanoContas::all();
		$formasPagamento = Models\FormaPagamento::all();
		return view('recibos.novo', compact('estados', 'pessoaTipos', 'pessoas', 'contas', 'planoContas', 'eventos', 'modalidades', 'formasPagamento'));
	}

	public function cancelar(Models\Recibo $recibo) {
		if (empty($recibo->gerado_em) || !empty($recibo->cancelado_em)) {
			return;
		}

		$recibo->cancelado_em = date('Y-m-d H:i:s');
		$recibo->save();

		$novoRecibo = new Models\Recibo;
		$novoRecibo->pagamento()->associate($recibo->pagamento);
		$novoRecibo->save();
	}

	public function gerar(Models\Recibo $recibo) {
		if (!empty($recibo->gerado_em)) {
			return;
		}

		$ultimoRecibo = Models\Recibo::where('ano', date('y'))->orderBy('numero', 'desc')->first();
		
		Models\Recibo::where('pagamento_id', $recibo->pagamento_id)->whereNull('cancelado_em')->where('id', '!=', $recibo->id)->update(['cancelado_em' => date('Y-m-d H:i:s')]);
		$recibo->numero = ($ultimoRecibo->numero ?? 0) + 1;
		$recibo->ano = date('y');
		$recibo->codigo = substr(md5(uniqid(mt_rand(), true)) , 0, 6);
		$recibo->gerado_em = $recibo->gerado_em ?? date('Y-m-d H:i:s');
		$recibo->save();
	}

	public function visualizar(Models\Recibo $recibo)
	{	
// 		$recibo  = (object) array(
// 			'data' => date('d/m/Y'),
// 			'codigo' => str_pad(2, 4, 0, STR_PAD_LEFT).'/'.date('y'),
// 			'verificacao' => 'G8WDF',
// 			'valor' => 'R$ 2.930,00',
// 			'referente' => 'Ao Campeonato Brasileiro de Ginástica Artítica, categoria Infantil, realizado de 20 a 23 de junho de 2018
// em São Paulo - SP

// √ Cadastro ginastas / técnicos
// √ Recadastro ginastas / técnicos
// √ Inscrição
// √ Arbitragem

// Clube de Ginástica Estrela Dourada',
// 		);

		$empresa = (object) array(
			'nome' => 'Confederação Brasileira de Ginástica',
			'cnpj' => '37.160.348/0001-56',
			'inscricao_municipal' => '081201-7',
			'inscricao_estadual' => 'ISENTA',
			'contato' => (object) array(
				'telefones' => '(79) 3211-1206 / 3211-1207',
				'site' => 'www.cbginastica.com.br',
			),
			'endereco' => (object) array(
				'cep' => '49.050-240',
				'cidade' => 'Aracaju',
				'estado' => 'Sergipe',
				'logradouro' => 'Avenida Doutor Edézio Vieira de Melo, 419',
			),
		);
		// $pessoa = (object) array(
		// 	'nome' => 'CLUBE DE GINÁSTICA ESTRELA DOURADA - Prefeitura Municipal de Estrela Dourada - AC - Empenho numero 564.586.23/18.',
		// 	'cnpj' => '23.756.892/0001-44',
		// 	'contato' => (object) array(
		// 		'telefones' => '(79) 3211-1206 / 3211-1207',
		// 		'email' => 'financeiro@estreladourada.com.br',
		// 	),
		// 	'endereco' => (object) array(
		// 		'cep' => '49.050-240',
		// 		'cidade' => 'Aracaju',
		// 		'estado' => 'Sergipe',
		// 		'logradouro' => 'Rua Teixeira Soares de Campo, 1.253',
		// 		'complemento' => 'Galpão II',
		// 	),
		// );
		
		$ultimoRecibo = Models\Recibo::where('ano', date('y'))->orderBy('numero', 'desc')->first();
		$recibo->numero = !empty($recibo->numero) ? $recibo->numero : (($ultimoRecibo->numero ?? 0) + 1);
		$recibo->ano = !empty($recibo->ano) ? $recibo->ano : date('y');
		$recibo->codigo = !empty($recibo->codigo) ? $recibo->codigo : '??????';

		ob_start();
		echo view('recibos.recibo', compact('recibo', 'empresa'));
		$html = ob_get_clean();
		$pdf = \PDF::loadHTML($html);
		$pdf->setOption('disable-smart-shrinking', true);

		return $pdf->inline();

	}

	public function editar(Models\Recibo $recibo)
	{
		$estados = Models\Estado::all();
		$pessoaTipos = collect(Models\Pessoa::$tipo);

		return view('recibos.editar', compact('recibo', 'estados', 'pessoaTipos'));
	}

	public function cadastrar(Request $request)
	{
		
		$mensagens = [];

		\DB::beginTransaction();
		try {
			$notaFiscal = new Models\NotaFiscal($request->input('formulario.nota_fiscal'));
			$notaFiscal->save();


			$recibo = new Models\Recibo($request->input('formulario.recibo'));
			$recibo->credito = (bool) $request->input('formulario.recibo.credito');
			$recibo->notaFiscal()->associate($notaFiscal);
			
			$pessoa = Models\Pessoa::find($request->input('formulario.pessoa.id')) ?? new Models\pessoa;
	        $pessoa->fill($request->input('formulario.pessoa'));
	        $pessoa->save();
			
			$recibo->pessoa()->associate($pessoa);
			$recibo->save();
			
			$pagamento = new Models\Pagamento($request->input('formulario.pagamento'));
			$pagamento->pago = (bool) $request->input('formulario.pagamento.pago');
			$pagamento->devolvido = (bool) $request->input('formulario.pagamento.devolvido');
			$pagamento->recibo()->associate($recibo);
			$pagamento->pessoa()->associate($pessoa);
			$pagamento->save();

			$mensagens['success'] = ucfirst(__('recibo')) . ' cadastrado com sucesso!';
		} catch (Exception $e) {
			\DB::rollBack();
			$mensagens['error'] = 'Falha ao cadastrar ' . __('recibo');
			throw $e;
		}

		\DB::commit();

		return redirect()->route('recibo.visualizar', ['id' => $recibo->id])
			->with('notifications', $mensagens);
	}

	public function atualizar(Request $request, Models\Recibo $recibo)
	{
		$mensagens = [];

		\DB::beginTransaction();
		try {
			$pessoa = $recibo->pessoa;
			$pessoa->fill($request->input('formulario.pessoa'));

			if (!empty($request->input('formulario.endereco.logradouro'))) {
				$endereco = Models\Endereco::find($request->input('formulario.endereco.id')) ?? new Models\Endereco;
				$endereco->fill($request->input('formulario.endereco'));
				$endereco->save();
				$pessoa->endereco()->associate($endereco);
			}

			$pessoa->save();

			$recibo->save();
			$mensagens['success'] = ucfirst(__('recibo')) . ' atualizado com sucesso!';
		} catch (Exception $e) {
			\DB::rollBack();
			$mensagens['error'] = 'Falha ao atualizar ' . __('recibo');
			throw $e;
		}

		\DB::commit();

		return redirect()->route('recibo.editar', ['id' => $recibo->id])
			->with('notifications', $mensagens);
	}

	public function apagar(Models\Recibo $recibo)
	{
		$mensagens = [];
		try {
			$recibo->delete();
			$mensagens['success'] = ucfirst(__('recibo')) . ' apagado com sucesso!';
		} catch (Exception $e) {
			$mensagens['error'] = 'Falha ao apagar ' . __('recibo');
		}

		return redirect()->route('recibo.consultar')
			->with('notifications', $mensagens);
	}

}
