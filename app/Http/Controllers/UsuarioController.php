<?php

namespace App\Http\Controllers;

use App\Models;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function consultar()
	{
		// $this->usuario->pushCriteria(new OrderByMaisRecentes);

		$usuarios = Models\Usuario::latest()->get();

		return view('usuarios.consultar', ['usuarios' => $usuarios]);
	}

	public function novo()
	{
		$estados = Models\Estado::all();

		return view('usuarios.novo', compact('estados'));
	}

	public function visualizar(Models\Usuario $usuario)
	{
		return view('usuarios.visualizar', compact('usuario'));
	}

	public function editar(Models\Usuario $usuario)
	{
		$estados = Models\Estado::all();

		return view('usuarios.editar', compact('usuario', 'estados'));
	}

	public function cadastrar(Request $request)
	{
		\DB::beginTransaction();

		try {
			$pessoa = Models\Pessoa::create($request->input('vendedor.pessoa'));

			$endereco = new Models\Endereco($request->input('vendedor.endereco'));
			$endereco->pessoa()->associate($pessoa);
			$endereco->save();

			$usuario = new Models\Usuario(['email' => "allan@email2.com", 'password' => '123456']);
			$usuario->pessoa()->associate($pessoa);
			$usuario->save();
		} catch (Exception $e) {
			\DB::rollBack();
			throw $e;
		}

		\DB::commit();

		return redirect()->route('usuario_visualizar', ['id' => $usuario->id])->with('message',
			'Usuario created successfully!');
	}

	public function atualizar(Request $request, Models\Usuario $usuario)
	{
		\DB::beginTransaction();
		try {
			$pessoa = $usuario->pessoa;
			$pessoa->fill($request->input('vendedor.pessoa'));
			$pessoa->save();

			$endereco = $usuario->pessoa->endereco;
			$endereco->fill($request->input('vendedor.endereco'));
			$endereco->save();

			$usuario->save();

		} catch (Exception $e) {
			\DB::rollBack();
			throw $e;
		}

		\DB::commit();

		return redirect()->route('usuario_editar', ['id' => $usuario->id])->with('message',
			'Usuario updated successfully!');
	}

	public function apagar(Models\Usuario $usuario)
	{
		$usuario->delete();

		return redirect()->route('usuarios')->with('alert-success', 'Usuario hasbeen deleted!');
	}

}
