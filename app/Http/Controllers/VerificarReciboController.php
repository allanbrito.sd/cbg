<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models;
use Illuminate\Routing\Controller as BaseController;


class VerificarReciboController extends BaseController
{
	public function verificar()
	{
        return view('recibos.verificar');
	}

	public function validar(Request $request)
	{	
		$recibo = Models\Recibo::where('codigo', $request->input('codigo'))->first();
		if (empty($recibo) || $request->input('numero') != str_pad($recibo->numero, 4, STR_PAD_LEFT, '0').'/'.$recibo->ano) {
            $mensagens['error'] = 'Recibo não encontrado';
			return redirect()->route('recibo.verificar')->with('notifications', $mensagens);
		}

		// if (preg_replace('/\D/', '', $recibo->pagamento->pessoa->cpf) != $request->input('cpfcnpj') && preg_replace('/\D/', '', $recibo->pagamento->pessoa->cnpj) != $request->input('cpfcnpj')) {
  //           $mensagens['error'] = 'Recibo não encontrado';
		// 	return redirect()->route('recibo.verificar')->with('notifications', $mensagens);
		// }

		$empresa = (object) array(
			'nome' => 'Confederação Brasileira de Ginástica',
			'cnpj' => '37.160.348/0001-56',
			'inscricao_municipal' => '081201-7',
			'inscricao_estadual' => 'ISENTA',
			'contato' => (object) array(
				'telefones' => '(79) 3211-1206 / 3211-1207',
				'site' => 'www.cbginastica.com.br',
			),
			'endereco' => (object) array(
				'cep' => '49.050-240',
				'cidade' => 'Aracaju',
				'estado' => 'Sergipe',
				'logradouro' => 'Avenida Doutor Edézio Vieira de Melo, 419',
			),
		);

		$ultimoRecibo = Models\Recibo::where('ano', date('y'))->orderBy('numero', 'desc')->first();
		$recibo->numero = !empty($recibo->numero) ? $recibo->numero : (($ultimoRecibo->numero ?? 0) + 1);
		$recibo->ano = !empty($recibo->ano) ? $recibo->ano : date('y');
		$recibo->codigo = !empty($recibo->codigo) ? $recibo->codigo : '??????';

		$dataVisualizacao = true;

		ob_start();
		echo view('recibos.recibo', compact('recibo', 'empresa', 'dataVisualizacao'));
		$html = ob_get_clean();

		$pdf = \PDF::loadHTML($html);
		return $pdf->inline();
	}
}
