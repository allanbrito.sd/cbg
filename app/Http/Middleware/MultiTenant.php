<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Tenant;

class MultiTenant
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */

	public function handle($request, Closure $next)
	{
		$tenant = Auth::user()->empresas;

		if ($tenant->count() == 0) {
			return redirect()->route('equipes.novo');
		}

		Tenant::addTenant('empresa_id', $tenant->first()->id);

		return $next($request);
	}
}