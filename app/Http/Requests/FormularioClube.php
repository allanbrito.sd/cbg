<?php

namespace App\Http\Requests;

use App\Models;
use Illuminate\Foundation\Http\FormRequest;

class FormularioClube extends FormRequest
{
	private $regras = [];

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$this->addRegrasPessoa();
		$this->addRegrasEndereco();

		return $this->regras;
	}

	private function addRegrasPessoa()
	{
		$regrasPessoa = Models\Pessoa::validationRules();

		foreach ($regrasPessoa as $campo => $regra) {
			$this->regras['formulario.pessoa.' . $campo] = $regra;
		}
	}

	private function addRegrasEndereco()
	{
		$regrasEndereco = Models\Endereco::validationRules();

		foreach ($regrasEndereco as $campo => $regra) {
			$this->regras['formulario.endereco.' . $campo] = $regra;
		}
	}

}
