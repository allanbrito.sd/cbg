<?php

namespace App\Http\Requests;

use App\Models;
use Illuminate\Foundation\Http\FormRequest;

class FormularioConta extends FormRequest
{
	private $regras = [];

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$this->regras['formulario.conta.nome'] = 'required';

		return $this->regras;
	}

}
