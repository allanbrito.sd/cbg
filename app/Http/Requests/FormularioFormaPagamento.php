<?php

namespace App\Http\Requests;

use App\Models;
use Illuminate\Foundation\Http\FormRequest;

class FormularioFormaPagamento extends FormRequest
{
	private $regras = [];

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$this->regras['formulario.formaPagamento.nome'] = 'required';

		return $this->regras;
	}

}
