<?php

namespace App\Http\Requests;

use App\Models;
use Illuminate\Foundation\Http\FormRequest;

class FormularioPlanoContas extends FormRequest
{
	private $regras = [];

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$this->regras['formulario.planoConta.nome'] = 'required';
		$this->regras['formulario.planoConta.codigo'] = 'required';

		return $this->regras;
	}

}
