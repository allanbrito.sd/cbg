<?php

namespace App\Http\Requests;

use App\Models;
use Illuminate\Foundation\Http\FormRequest;

class FormularioProduto extends FormRequest
{
	private $regras = [];

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$this->addRegrasProduto();
		$this->addRegrasTabelaPreco();


		return $this->regras;
	}

	private function addRegrasProduto()
	{
		$regrasProduto = Models\Produto::validationRules();

		foreach ($regrasProduto as $campo => $regra) {
			$this->regras['formulario.produto.' . $campo] = $regra;
		}
	}

	private function addRegrasTabelaPreco()
	{
		$regrasTabelaPreco = Models\ProdutoTabelaPreco::validationRules();

		foreach ($regrasTabelaPreco as $campo => $regra) {
			$this->regras['formulario.tabelasDePreco.*.' . $campo] = $regra;
		}
	}

	public function save(Models\Produto $produto): Models\Produto
	{
		$produto->fill($this->input('formulario.produto'));

		if (!empty($this->input('formulario.estoqueConfiguracao'))) {
			$estoqueConfiguracao = Models\EstoqueConfiguracao::firstOrCreate($this->input('formulario.estoqueConfiguracao'));

			$produtoEstoqueConfiguracao = Models\ProdutoEstoqueConfiguracao::findOrNew($this->input('formulario.produto.estoque_configuracao_id'));
			$produtoEstoqueConfiguracao->configuracao()->associate($estoqueConfiguracao);
			$produtoEstoqueConfiguracao->save();

			$produto->estoque()->associate($produtoEstoqueConfiguracao);
		}

		$produto->save();

		$tabelasPreco = collect($this->input('formulario.tabelasDePreco'));
		$produto->tabelasPreco()->sync($tabelasPreco->keys()->all());

		foreach ($produto->tabelasPreco as $tabelaPreco) {
			$tabelaPreco->pivot->valor = str_replace(',', '.',
				str_replace('.', '', $tabelasPreco[$tabelaPreco->id]['valor']));
			$tabelaPreco->pivot->save();
		}

		return $produto;
	}

}
