<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AbstractModel extends Model
{
    protected $guarded = [];

    use SoftDeletes;

    protected static function boot() {
        parent::boot();

        static::addGlobalScope('maisRecente', function (Builder $builder) {
            $builder->orderBy($builder->getModel()->getTable().'.created_at', 'desc');
        });
    }

    public function scopePaginar($query, $quantidade, $pagina = 1)
    {
        $offset = ($pagina - 1) * $quantidade;

        return $query->skip($offset)->take($quantidade)->get();
    }

    public function scopeMaisRecente($query)
    {
        $obj = $query->orderBy('created_at', 'desc')->first();
        return $obj->exists ? $obj : $this;
    }

    public function scopeMaisAntigo($query)
    {
        $obj = $query->orderBy('created_at', 'asc')->first();
        return $obj->exists ? $obj : $this;
    }

    public function fill(array $attributes)
    {
        foreach ($attributes as &$attribute) {
            if ($attribute == "") {
                $attribute = null;
            }
        }

        parent::fill($attributes);

        if (!empty($this->{$this->getKeyName()})) {
            $this->exists = true;
        }
    }

    protected function performInsert(Builder $query)
    {
        $this->created_by = \Auth::user()->id ?? null;
        return parent::performInsert($query);
    }

    public function cadastradoPor()
    {
        return $this->belongsTo('Mrbarber\Model\Usuario', 'created_by', 'uso_id');
    }

    public function save(array $options = [])
    {
        try {
            return parent::save($options);
        } catch (Exception $e) {
            throw new Exception("Ocorreu um erro inesperado. Por favor, contate o administrador do sistema.", 1);
        }
    }

    public static function firstWhere($column, $operator = null, $value = null, $boolean = 'and')
    {
        if (func_num_args() == 2) {
            $value = $operator;

            $operator = '=';
        }

        return self::where($column, $operator, $value, $boolean)->first();
    }

}
