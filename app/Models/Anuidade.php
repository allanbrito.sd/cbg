<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Anuidade extends AbstractModel
{
	public function lancamento()
	{
		return $this->belongsTo('App\Models\Lancamento');
	}

	public function modalidade()
	{
		return $this->belongsTo('App\Models\Modalidade');
	}
}
