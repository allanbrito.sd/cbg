<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Atleta extends Pessoa
{
	protected static function boot() {
        parent::boot();

        static::addGlobalScope('pessoa', function (Builder $builder) {
			$builder->join('pessoas as pessoa_join', 'pessoa_join.id', 'atletas.pessoa_id')->selectRaw('pessoa_join.*, atletas.*')->whereNull('pessoa_join.deleted_at');
        });
    }

	public function pessoa()
	{
		return $this->belongsTo('App\Models\Pessoa');
	}
}
