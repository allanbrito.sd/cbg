<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Conta extends AbstractModel
{
	public function pagamento()
	{
		return $this->belongsTo('App\Models\Pagamento');
	}

    public function dadosBancarios()
    {
        return $this->belongsTo('App\Models\DadosBancarios', 'dado_bancario_id');
    }

}
