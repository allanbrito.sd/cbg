<?php

namespace App\Models;

class Contato extends AbstractModel
{
    public function pessoa()
    {
        return $this->hasOne('App\Models\Pessoa', 'contato_principal_id');
    }
}
