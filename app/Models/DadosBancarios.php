<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class DadosBancarios extends AbstractModel
{
    use SoftDeletes;

    public function pessoa()
    {
        return $this->hasOne('App\Models\Pessoa', 'dado_bancario_id');
    }
}
