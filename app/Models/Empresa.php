<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use App\Traits\IsPessoa;
use App\Contracts\IsPessoa as IsPessoaContract;

class Empresa extends AbstractModel implements IsPessoaContract
{
	use IsPessoa;

	protected static function boot() {
        parent::boot();

        static::addGlobalScope('pessoa', function (Builder $builder) {
			$builder->join('pessoas as pessoa_join', 'pessoa_join.id', 'clubes.pessoa_id')->selectRaw('pessoa_join.*, clubes.*')->whereNull('pessoa_join.deleted_at');
        });
    }

	public function pessoa()
	{
		return $this->belongsTo('App\Models\Pessoa');
	}
}
