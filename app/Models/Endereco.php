<?php

namespace App\Models;

class Endereco extends AbstractModel
{
    public static $tipo = ['principal' => 1];

    // Validação
    public static function validationRules()
    {
        return [
            'logradouro' => 'nullable|max:255',
            'complemento' => 'nullable|max:255',
            'numero' => 'nullable|max:15',
            'cep' => 'nullable|formato_cep',
            'cidade' => 'nullable|max:100',
            'bairro' => 'nullable|max:100',
            'estado_id' => 'nullable|integer',
        ];

    }

    // Atributos calculados
    public function getCidadeEstadoAttribute()
    {
        return $this->cidade . "/" . $this->estado->sigla;
    }

    // Escopos
    public function scopePrincipal($query)
    {
        return $query->where('tipo', self::tipos['principal']);
    }

    // Relações
    public function pessoa()
    {
        return $this->hasOne('App\Models\Pessoa');
    }

    public function estado()
    {
        return $this->belongsTo('App\Models\Estado');
    }

}
