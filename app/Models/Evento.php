<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Evento extends AbstractModel
{
	public $dates = [
		'data_inicio',
		'data_fim'];

	public function setDataInicioAttribute($value)
    {
        $this->attributes['data_inicio'] = !empty($value) ? Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d')." 00:00:00" : $value;
    }

	public function setDataFimAttribute($value)
    {
        $this->attributes['data_fim'] = !empty($value) ? Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d')." 00:00:00" : $value;
    }


	public function lancamento()
	{
		return $this->hasOne('App\Models\Lancamento');
	}

	public function getNomeExibicaoAttribute() {
		return $this->nome.", realizado de ".$this->data_inicio->format('d/m')." a ".$this->data_fim->format('d/m').(!empty($this->local) ? " em ".$this->local : "");
	}

}
