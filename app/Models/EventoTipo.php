<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class EventoTipo extends AbstractModel
{
	public function evento()
	{
		return $this->belongsTo('App\Models\Evento');
	}

}
