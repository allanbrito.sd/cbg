<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class FigLicense extends AbstractModel
{
	public function lancamento()
	{
		return $this->belongsTo('App\Models\Lancamento');
	}

	public function atleta()
	{
		return $this->belongsTo('App\Models\Atleta');
	}

	public function modalidade()
	{
		return $this->belongsTo('App\Models\Modalidade');
	}

	public function clube()
	{
		return $this->belongsTo('App\Models\Clube');
	}
}
