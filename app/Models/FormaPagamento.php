<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class FormaPagamento extends AbstractModel
{
	public $table = 'formas_pagamento';
	public function pagamento()
	{
		return $this->belongsTo('App\Models\Pagamento');
	}
}
