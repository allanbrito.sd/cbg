<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Lancamento extends AbstractModel
{

	public $dates = [
		'vencimento',
		'competencia'];

	public function setVencimentoAttribute($value)
    {
        $this->attributes['vencimento'] = !empty($value) ? Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d')." 00:00:00" : $value;
    }

	public function setCompetenciaAttribute($value)
    {
        $this->attributes['competencia'] = !empty($value) ? Carbon::createFromFormat('d/m/Y', '01/'.$value)->format('Y-m-d')." 00:00:00" : $value;
    }

	public function pessoa()
	{
		return $this->belongsTo('App\Models\Pessoa');
	}

	public function notaFiscal()
	{
		return $this->belongsTo('App\Models\NotaFiscal');
	}

	public function pagamento()
	{
		return $this->hasOne('App\Models\Pagamento');
	}

	public function eventoItens()
	{
		return $this->hasMany('App\Models\LancamentoEventoItem');
	}

	public function anuidade()
	{
		return $this->hasMany('App\Models\Anuidade');
	}

	public function figLicenses()
	{
		return $this->hasMany('App\Models\FigLicense');
	}

	public function transferencias()
	{
		return $this->hasMany('App\Models\Transferencia');
	}
}
