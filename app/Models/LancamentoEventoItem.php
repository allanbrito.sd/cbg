<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class LancamentoEventoItem extends AbstractModel
{
	protected $table = "lancamento_evento_itens";

	public function lancamento()
	{
		return $this->belongsTo('App\Models\Lancamento');
	}

	public function evento()
	{
		return $this->belongsTo('App\Models\Evento');
	}

	public function item()
	{
		return $this->belongsTo('App\Models\EventoItem', 'evento_item_id', 'id');
	}

}
