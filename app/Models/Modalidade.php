<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Modalidade extends AbstractModel
{
	public function lancamento()
	{
		return $this->hasOne('App\Models\Lancamento');
	}

}
