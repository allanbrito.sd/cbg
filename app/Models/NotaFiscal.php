<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class NotaFiscal extends AbstractModel
{
	public $table = "notas_fiscais";

	public function lancamento()
	{
		return $this->hasOne('App\Models\Lancamento');
	}
}
