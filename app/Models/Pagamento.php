<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Pagamento extends AbstractModel
{
	public $dates = [
		'data',
	];

	public function setDataAttribute($value)
    {
        $this->attributes['data'] = !empty($value) ? Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d')." 00:00:00" : $value;
    }

	protected static function boot() {
        parent::boot();
    }

	public function pessoa()
	{
		return $this->belongsTo('App\Models\Pessoa');
	}

	public function lancamento()
	{
		return $this->belongsTo('App\Models\Lancamento');
	}

	public function planoContas()
	{
		return $this->belongsTo('App\Models\PlanoContas');
	}

	public function recibo()
	{
		return $this->hasOne('App\Models\Recibo');
	}

	public function recibos()
	{
		return $this->hasMany('App\Models\Recibo');
	}
}
