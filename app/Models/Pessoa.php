<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pessoa extends AbstractModel
{
    use SoftDeletes;

    public static $tipo = [1 => 'Física', 2 => 'Jurídica'];

    public $fotoPadraoPessoaFisica = 'user-no-photo.jpg';
    public $fotoPadraoPessoaJuridica = 'no-image.png';

    public $dates = [
        'data_nascimento',
        'rg_data_expedicao',
        'passaporte_data_validade',
    ];

    public function setDataNascimentoAttribute($value)
    {
        $this->attributes['data_nascimento'] = !empty($value) ? Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d')." 00:00:00" : $value;
    }

    public function setRgDataExpedicaoAttribute($value)
    {
        $this->attributes['rg_data_expedicao'] = !empty($value) ? Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d')." 00:00:00" : $value;
    }

    public function setPassaporteDataValidadeAttribute($value)
    {
        $this->attributes['passaporte_data_validade'] = !empty($value) ? Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d')." 00:00:00" : $value;
    }

    // Validação
    public static function validationRules()
    {
        $tipoPessoas = collect(self::$tipo);

        return [
            'nome' => 'required|max:255',
            'tipo' => 'nullable|in:' . $tipoPessoas->keys()->implode(','),
            'cpf' => 'nullable|cpf',
            'razao_social' => 'nullable|max:255',
            'cnpj' => 'nullable|cnpj',
            'rg' => 'nullable|max:20',
            'rg_orgao_expedidor' => 'nullable|max:50',
        ];
    }

    // Regras
    public function isFisica()
    {
        return $this->tipo == collect(self::$tipo)->search('Física');
    }

    public function isJuridica()
    {
        return $this->tipo == collect(self::$tipo)->search('Jurídica');
    }

    // Atributos calculados
    public function getNomeESobrenomeAttribute()
    {
        return $this->getPrimeiroNomeAttribute() . " " . $this->getSegundoNomeAttribute();
    }

    public function getPrimeiroNomeAttribute()
    {
        $nomeArr = explode(' ', $this->nome);
        $nome = ($nomeArr[0] ?? '');

        return ucwords(trim($nome));
    }

    public function getSegundoNomeAttribute()
    {
        $nomeArr = explode(' ', $this->nome);
        $nome = ($nomeArr[1] ?? '');

        return ucwords(trim($nome));
    }

    public function getFotoAttribute()
    {
        $fotoPadrao = $this->tipo == collect(self::$tipo)->search('Física') ? $this->fotoPadraoPessoaFisica : $this->fotoPadraoPessoaJuridica;

        return 'images/' . ($this->photo ?? $fotoPadrao);
    }

    public function scopeAvulsas($query)
    {
        return $query->doesntHave('atleta')->doesntHave('clube')->doesntHave('usuario');
    }

    // public function getenderecoAttribute() {
    //     $endereco = $this->enderecos()->get()->first();/*->principal()->first();*/

    //     if ($endereco == null) {
    //         $endereco  = new Endereco;
    //         $endereco->pessoa()->associate($this);
    //     }

    //     return $endereco;
    // }

    // Relações
    public function atleta()
    {
        return $this->hasOne('App\Models\Atleta');
    }

    public function clube()
    {
        return $this->hasOne('App\Models\Clube');
    }

    public function endereco()
    {
        return $this->belongsTo('App\Models\Endereco', 'endereco_principal_id');
    }

    public function contato()
    {
        return $this->belongsTo('App\Models\Contato', 'contato_principal_id');
    }

    public function dadosBancarios()
    {
        return $this->belongsTo('App\Models\DadosBancarios', 'dado_bancario_id');
    }

    public function usuario()
    {
        return $this->hasOne('App\Models\Usuario');
    }

}
