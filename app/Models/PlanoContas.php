<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class PlanoContas extends AbstractModel
{
	public function pagamento()
	{
		return $this->belongsTo('App\Models\Pagamento');
	}
}
