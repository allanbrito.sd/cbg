<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Recibo extends AbstractModel
{
	public $dates = [
		'gerado_em',
		'cancelado_em',
	];

	public function pagamento()
	{
		return $this->belongsTo('App\Models\Pagamento');
	}
}
