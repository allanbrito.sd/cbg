<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Transferencia extends AbstractModel
{
	public function lancamento()
	{
		return $this->belongsTo('App\Models\Lancamento');
	}

	public function atleta()
	{
		return $this->belongsTo('App\Models\Atleta');
	}

	public function modalidade()
	{
		return $this->belongsTo('App\Models\Modalidade');
	}

	public function clubeOrigem()
	{
		return $this->belongsTo('App\Models\Clube', 'clube_origem_id');
	}

	public function clubeDestino()
	{
		return $this->belongsTo('App\Models\Clube', 'clube_destino_id');
	}
}
