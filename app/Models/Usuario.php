<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;

class Usuario extends Authenticatable
{
	use Notifiable;
	use HasRoleAndPermission;

	protected $table = 'usuarios';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'nome',
		'email',
		'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	public function pessoa()
	{
		return $this->belongsTo('App\Models\Pessoa');
	}

	public function empresas()
	{
		return $this->belongsToMany('App\Models\Empresa');
	}

	public function rolePermissions()
	{
		$permissionModel = app(config('roles.models.permission'));

		if (!$permissionModel instanceof Model) {
			throw new \InvalidArgumentException('[roles.models.permission] must be an instance of \Illuminate\Database\Eloquent\Model');
		}

		return $permissionModel::select([
			'privilegios.*',
			'grupo_privilegio.created_at as pivot_created_at',
			'grupo_privilegio.updated_at as pivot_updated_at',
		])
			->join('grupo_privilegio', 'grupo_privilegio.privilegio_id', '=', 'privilegios.id')
			->join('grupos', 'grupos.id', '=', 'grupo_privilegio.grupo_id')
			->whereIn('grupos.id', $this->getRoles()->pluck('id')->toArray())
			->orWhere('grupos.level', '<', $this->level())
			->groupBy([
				'privilegios.id',
				'privilegios.name',
				'privilegios.slug',
				'privilegios.description',
				'privilegios.model',
				'privilegios.created_at',
				'privilegios.updated_at',
				'pivot_created_at',
				'pivot_updated_at',
			]);
	}


}
