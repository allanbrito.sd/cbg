<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class ProdutoPresenter extends Presenter
{

	public function valor($tabelaPreco = null)
	{
		$tabelasPreco = $this->tabelasPreco;
		if ($tabelaPreco) {
			$tabelasPreco->where('id', $tabelaPreco->id);
		}

		return number_format($tabelasPreco->first()->pivot->valor, 2, ',', '.');
	}

}