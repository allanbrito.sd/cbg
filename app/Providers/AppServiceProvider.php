<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Carbon::setLocale('pt');

		Blade::directive('define', function ($expression) {
			return "<?php $expression; ?>";
		});

		Blade::directive('method', function ($expression) {
			return "<input name='_method' type='hidden' value=$expression/> <?php echo csrf_field(); ?>";
		});

		Blade::directive('showError', function ($expression) {
			return "<?php \$errorFor = $expression;
				 echo \$__env->make('errors.partials.form', array_except(get_defined_vars(), array('__data', '__path')))->render();
			?>";
		});

	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
		require_once __DIR__ . '/../Http/Helpers/Navigation.php';
	}
}
