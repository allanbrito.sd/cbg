<?php

namespace App\Providers;

use LaravelLegends\PtBrValidator\ValidatorProvider as Validator;

class ValidatorProvider extends Validator
{
	protected function getMessages()
	{
		return [
			'celular'          => 'O campo não é um celular válido',
			'celular_com_ddd'  => 'O campo não é um possui o formato válido de celular com DDD',
			'cnh'              => 'O campo não é uma carteira nacional de habilitação válida',
			'cnpj'             => 'O campo não é um CNPJ válido',
			'cpf'              => 'O campo não é um CPF válido',
			'data'             => 'O campo não é uma data com formato válido',
			'formato_cnpj'     => 'O campo não possui o formato válido de CNPJ',
			'formato_cpf'      => 'O campo não possui o formato válido de CPF',
			'telefone'         => 'O campo não é um telefone válido',
			'telefone_com_ddd' => 'O campo não é um possui o formato válido de telefone com DDD',
			'formato_cep'      => 'O campo não possui um formato válido de CEP',
		];
	}
}
