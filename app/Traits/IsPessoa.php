<?php

namespace App\Traits;

trait IsPessoa
{
    // Regras
    public function isFisica()
    {
        return $this->pessoa->isFisica();
    }

    public function isJuridica()
    {
        return $this->pessoa->isJuridica();
    }

    // Atributos calculados
    public function getNomeESobrenomeAttribute()
    {
        return $this->pessoa->nomeESobrenome;
    }

    public function getPrimeiroNomeAttribute()
    {
        return $this->pessoa->primeiroNome;
    }

    public function getSegundoNomeAttribute()
    {
        return $this->pessoa->segundoNome;
    }

    public function getFotoAttribute()
    {
        return $this->pessoa->foto;
    }

    public function endereco()
    {
        return $this->pessoa->endereco();
    }

    // public function enderecos()
    // {
    //     return $this->pessoa->enderecos();
    // }

    public function contato()
    {
        return $this->pessoa->contato();
    }

    public function usuario()
    {
        return $this->pessoa->usuario();
    }

    public function dadosBancarios()
    {
        return $this->pessoa->dadosBancarios();
    }
}
