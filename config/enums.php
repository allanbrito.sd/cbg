<?php
return [
	'pessoa_tipo'                 => [1 => 'fisica', 2 => 'juridica'],
	'empresa_atividade'           => [
		1 => 'Representação comercial',
		2 => 'Distribuição',
		3 => 'Indústria / Manufatura',
		4 => 'Outros',
	],
	'empresa_atividade_descricao' => [
		1 => 'Tenho uma carteira de clientes e presento comercialmente uma ou mais marcas',
		2 => 'Compro e revendo produtos de uma ou mais marcas, vendo produtos no atacado',
		3 => 'Fabrico produtos em geral',
		4 => 'Sou vendedor consignado, prestador de serviços, consultor, varejista e afins',
	],
];
