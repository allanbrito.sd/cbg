<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnderecosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('enderecos', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('logradouro', 255)->nullable();
			$table->string('complemento', 255)->nullable();
			$table->string('numero', 15)->nullable();
			$table->string('cep', 10)->nullable();
			$table->string('cidade', 100)->nullable();
			$table->string('bairro', 100)->nullable();


			$table->integer('estado_id')->unsigned()->nullable();
			$table->foreign('estado_id')->references('id')->on('estados');


			//--
			$table->timestamps();
			$table->softDeletes();

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('enderecos');
	}
}
