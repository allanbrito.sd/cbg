<?php

use App\Models\Pessoa;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePessoaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome', 255)->index();

            $pessoaTipos = collect(Pessoa::$tipo);
            $table->tinyInteger('tipo')->default($pessoaTipos->search('Física'))->index();

            //Pessoa física:'   A'
            $table->string('cpf', 14)->nullable()->index();
            $table->dateTime('data_nascimento')->nullable()->index();
            $table->string('rg', 20)->nullable()->index();
            $table->string('rg_orgao_expedidor', 50)->nullable();
            $table->dateTime('rg_data_expedicao')->nullable()->index();
            $table->string('passaporte', 10)->nullable()->index();
            $table->dateTime('passaporte_data_validade')->nullable()->index();

            //Pessoa jurídica;
            $table->string('cnpj', 18)->nullable()->index();
            $table->string('inscricao_municipal', 15)->nullable()->index();
            $table->string('inscricao_estadual', 15)->nullable()->index();

            $table->bigInteger('endereco_principal_id')->unsigned()->nullable();
            $table->foreign('endereco_principal_id')->references('id')->on('enderecos');

            //--
            $table->timestamps();
            $table->softDeletes();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pessoas');
    }
}
