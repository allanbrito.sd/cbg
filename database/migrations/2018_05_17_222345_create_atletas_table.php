<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAtletasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('atletas', function (Blueprint $table) {
			$table->increments('id');

			$table->bigInteger('pessoa_id')->unsigned();
			$table->foreign('pessoa_id')->references('id')->on('pessoas');

			//--
			$table->timestamps();
			$table->softDeletes();

			$table->integer('created_by')->unsigned()->nullable();
			$table->foreign('created_by')->references('id')->on('usuarios');

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('atletas');
	}
}
