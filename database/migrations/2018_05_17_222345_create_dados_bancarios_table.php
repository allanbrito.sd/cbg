<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDadosBancariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dados_bancarios', function (Blueprint $table) {
            $table->increments('id');

            $table->string('numero_banco', 5)->nullable();
            $table->string('nome_banco', 30)->nullable();
            $table->string('agencia', 10)->nullable();
            $table->string('operacao', 5)->nullable();
            $table->string('conta', 10)->nullable();
            $table->string('tipo', 30)->nullable();

            //--
            // $table->timestamps();
            $table->softDeletes();

            $table->integer('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('usuarios');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dados_bancarios');
    }
}
