<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eventos', function (Blueprint $table) {
			$table->increments('id');

			$table->string('nome', 255)->index();
			$table->timestamp('data_inicio');
			$table->timestamp('data_fim');
			$table->string('local', 255)->nullable();

			
			//--
			$table->timestamps();
			$table->softDeletes();

			$table->integer('created_by')->unsigned()->nullable();
			$table->foreign('created_by')->references('id')->on('usuarios');

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eventos');
	}
}
