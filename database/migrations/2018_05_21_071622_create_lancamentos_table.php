<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLancamentosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lancamentos', function (Blueprint $table) {
			$table->increments('id');

			$table->string('historico', 255)->index();
			$table->decimal('valor', 10, 2);
			$table->date('vencimento');
			$table->date('competencia');
			$table->tinyInteger('credito');
			
			$table->integer('evento_id')->unsigned()->nullable();
			$table->foreign('evento_id')->references('id')->on('eventos');

			$table->integer('nota_fiscal_id')->unsigned();
			$table->foreign('nota_fiscal_id')->references('id')->on('notas_fiscais');

			$table->bigInteger('pessoa_id')->unsigned();
			$table->foreign('pessoa_id')->references('id')->on('pessoas');
			//--
			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('created_by')->unsigned()->nullable();
			$table->foreign('created_by')->references('id')->on('usuarios');

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lancamentos');
	}
}
