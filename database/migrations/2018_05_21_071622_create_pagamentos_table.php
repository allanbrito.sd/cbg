<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagamentosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pagamentos', function (Blueprint $table) {
			$table->increments('id');

			$table->decimal('valor', 10, 2);
			$table->tinyInteger('pago')->default(0);
			$table->tinyInteger('devolvido')->default(0);
			$table->date('data')->nullable();
			$table->string('comprovante', 100)->nullable();
			$table->string('detalhamento')->nullable();

			$table->integer('forma_pagamento_id')->unsigned()->nullable();
			$table->foreign('forma_pagamento_id')->references('id')->on('formas_pagamento');

			$table->bigInteger('pessoa_id')->unsigned()->nullable();
			$table->foreign('pessoa_id')->references('id')->on('pessoas');

			//--
			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('created_by')->unsigned()->nullable();
			$table->foreign('created_by')->references('id')->on('usuarios');

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pagamentos');
	}
}
