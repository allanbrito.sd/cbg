<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPessoaTableAddDadosBancarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pessoas', function (Blueprint $table) {
            $table->integer('dado_bancario_id')->unsigned()->nullable();
            $table->foreign('dado_bancario_id')->references('id')->on('dados_bancarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pessoas', function (Blueprint $table) {
            $table->dropForeign(['dado_bancario_id']);
            $table->dropColumn('dado_bancario_id');
        });
    }
}
