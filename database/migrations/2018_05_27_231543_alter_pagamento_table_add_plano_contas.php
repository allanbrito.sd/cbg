<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPagamentoTableAddPlanoContas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pagamentos', function (Blueprint $table) {
            $table->integer('plano_contas_id')->unsigned()->nullable();
            $table->foreign('plano_contas_id')->references('id')->on('plano_contas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pagamentos', function (Blueprint $table) {
            $table->dropForeign(['plano_contas_id']);
            $table->dropColumn('plano_contas_id');
        });
    }
}
