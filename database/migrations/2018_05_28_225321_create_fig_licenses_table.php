<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFigLicensesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fig_licenses', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('lancamento_id')->unsigned();
			$table->foreign('lancamento_id')->references('id')->on('lancamentos');

			$table->integer('atleta_id')->unsigned();
			$table->foreign('atleta_id')->references('id')->on('atletas');

			$table->integer('modalidade_id')->unsigned();
			$table->foreign('modalidade_id')->references('id')->on('modalidades');

			$table->integer('clube_id')->unsigned();
			$table->foreign('clube_id')->references('id')->on('clubes');

			//--
			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('created_by')->unsigned()->nullable();
			$table->foreign('created_by')->references('id')->on('usuarios');

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fig_licenses');
	}
}
