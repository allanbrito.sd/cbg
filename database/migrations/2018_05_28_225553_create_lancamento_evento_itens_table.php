<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLancamentoEventoItensTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lancamento_evento_itens', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('lancamento_id')->unsigned();
			$table->foreign('lancamento_id')->references('id')->on('lancamentos');

			$table->integer('evento_id')->unsigned();
			$table->foreign('evento_id')->references('id')->on('eventos');
			
			$table->integer('evento_item_id')->unsigned();
			$table->foreign('evento_item_id')->references('id')->on('evento_itens');

			//--
			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('created_by')->unsigned()->nullable();
			$table->foreign('created_by')->references('id')->on('usuarios');

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lancamento_evento_itens');
	}
}
