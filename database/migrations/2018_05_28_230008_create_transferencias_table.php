<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransferenciasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transferencias', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('lancamento_id')->unsigned();
			$table->foreign('lancamento_id')->references('id')->on('lancamentos');

			$table->integer('atleta_id')->unsigned();
			$table->foreign('atleta_id')->references('id')->on('atletas');
			
			$table->string('principal_titulo_atual', 150)->nullable();

			$table->integer('modalidade_id')->unsigned();
			$table->foreign('modalidade_id')->references('id')->on('modalidades');

			$table->integer('clube_origem_id')->unsigned();
			$table->foreign('clube_origem_id')->references('id')->on('clubes');
			
			$table->integer('clube_destino_id')->unsigned();
			$table->foreign('clube_destino_id')->references('id')->on('clubes');

			//--
			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('created_by')->unsigned()->nullable();
			$table->foreign('created_by')->references('id')->on('usuarios');

			$table->engine = 'InnoDB';
		});

		Schema::table('atletas', function(Blueprint $table) {
			$table->string('principal_titulo_atual', 150);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transferencias');
		Schema::table('atletas', function (Blueprint $table) {
            $table->dropColumn('principal_titulo_atual');
        });
	}
}
