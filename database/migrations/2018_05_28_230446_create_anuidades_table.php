<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnuidadesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('anuidades', function (Blueprint $table) {
			$table->increments('id');

			$table->string('ano', 4);
			$table->integer('lancamento_id')->unsigned();
			$table->foreign('lancamento_id')->references('id')->on('lancamentos');

			$table->integer('modalidade_id')->unsigned();
			$table->foreign('modalidade_id')->references('id')->on('modalidades');

			//--
			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('created_by')->unsigned()->nullable();
			$table->foreign('created_by')->references('id')->on('usuarios');

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('anuidades');
	}
}
