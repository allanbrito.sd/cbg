<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterRecibosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recibos', function (Blueprint $table) {
			$table->string('codigo', 15);
			$table->string('ano', 2);
			$table->string('numero', 4);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recibos', function (Blueprint $table) {
            $table->dropColumn('codigo');
            $table->dropColumn('ano');
            $table->dropColumn('numero');
        });
	}
}
