<?php

use App\Models\Estado;
use Illuminate\Database\Seeder;

class EstadosSeeder extends Seeder
{

	public function run()
	{
		Estado::create(['sigla' => 'AC', 'nome' => 'Acre']);
		Estado::create(['sigla' => 'AL', 'nome' => 'Alagoas']);
		Estado::create(['sigla' => 'AP', 'nome' => 'Amapá']);
		Estado::create(['sigla' => 'AM', 'nome' => 'Amazonas']);
		Estado::create(['sigla' => 'BA', 'nome' => 'Bahia']);
		Estado::create(['sigla' => 'CE', 'nome' => 'Ceará']);
		Estado::create(['sigla' => 'DF', 'nome' => 'Distrito Federal']);
		Estado::create(['sigla' => 'ES', 'nome' => 'Espírito Santo']);
		Estado::create(['sigla' => 'GO', 'nome' => 'Goiás']);
		Estado::create(['sigla' => 'MA', 'nome' => 'Maranhão']);
		Estado::create(['sigla' => 'MT', 'nome' => 'Mato Grosso']);
		Estado::create(['sigla' => 'MS', 'nome' => 'Mato Grosso do Sul']);
		Estado::create(['sigla' => 'MG', 'nome' => 'Minas Gerais']);
		Estado::create(['sigla' => 'PA', 'nome' => 'Pará']);
		Estado::create(['sigla' => 'PB', 'nome' => 'Paraíba']);
		Estado::create(['sigla' => 'PR', 'nome' => 'Paraná']);
		Estado::create(['sigla' => 'PE', 'nome' => 'Pernambuco']);
		Estado::create(['sigla' => 'PI', 'nome' => 'Piauí']);
		Estado::create(['sigla' => 'RJ', 'nome' => 'Rio de Janeiro']);
		Estado::create(['sigla' => 'RN', 'nome' => 'Rio Grande do Norte']);
		Estado::create(['sigla' => 'RS', 'nome' => 'Rio Grande do Sul']);
		Estado::create(['sigla' => 'RO', 'nome' => 'Rondônia']);
		Estado::create(['sigla' => 'RR', 'nome' => 'Roraima']);
		Estado::create(['sigla' => 'SC', 'nome' => 'Santa Catarina']);
		Estado::create(['sigla' => 'SP', 'nome' => 'São Paulo']);
		Estado::create(['sigla' => 'SE', 'nome' => 'Sergipe']);
		Estado::create(['sigla' => 'TO', 'nome' => 'Tocantins']);
	}
}
