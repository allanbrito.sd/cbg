<?php

use App\Models\EventoItem;
use Illuminate\Database\Seeder;

class EventoItensSeeder extends Seeder
{

	public function run()
	{
		EventoItem::create(['nome' => 'Cadastro ginastas / técnicos']);
		EventoItem::create(['nome' => 'Recadastro ginastas / técnicos']);
 		EventoItem::create(['nome' => 'Inscrição']);
 		EventoItem::create(['nome' => 'Arbitragem']);
 		EventoItem::create(['nome' => 'Grupo GPT']);
		EventoItem::create(['nome' => 'Participante GPT']);
	}
}
