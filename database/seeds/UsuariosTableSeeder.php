<?php

use App\Models\Grupo;
use App\Models\Pessoa;
use App\Models\Privilegio;
use App\Models\Usuario;
use Illuminate\Database\Seeder;

class UsuariosTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		if (Usuario::where('email', 'contato@allanbritosd.com')->get()->isEmpty()) {
			$pessoa = Pessoa::create(['nome' => 'Admin']);
			$usuario = new Usuario([
				'email'    => 'contato@allanbritosd.com',
				'password' => bcrypt('123'),
			]);

			$usuario->pessoa()->associate($pessoa);
			$usuario->save();
		}
	}
}
