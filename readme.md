# Equipe de Vendas

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

## Configurar ambiente
sudo apt-get update
sudo apt-get install redis-server

## Configurar ambiente de desenvolvimento
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/equipedevendas.com.conf

sudo nano /etc/apache2/sites-available/equipedevendas.com.conf

<VirtualHost *:80>
	ServerAdmin job@allanbritosd.com
    ServerName example.com
	ServerAlias www.example.com
	DocumentRoot /var/www/html/equipedevendas/public
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

sudo a2ensite equipedevendas.com.conf

sudo vim /etc/hosts

127.0.0.1       equipedevendas.com
127.0.0.1       www.equipedevendas.com

sudo service apache2 restart

## Vendors
https://github.com/yajra/laravel-datatables
https://github.com/jeremykenedy/laravel-roles
https://github.com/HipsterJazzbo/Landlord
https://github.com/barryvdh/laravel-snappy