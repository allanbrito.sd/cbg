/*
 *
 *   INSPINIA - Responsive Admin Theme
 *   version 2.7
 *
 */

$(document).ready(function () {


	// Add body-small class if window less than 768px
	if ($(this).width() < 769) {
		$('body').addClass('body-small')
	} else {
		$('body').removeClass('body-small')
	}

	// MetsiMenu
	$('#side-menu').metisMenu();

	// Collapse ibox function
	$('.collapse-link').on('click', function () {
		var ibox = $(this).closest('div.ibox');
		var button = $(this).find('i');
		var content = ibox.children('.ibox-content');
		content.slideToggle(200);
		button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
		ibox.toggleClass('').toggleClass('border-bottom');
		setTimeout(function () {
			ibox.resize();
			ibox.find('[id^=map-]').resize();
		}, 50);
	});

	// Close ibox function
	$('.close-link').on('click', function () {
		var content = $(this).closest('div.ibox');
		content.remove();
	});

	// Fullscreen ibox function
	$('.fullscreen-link').on('click', function () {
		var ibox = $(this).closest('div.ibox');
		var button = $(this).find('i');
		$('body').toggleClass('fullscreen-ibox-mode');
		button.toggleClass('fa-expand').toggleClass('fa-compress');
		ibox.toggleClass('fullscreen');
		setTimeout(function () {
			$(window).trigger('resize');
		}, 100);
	});

	// Minimalize menu
	$('.navbar-minimalize').on('click', function () {
		$("body").toggleClass("mini-navbar");
		SmoothlyMenu();


	});

	// Full height of sidebar
	function fix_height() {
		var heightWithoutNavbar = $("body > #wrapper").height() - 61;
		$(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");

		var navbarHeight = $('nav.navbar-default').height();
		var wrapperHeigh = $('#page-wrapper').height();

		if (navbarHeight > wrapperHeigh) {
			$('#page-wrapper').css("min-height", navbarHeight + "px");
		}

		if (navbarHeight < wrapperHeigh) {
			$('#page-wrapper').css("min-height", $(window).height() + "px");
		}

		if ($('body').hasClass('fixed-nav')) {
			if (navbarHeight > wrapperHeigh) {
				$('#page-wrapper').css("min-height", navbarHeight + "px");
			} else {
				$('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
			}
		}

	}

	fix_height();

	// Fixed Sidebar
	$(window).bind("load", function () {
		if ($("body").hasClass('fixed-sidebar')) {
			$('.sidebar-collapse').slimScroll({
				height: '100%',
				railOpacity: 0.9
			});
		}
	});

	$(window).bind("load resize scroll", function () {
		if (!$("body").hasClass('body-small')) {
			fix_height();
		}
	});

	// Add slimscroll to element
	$('.full-height-scroll').slimscroll({
		height: '100%'
	})

	$(document).on("click", ".widget-header", function() {
		if ($(this).find(".collapse-icon").length == 0) {
			return;
		}
		$iconOpen = $(this).find(".collapse-icon.fa-chevron-down"),
		$iconClose = $(this).find(".collapse-icon.fa-chevron-up"),
		$box = $(this).closest(".widget-box"),
		$body = $box.find(".widget-body");

		if($iconOpen.length) {
			$iconOpen.removeClass("fa-chevron-down").addClass("fa-chevron-up");
			$box.removeClass("collapsed");
			$body.slideDown();
		} else {
			$iconClose.removeClass("fa-chevron-up").addClass("fa-chevron-down")
			$box.addClass("collapsed");
			$body.slideUp();
		}
	});

	$('.has-tooltip').tooltip();

	$('select.chosen-select-search').chosen({width: "100%"});
	$('select.chosen-select').chosen({width: "100%", disable_search: true});


	$('.right-sidebar-toggle').on('click', function () {
		$('#right-sidebar').toggleClass('sidebar-open');
	});

	$(document).on('click', ".acoes [role=submit]", function(e) {
		var action = $(this).attr('href'),
			$formulario = $(this).closest('form'),
			$formulario = $formulario.length == 0 ? $('form[action="'+action+'"]') : $formulario;

		if ($formulario.length == 1) {
			e.preventDefault();
			$formulario.submit();
		}
	});

	$(document).on('click', '.table_link', function (){
		var href = $(this).data('href');
		if (!href) {
			return;
		}

		window.location.href = href;
	});

	$(document).on('click', '.mostrar_mais .expandir', function() {
		$(this).hide();
		$(this).parents('.mostrar_mais').find('.').show();
	});

	$('.data').mask("00/00/0000", {/*placeholder: "__/__/____", */selectOnFocus: true, clearIfNotMatch: true});
	$('.hora').mask('00:00:00', {/*placeholder: "__:__:__", */selectOnFocus: true, clearIfNotMatch: true});
	$('.data_hora').mask('00/00/0000 00:00:00', {/*placeholder: "__/__/____ __:__:__", */selectOnFocus: true, clearIfNotMatch: true});
	$('.cep').mask('00.000-000', {/*placeholder: "__.___-___", */selectOnFocus: true, clearIfNotMatch: true});
	var telefoneMaskBehavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
		onKeyPress: function(val, e, field, options) {
			field.mask(SPMaskBehavior.apply({}, arguments), options);
		}
	};
	$('.telefone').mask(telefoneMaskBehavior, spOptions);
	$('.cpf').mask('000.000.000-00', {/*placeholder: '___.___.___-__', */reverse: true});
	$('.cnpj').mask('00.000.000/0000-00', {/*placeholder: '__.___.___/____-__', */reverse: true});
	$('.valor').mask('000.000.000.000.000,00', {reverse: true});
	$('.valor2').mask("#.##0,00", {reverse: true});
	$('.porcentagem').mask('##0,00%', {reverse: true});
});


// Minimalize menu when screen is less than 768px
$(window).bind("resize", function () {
	if ($(this).width() < 769) {
		$('body').addClass('body-small')
	} else {
		$('body').removeClass('body-small')
	}
});

function SmoothlyMenu() {
	if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
		// Hide menu in order to smoothly turn on when maximize menu
		$('#side-menu').hide();
		// For smoothly turn on menu
		setTimeout(
			function () {
				$('#side-menu').fadeIn(400);
			}, 200);
	} else if ($('body').hasClass('fixed-sidebar')) {
		$('#side-menu').hide();
		setTimeout(
			function () {
				$('#side-menu').fadeIn(400);
			}, 100);
	} else {
		// Remove all inline style from jquery fadeIn function to reset menu state
		$('#side-menu').removeAttr('style');
	}
}
