<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'accepted'             => 'Este campo deve ser aceito.',
    'active_url'           => 'Este campo não é uma URL válida.',
    'after'                => 'Este campo deve ser uma data depois de :date.',
    'after_or_equal'       => 'Este campo deve ser uma data posterior ou igual a :date.',
    'alpha'                => 'Este campo deve conter somente letras.',
    'alpha_dash'           => 'Este campo deve conter letras, números e traços.',
    'alpha_num'            => 'Este campo deve conter somente letras e números.',
    'array'                => 'Este campo deve ser um array.',
    'before'               => 'Este campo deve ser uma data antes de :date.',
    'before_or_equal'      => 'Este campo deve ser uma data anterior ou igual a :date.',
    'between'              => [
        'numeric' => 'Este campo deve estar entre :min e :max.',
        'file'    => 'Este campo deve estar entre :min e :max kilobytes.',
        'string'  => 'Este campo deve estar entre :min e :max caracteres.',
        'array'   => 'Este campo deve ter entre :min e :max itens.',
    ],
    'boolean'              => 'Este campo deve ser verdadeiro ou falso.',
    'confirmed'            => 'A confirmação deste campo não confere.',
    'date'                 => 'Este campo não é uma data válida.',
    'date_format'          => 'Este campo não confere com o formato :format.',
    'different'            => 'Este campo e :other devem ser diferentes.',
    'digits'               => 'Este campo deve ter :digits dígitos.',
    'digits_between'       => 'Este campo deve ter entre :min e :max dígitos.',
    'dimensions'           => 'Este campo tem dimensões de imagem inválidas.',
    'distinct'             => 'Este campo tem um valor duplicado.',
    'email'                => 'Este campo deve ser um endereço de e-mail válido.',
    'exists'               => 'Este campo selecionado é inválido.',
    'file'                 => 'Este campo deve ser um arquivo.',    
    'filled'               => 'Este campo é um campo obrigatório.',
    'image'                => 'Este campo deve ser uma imagem.',
    'in'                   => 'Este campo é inválido.',
    'in_array'             => 'Este campo não existe em :other.',
    'integer'              => 'Este campo deve ser um inteiro.',
    'ip'                   => 'Este campo deve ser um endereço IP válido.',
    'json'                 => 'Este campo deve ser um JSON válido.',
    'max'                  => [
        'numeric' => 'Este campo não deve ser maior que :max.',
        'file'    => 'Este campo não deve ter mais que :max kilobytes.',
        'string'  => 'Este campo não deve ter mais que :max caracteres.',
        'array'   => 'Este campo não deve ter mais que :max itens.',
    ],
    'mimes'                => 'Este campo deve ser um arquivo do tipo: :values.',
    'mimetypes'            => 'Este campo deve ser um arquivo do tipo: :values.',
    'min'                  => [
        'numeric' => 'Este campo deve ser no mínimo :min.',
        'file'    => 'Este campo deve ter no mínimo :min kilobytes.',
        'string'  => 'Este campo deve ter no mínimo :min caracteres.',
        'array'   => 'Este campo deve ter no mínimo :min itens.',
    ],
    'not_in'               => 'Este campo selecionado é inválido.',
    'numeric'              => 'Este campo deve ser um número.',
    'present'              => 'Este campo deve ser presente.',    
    'regex'                => 'O formato deste campo é inválido.',
    'required'             => 'Este campo é obrigatório.',
    'required_if'          => 'Este campo é obrigatório quando :other é :value.',
    'required_unless'      => 'Este campo é necessário a menos que :other esteja em :values.',
    'required_with'        => 'Este campo é obrigatório quando :values está presente.',
    'required_with_all'    => 'Este campo é obrigatório quando :values estão presentes.',
    'required_without'     => 'Este campo é obrigatório quando :values não está presente.',
    'required_without_all' => 'Este campo é obrigatório quando nenhum destes estão presentes: :values.',
    'same'                 => 'Este campo e :other devem ser iguais.',
    'size'                 => [
        'numeric' => 'Este campo deve ser :size.',
        'file'    => 'Este campo deve ter :size kilobytes.',
        'string'  => 'Este campo deve ter :size caracteres.',
        'array'   => 'Este campo deve conter :size itens.',
    ],
    'string'               => 'Este campo deve ser uma string',
    'timezone'             => 'Este campo deve ser uma timezone válida.',
    'unique'               => 'Este campo já está em uso.',
    'uploaded'             => 'Este campo falhou ao ser enviado.',    
    'url'                  => 'O formato deste campo é inválido.',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */
    'attributes' => [
    ],
];