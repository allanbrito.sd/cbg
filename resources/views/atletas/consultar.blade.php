@extends('layouts.app')

@include('layouts.vendor.datatable')

@section('title', 'Atletas')

@section('migalha')
@parent
<li class="active"><a href="{{ route('atleta.consultar') }}">Atletas</a></li>
<li><strong>Consultar</strong></li>
@endsection

@section('acoes')
<a class="btn btn-success" href="{{ route('atleta.novo') }}"><i class="fa fa-plus-square"></i> Novo</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="ibox">
				<div class="ibox-content">
					@if($atletas->count() > 0)
						@include('atletas.partials.tabela')
					@else
						@include('atletas.partials.sem_registro')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	@parent
	<script type="text/javascript">

		$(document).ready(function(){
			var $columns = $('thead th:not(.dt-no-export)', $('.tableAtleta'));
			var tableAtleta = $('.tableAtleta').DataTable({
				columnDefs: [
					{'aTargets': [1,2,4], 'bSortable': false},
					{'aTargets': [2,4], 'searchable': false}
				],
				buttons: [
					{
		            	extend: 'csv',
		            	text: 'CSV',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'excel',
		            	text: 'Excel',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'pdf',
		            	text: 'PDF',
		            	exportOptions: {
				            columns: $columns
				        }
		            }
		        ]
			});

			$(document).on('keyup', '.filtroAtleta', function() {
				tableAtleta.search($(this).val()).draw() ;
			});
		});

	</script>

@endsection
