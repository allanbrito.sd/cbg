<div class="row">
	<div class="col-md-12">
		<div class="input-group">
			<input type="text" placeholder="Pesquisar por nome ou cpf " class="input form-control filtroAtleta">
			<span class="input-group-btn">
				<button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i></button>
			</span>
		</div>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-sm-12">
		<div class="full-height-scroll">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover mt-0 tableAtleta">
					<thead>
						<tr>
							<th>Nome</th>
							<th>CPF</th>
							<th>Telefone</th>
							<th>Situação</th>
							<th class="dt-no-export">Ações</th>
						</tr>
					</thead>
					<tbody>
					@foreach($atletas as $atleta)
						<tr class="pointer visualizarAtleta">
							<td><img class="gravatar" src="{{ asset($atleta->pessoa->foto) }}"> {{ $atleta->pessoa->nomeESobrenome }}</td>
							<td> {{ $atleta->pessoa->cpf }}</td>
							<td><i class="fa fa-phone"> </i> {{ $atleta->pessoa->telefones ?? 'Não informado'}}</td>
							<td class="client-status"><span class="label label-primary">Ativo</span></td>
							<td class="text-center acoes" nowrap width="5%">
								<i class="fa fa-search fa-1-4x has-tooltip hidden" data-placement="bottom" title="Visualizar"></i>
								<a href="{{ route('atleta.editar', ['id' => $atleta->id]) }}"><i class="fa fa-edit fa-1-4x has-tooltip" data-placement="bottom" title="Editar"></i></a>
								<form method="POST" action="{{ route('atleta.apagar', ['id' => $atleta->id]) }}">
									@method('DELETE')
									<a role="submit"><i class="fa fa-1-4x fa-trash text-danger" data-placement="bottom" title="Apagar"></i></a>
								</form>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>