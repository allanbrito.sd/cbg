@extends('layouts.site')

@section('title', 'Entrar')

@section('content')
	<div class="middle-box loginscreen animated fadeInDown">
		<div>
			<h2 class="text-center">Acessar o sistema</h2>
			<br/>
			<form class="m-t" role="form" method="POST" action="{{ route('login') }}">
				{{ csrf_field() }}

				<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
					<label>Email</label>
					<input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus>
					@showError('email')
				</div>
				<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
					<label>Senha</label>
					<input id="password" type="password" class="form-control" name="password" placeholder="Senha" required>
					@showError('password')
				</div>
				<div class="form-group">
					<div class="checkbox checkbox-primary">
						<input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
						<label for="remember">
							Permanecer conectado
						</label>
					</div>
				</div>

				<button type="submit" class="btn btn-primary block full-width m-b">Login</button>

				<div class="form-group text-center">
					<a href="{{ route('password.request') }}">Esqueceu sua senha?</a>
				</div>
			</form>
		</div>
	</div>
{{-- <div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Login</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
						{{ csrf_field() }}

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email" class="col-md-4 control-label">E-Mail Address</label>

							<div class="col-md-6">
								<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

								@if ($errors->has('email'))
									<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<label for="password" class="col-md-4 control-label">Password</label>

							<div class="col-md-6">
								<input id="password" type="password" class="form-control" name="password" required>

								@if ($errors->has('password'))
									<span class="help-block">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-8 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Login
								</button>

								<a class="btn btn-link" href="{{ route('password.request') }}">
									Forgot Your Password?
								</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div> --}}
@endsection
