@extends('layouts.site')

@section('title', 'Registrar')


@section('content')
	<div class="middle-box loginscreen animated fadeInDown">
		<div>
			<h2 class="text-center">Conheça o sistema</h2>
			<br/>
			<form class="m-t" role="form" method="POST" action="{{ route('register') }}">
				{{ csrf_field() }}

				<div class="form-group{{ $errors->has('usuario.pessoa.nome') ? ' has-error' : '' }}">
					<label>Nome</label>
					<input type="text" class="form-control" name="usuario[pessoa][nome]" value="{{ old('usuario.pessoa.nome') }}" required autofocus>
					@showError('usuario.pessoa.nome')
				</div>
				<div class="form-group{{ $errors->has('usuario.email') ? ' has-error' : '' }}">
					<label>Email</label>
					<input type="email" class="form-control" name="usuario[email]" value="{{ old('usuario.email') }}" required autofocus>
					@showError('usuario.email')
				</div>
				<div class="form-group{{ $errors->has('usuario.password') ? ' has-error' : '' }}">
					<label>Senha</label>
					<input type="password" class="form-control" name="usuario[password]" required>
					@showError('usuario.password')
				</div>
				<div class="form-group{{ $errors->has('usuario.password_confirmation') ? ' has-error' : '' }}">
					<label>Confirmar Senha</label>
					<input type="password" class="form-control" name="usuario[password_confirmation]" required>
					@showError('usuario.password_confirmation')
				</div>

				<button type="submit" class="btn btn-primary block full-width m-b">Registrar</button>

				<div class="form-group text-center">
					<a href="{{ route('login') }}">Já possui uma conta?</a>
				</div>
			</form>
		</div>
	</div>
@endsection
