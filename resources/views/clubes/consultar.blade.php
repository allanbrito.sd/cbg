@extends('layouts.app')

@include('layouts.vendor.datatable')

@section('title', 'Clubes')

@section('migalha')
@parent
<li class="active"><a href="{{ route('clube.consultar') }}">Clubes</a></li>
<li><strong>Consultar</strong></li>
@endsection

@section('acoes')
<a class="btn btn-success" href="{{ route('clube.novo') }}"><i class="fa fa-plus-square"></i> Novo</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="ibox">
				<div class="ibox-content">
					@if($clubes->count() > 0)
						@include('clubes.partials.tabela')
					@else
						@include('clubes.partials.sem_registro')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	@parent
	<script type="text/javascript">

		$(document).ready(function(){
			var $columns = $('thead th:not(.dt-no-export)', $('.tableClube'));
			var tableClube = $('.tableClube').DataTable({
				columnDefs: [
					{'aTargets': [1,2,4], 'bSortable': false},
					{'aTargets': [2,4], 'searchable': false}
				],
				buttons: [
					{
		            	extend: 'csv',
		            	text: 'CSV',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'excel',
		            	text: 'Excel',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'pdf',
		            	text: 'PDF',
		            	exportOptions: {
				            columns: $columns
				        }
		            }
		        ]
			});

			$(document).on('keyup', '.filtroClube', function() {
				tableClube.search($(this).val()).draw() ;
			});
		});

	</script>

@endsection
