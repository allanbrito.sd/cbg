<div class="row m-b-lg">
	<div class="col-lg-4 text-center">
		<h2>{{ $clube->pessoa->nomeESobrenome }}</h2>

		<div class="m-b-sm">
			<img alt="image" class="img-circle" src="{{ asset($clube->pessoa->foto) }}"
				 style="width: 62px">
		</div>
	</div>
	<div class="col-lg-8">
		<!-- <strong>
			Resumo
		</strong> -->

		<address class="m-t-md text-left">
			<i class="fa fa-map-marker"></i> <strong>{{ $clube->pessoa->endereco->cidadeEstado ?? 'Nenhum informado' }}</strong><br>
			<i class="fa fa-phone"></i> {{ $clube->pessoa->contato->endereco->telefone->numero ?? 'Nenhum informado' }}<br/>
			<i class="fa fa-envelope"></i> <span class="clube_email" style="word-break: break-all">{{ $clube->email ?? 'Nenhum informado'}}</span><br/>
		</address>
		<button type="button" class="btn btn-primary btn-sm btn-block"><i
				class="fa fa-envelope"></i> Send Message
		</button>
	</div>
</div>
<div class="client-detail">
	<div class="full-height-scroll">

		<strong>Last activity</strong>

		<ul class="list-group clear-list">
			<li class="list-group-item fist-item">
				<span class="pull-right"> 09:00 pm </span>
				Please contact me
			</li>
			<li class="list-group-item">
				<span class="pull-right"> 10:16 am </span>
				Sign a contract
			</li>
			<li class="list-group-item">
				<span class="pull-right"> 08:22 pm </span>
				Open new shop
			</li>
			<li class="list-group-item">
				<span class="pull-right"> 11:06 pm </span>
				Call back to Sylvia
			</li>
			<li class="list-group-item">
				<span class="pull-right"> 12:00 am </span>
				Write a letter to Sandra
			</li>
		</ul>
		<strong>Notes</strong>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua.
		</p>
		<hr/>
		<strong>Timeline activity</strong>
		<div id="vertical-timeline" class="vertical-container dark-timeline">
			<div class="vertical-timeline-block">
				<div class="vertical-timeline-icon gray-bg">
					<i class="fa fa-coffee"></i>
				</div>
				<div class="vertical-timeline-content">
					<p>Conference on the sales results for the previous year.
					</p>
					<span class="vertical-date small text-muted"> 2:10 pm - 12.06.2014 </span>
				</div>
			</div>
			<div class="vertical-timeline-block">
				<div class="vertical-timeline-icon gray-bg">
					<i class="fa fa-briefcase"></i>
				</div>
				<div class="vertical-timeline-content">
					<p>Many desktop publishing packages and web page editors now use Lorem.
					</p>
					<span class="vertical-date small text-muted"> 4:20 pm - 10.05.2014 </span>
				</div>
			</div>
			<div class="vertical-timeline-block">
				<div class="vertical-timeline-icon gray-bg">
					<i class="fa fa-bolt"></i>
				</div>
				<div class="vertical-timeline-content">
					<p>There are many variations of passages of Lorem Ipsum available.
					</p>
					<span class="vertical-date small text-muted"> 06:10 pm - 11.03.2014 </span>
				</div>
			</div>
			<div class="vertical-timeline-block">
				<div class="vertical-timeline-icon navy-bg">
					<i class="fa fa-warning"></i>
				</div>
				<div class="vertical-timeline-content">
					<p>The generated Lorem Ipsum is therefore.
					</p>
					<span class="vertical-date small text-muted"> 02:50 pm - 03.10.2014 </span>
				</div>
			</div>
			<div class="vertical-timeline-block">
				<div class="vertical-timeline-icon gray-bg">
					<i class="fa fa-coffee"></i>
				</div>
				<div class="vertical-timeline-content">
					<p>Conference on the sales results for the previous year.
					</p>
					<span class="vertical-date small text-muted"> 2:10 pm - 12.06.2014 </span>
				</div>
			</div>
			<div class="vertical-timeline-block">
				<div class="vertical-timeline-icon gray-bg">
					<i class="fa fa-briefcase"></i>
				</div>
				<div class="vertical-timeline-content">
					<p>Many desktop publishing packages and web page editors now use Lorem.
					</p>
					<span class="vertical-date small text-muted"> 4:20 pm - 10.05.2014 </span>
				</div>
			</div>
		</div>
	</div>
</div>
