<div class="row">
	<div class="col-md-12">
		<div class="input-group">
			<input type="text" placeholder="Pesquisar por nome ou cnpj " class="input form-control filtroClube">
			<span class="input-group-btn">
				<button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i></button>
			</span>
		</div>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-sm-12">
		<div class="full-height-scroll">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover mt-0 tableClube">
					<thead>
						<tr>
							<th>Nome</th>
							<th>CNPJ</th>
							<th>Telefone</th>
							<th>Situação</th>
							<th class="dt-no-export">Ações</th>
						</tr>
					</thead>
					<tbody>
					@foreach($clubes as $clube)
						<tr class="pointer visualizarClube">
							<td><img class="gravatar" src="{{ asset($clube->pessoa->foto) }}"> {{ $clube->pessoa->nomeESobrenome }}</td>
							<td> {{ $clube->pessoa->cnpj }}</td>
							<td><i class="fa fa-phone"> </i> {{ $clube->pessoa->telefones ?? 'Não informado'}}</td>
							<td class="client-status"><span class="label label-primary">Ativo</span></td>
							<td class="text-center acoes" nowrap width="5%">
								<i class="fa fa-search fa-1-4x has-tooltip hidden" data-placement="bottom" title="Visualizar"></i>
								<a href="{{ route('clube.editar', ['id' => $clube->id]) }}"><i class="fa fa-edit fa-1-4x has-tooltip" data-placement="bottom" title="Editar"></i></a>
								<form method="POST" action="{{ route('clube.apagar', ['id' => $clube->id]) }}">
									@method('DELETE')
									<a role="submit"><i class="fa fa-1-4x fa-trash text-danger" data-placement="bottom" title="Apagar"></i></a>
								</form>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>