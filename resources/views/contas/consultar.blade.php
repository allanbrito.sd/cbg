@extends('layouts.app')

@include('layouts.vendor.datatable')

@section('title', ucfirst(__('modulos.contas')))

@section('migalha')
@parent
<li class="active"><a href="{{ route('conta.consultar') }}">{{ ucfirst(__('modulos.contas')) }}</a></li>
<li><strong>Consultar</strong></li>
@endsection

@section('acoes')
<a class="btn btn-success" href="{{ route('conta.novo') }}"><i class="fa fa-plus-square"></i> Novo</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="ibox">
				<div class="ibox-content">
					@if($contas->count() > 0)
						@include('contas.partials.tabela')
					@else
						@include('contas.partials.sem_registro')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	@parent
	<script type="text/javascript">

		$(document).ready(function(){
			var $columns = $('thead th:not(.dt-no-export)', $('.tableConta'));
			var tableConta = $('.tableConta').DataTable({
				columnDefs: [
					{'aTargets': [1,2], 'bSortable': false},
					{'aTargets': [1,2], 'searchable': false}
				],
				buttons: [
					{
		            	extend: 'csv',
		            	text: 'CSV',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'excel',
		            	text: 'Excel',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'pdf',
		            	text: 'PDF',
		            	exportOptions: {
				            columns: $columns
				        }
		            }
		        ]
		
			});

			$(document).on('keyup', '.filtroConta', function() {
				tableConta.search($(this).val()).draw() ;
			});
		});

	</script>

@endsection
