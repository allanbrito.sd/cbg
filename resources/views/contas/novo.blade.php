@extends('layouts.app')

@section('title', "Cadastrar ".__('modulos.conta'))

@section('migalha')
@parent
<li><a href="{{ route('conta.consultar') }}">{{ ucfirst(__('modulos.contas')) }}</a></li>
<li class="active"><strong>Novo</strong></li>
@endsection

@section('acoes')
<a class="btn" href="{{ route('conta.consultar') }}"><i class="fa fa-mail-reply"></i> Voltar</a>
<a class="btn btn-success" role="submit" href="{{ route('conta.cadastrar') }}"><i class="fa fa-save"></i> Salvar</a>
@endsection

@section('content')
<form role="form" method="POST" action="{{ route('conta.cadastrar') }}">
	@method('POST')

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content">
						@define($prefixo = 'formulario')
						@include('contas.partials.formulario')
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

