@define($conta = $conta ?? null)
@define($prefixoInput = $prefixo ?? null)
@define($prefixo = $prefixo ?? null)

<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-body">
		<div class="row">
	        <input type="hidden" name="{{ $prefixoInput }}[conta][id]" value="{{ old($prefixo.'conta.id') ?? $conta->id ?? '' }}">
	        <div class="form-group col-md-3{{ $errors->has($prefixo.'conta.nome') ? ' has-error' : '' }}">
	            <label class="control-label">Apelido da conta <span style="color:#f00">*</span></label>
	            <input type="text" class="form-control" name="{{ $prefixoInput }}[conta][nome]" value="{{ old($prefixo.'conta.nome') ?? $conta->nome ?? '' }}">
	            @showError($prefixo.'conta.nome')
	        </div>
		</div>

		<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
			<div class="widget-header">
				<h2 class="widget-title lighter">Dados Bancários</h2>
			</div>
			<div class="widget-body">
				<div class="widget-main padding-6 no-padding-left no-padding-right">
					@define($dadosBancarios = $conta->dadosBancarios ?? null)
		            @include('pessoas.partials.dados_bancarios')
				</div>
			</div>
		</div>
	</div>
</div>