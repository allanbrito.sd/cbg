<div class="row">
	<div class="col-md-12">
		<div class="input-group">
			<input type="text" placeholder="Pesquisar por nome" class="input form-control filtroConta">
			<span class="input-group-btn">
				<button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i></button>
			</span>
		</div>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-sm-12">
		<div class="full-height-scroll">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover mt-0 tableConta">
					<thead>
						<tr>
							<th>Conta</th>
							<th class="dt-no-export">Ações</th>
						</tr>
					</thead>
					<tbody>
					@foreach($contas as $conta)
						<tr class="pointer visualizarConta">
							<td>{{ $conta->nome }}</td>
							<td class="text-center acoes" nowrap width="5%">
								<i class="fa fa-search fa-1-4x has-tooltip hidden" data-placement="bottom" title="Visualizar"></i>
								<a href="{{ route('conta.editar', ['id' => $conta->id]) }}"><i class="fa fa-edit fa-1-4x has-tooltip" data-placement="bottom" title="Editar"></i></a>
								<form method="POST" action="{{ route('conta.apagar', ['id' => $conta->id]) }}">
									@method('DELETE')
									<a role="submit"><i class="fa fa-1-4x fa-trash text-danger" data-placement="bottom" title="Apagar"></i></a>
								</form>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>