@extends('layouts.app')

@section('title', 'Perfil de '.$conta->pessoa->nomeESobrenome)

@section('migalha')
@parent
<li><a href="{{ route('conta.consultar') }}">{{ ucfirst(__('modulos.contas')) }}</a></li>
<li><a href="{{ route('conta.visualizar', ['id' => $conta->id]) }}">{{ $conta->pessoa->nomeESobrenome }}</a></li>
<li class="active"><strong>Perfil</strong></li>
@endsection

@section('acoes')
<a class="btn" href="{{ route('conta.consultar') }}"><i class="fa fa-mail-reply"></i> Voltar</a>
	<form  method="POST" action="{{ route('conta.apagar', ['id' => $conta->id]) }}">
		@method('DELETE')
		<a class="btn btn-danger" role="submit"><i class="fa fa-trash"></i> Apagar</a>
	</form>
<a class="btn btn-success" href="{{ route('conta.editar', ['id' => $conta->id]) }}"><i class="fa fa-edit"></i> Editar</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row m-b-sm m-t-md">
		<div class="col-md-6">

			<div class="profile-image">
				<img src="{{ asset($conta->pessoa->foto)}}" class="img-circle circle-border m-b-md" alt="profile">
			</div>
			<div class="profile-info">
				<div class="">
					<div>
						<h2 class="no-margins">
							{{ $conta->pessoa->nomeESobrenome }}
						</h2>
						<h4><i class="fa fa-circle text-navy"></i> Online a {{ $conta->created_at->diffForHumans() }}</h4>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<table class="table small m-b-xs">
				<tbody>
				<tr>
					<td>
						<strong>142</strong> Vendas concretizadas
					</td>

				</tr>
				<tr>
					<td>
						<strong>13</strong> Orçamentos em aberto
					</td>

				</tr>
				<tr>
					<td>
						<strong>22</strong> Contas ativos
					</td>
				</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-3">
			<small>Vendas da última semana</small>
			<h2 class="no-margins">R$ 206.480,00</h2>
			<div id="sparkline1"></div>
		</div>
		<div class="col-md-3">
			<small>Comissão do mês passado</small>
			<h2 class="no-margins">R$ 2.480,00</h2>
			<div id="sparkline1"></div>
		</div>


	</div>
	<div class="row">

		<div class="col-lg-3">
			<div class="ibox">
				<div class="ibox-content">
					<h3>Mensagem</h3>

					<div class="form-group">
						<label>Assunto</label>
						<input type="email" class="form-control">
					</div>
					<div class="form-group">
						<label>Mensagem</label>
						<textarea class="form-control" rows="3"></textarea>
					</div>
					<button class="btn btn-primary btn-block">Enviar</button>
				</div>
			</div>

			<div class="ibox">
				<div class="ibox-content">
					<h3>Contas</h3>
					{{-- <p class="small">
						If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't
						anything embarrassing
					</p> --}}
					<div class="user-friends">
						<a href=""><img alt="image" class="img-circle" src="{{ asset('images/user-no-photo.jpg') }}"></a>
						<a href=""><img alt="image" class="img-circle" src="{{ asset('images/user-no-photo.jpg') }}"></a>
						<a href=""><img alt="image" class="img-circle" src="{{ asset('images/user-no-photo.jpg') }}"></a>
						<a href=""><img alt="image" class="img-circle" src="{{ asset('images/user-no-photo.jpg') }}"></a>
						<a href=""><img alt="image" class="img-circle" src="{{ asset('images/user-no-photo.jpg') }}"></a>
						<a href=""><img alt="image" class="img-circle" src="{{ asset('images/user-no-photo.jpg') }}"></a>
						<a href=""><img alt="image" class="img-circle" src="{{ asset('images/user-no-photo.jpg') }}"></a>
						<a href=""><img alt="image" class="img-circle" src="{{ asset('images/user-no-photo.jpg') }}"></a>
						<a href=""><img alt="image" class="img-circle" src="{{ asset('images/user-no-photo.jpg') }}"></a>
						<a href=""><img alt="image" class="img-circle" src="{{ asset('images/user-no-photo.jpg') }}"></a>
					</div>
				</div>
			</div>

		</div>

		<div class="col-lg-5">

			<div class="ibox">
				<div class="ibox-content">
					<h3>Gráfico</h3>
					Muitos gráficos
				</div>
			</div>

			<div class="ibox">
				<div class="ibox-content">
					<h3>Gráfico</h3>
					Melhor gráfico
				</div>
			</div>

			<div class="ibox">
				<div class="ibox-content">
					<h3>Gráfico</h3>
					Todos gráfico
				</div>
			</div>
		</div>
		<div class="col-lg-4 m-b-lg">
			<div id="vertical-timeline" class="vertical-container light-timeline no-margins">
				<div class="vertical-timeline-block">
					<div class="vertical-timeline-icon navy-bg">
						<i class="fa fa-briefcase"></i>
					</div>

					<div class="vertical-timeline-content">
						<h2>Agenda</h2>
						<p>Não há compromissos marcados
						</p>
						<a href="#" class="btn btn-sm btn-primary"> Cadastrar</a>
							<span class="vertical-date">
								Today <br>
								<small>Dec 24</small>
							</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
