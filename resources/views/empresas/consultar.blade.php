@extends('layouts.app')

@include('layouts.vendor.datatable')

@section('title', 'Empresas')

@section('migalha')
@parent
<li class="active"><a href="{{ route('empresa.consultar') }}">Empresas</a></li>
<li><strong>Consultar</strong></li>
@endsection

@section('acoes')
<a class="btn btn-success" href="{{ route('empresa.novo') }}"><i class="fa fa-plus-square"></i> Novo</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="ibox">
				<div class="ibox-content">
					@if($empresas->count() > 0)
						@include('empresas.partials.tabela')
					@else
						@include('empresas.partials.sem_registro')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	@parent
	<script type="text/javascript">

		$(document).ready(function(){
			var tableEmpresa = $('.tableEmpresa').DataTable({
				columnDefs: [
					{'aTargets': [1,2,4], 'bSortable': false},
					{'aTargets': [2,4], 'searchable': false}
				]
			});

			$(document).on('keyup', '.filtroEmpresa', function() {
				tableEmpresa.search($(this).val()).draw() ;
			});
		});

	</script>

@endsection
