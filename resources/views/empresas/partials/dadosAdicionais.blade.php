@define($prefixoInput = !empty($prefixo) ? $prefixo  : '')
@define($prefixo = !empty($prefixo) ? $prefixo.'.'  : '')

<div class="contato_container">
	<div class="row">
		<div class="form-group col-md-3{{ $errors->has($prefixo.'categoria') ? ' has-error' : '' }}">
			<label class="control-label">Categoria / Classificação</label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[categoria]" value="{{ old($prefixo.'categoria') ?? $empresa->categoria ?? '' }}">
			@showError($prefixo.'categoria')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'comissionamento') ? ' has-error' : '' }}">
			<label class="control-label">Vendedor</label>
			<input i type="text" class="form-control" name="{{ $prefixoInput }}[comissionamento]" value="{{ old($prefixo.'comissionamento') ?? $empresa->comissionamento ?? '' }}">
			@showError($prefixo.'comissionamento')
		</div>
		<div class="form-group col-md-3{{ $errors->has($prefixo.'comissionamento') ? ' has-error' : '' }}">
			<label class="control-label">Início da atividade</label>
			<input i type="text" class="form-control" name="{{ $prefixoInput }}[comissionamento]" value="{{ old($prefixo.'comissionamento') ?? $empresa->comissionamento ?? '' }}">
			@showError($prefixo.'comissionamento')
		</div>
		<div class="form-group col-md-3{{ $errors->has($prefixo.'comissionamento') ? ' has-error' : '' }}">
			<label class="control-label">Final da atividade</label>
			<input i type="text" class="form-control" name="{{ $prefixoInput }}[comissionamento]" value="{{ old($prefixo.'comissionamento') ?? $empresa->comissionamento ?? '' }}">
			@showError($prefixo.'comissionamento')
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-12{{ $errors->has($prefixo.'observacoes') ? ' has-error' : '' }}">
			<label class="control-label">Observações</label>
			<textarea class="form-control" name="{{ $prefixoInput }}[observacoes]">{{ old($prefixo.'observacoes') ?? $empresa->observacoes ?? '' }}</textarea>
			@showError($prefixo.'observacoes')
		</div>
	</div>
</div>