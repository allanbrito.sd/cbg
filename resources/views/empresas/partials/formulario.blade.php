<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-header">
		<h2 class="widget-title lighter">Dados pessoais</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			@define($pessoa = $empresa ?? null)
			@define($pessoaOnly = $pessoaTipos->search('Jurídica'))
			@define($pessoaWith = ['inscricao_municipal' => true, 'inscricao_estadual' => true])
			@include('pessoas.partials.fisicaOuJuridica')
		</div>
	</div>
</div>

<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-header">
		<h2 class="widget-title lighter">Endereço</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			@define($endereco = $empresa->pessoa->endereco ?? null)
			@include('pessoas.partials.endereco')
		</div>
	</div>
</div>