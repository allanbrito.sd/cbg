@extends('layouts.app')

@section('title', 'Equipe')

@section('migalha')
@parent
<li><a href="{{ route('equipes') }}">Equipe</a></li>
<li class="active"><strong>Novo</strong></li>
@endsection

@section('acoes')
<a class="btn" href="{{ route('clientes') }}"><i class="fa fa-mail-reply"></i> Voltar</a>
<a class="btn btn-success" href="{{ route('cliente.novo') }}"><i class="fa fa-save"></i> Cadastrar</a>
@endsection

@section('content')
<div class="container">

</div>
@endsection

