@extends('layouts.pesquisa')

@section('title', 'Entrar')

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading no-padding">
		<div class="col-md-8 col-md-offset-2">
			<div class="simple-steps text-center">
				<div class="simple-progress">
					<div class="simple-progress-line" style="width: 37%;"></div>
				</div>
				<div class="simple-step activated">
					<div class="simple-step-icon"><i class="fa fa-user"></i></div>
					<p>{{ Auth::user()->pessoa->primeiroNome }}</p>
				</div>
				<div class="simple-step activated">
					<div class="simple-step-icon"><i class="fa fa-bank"></i></div>
					<p>Empresa</p>
				</div>
				<div class="simple-step active">
					<div class="simple-step-icon"><i class="fa fa-suitcase"></i></div>
					<p>Atividade</p>
				</div>
				<div class="simple-step">
					<div class="simple-step-icon"><i class="fa fa-users"></i></div>
					<p>Equipe</p>
				</div>
			</div>
		</div>
	</div>

	<br/>
	<h2 class="text-center">Qual o tipo de atividade da sua empresa?</h2>
	<br/>
	<form class="m-t" role="form" method="POST" action="{{ route('login') }}">
		{{ csrf_field() }}

		<div class="row equal" style="padding: 0 30px">

			<div class="col-md-3">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3>Representação comercial</h3>
					</div>
					<div class="panel-body">
						<p>Tenho uma carteira de clientes e presento comercialmente uma ou mais marcas</p>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3>Distribuição</h3>
					</div>
					<div class="panel-body">
						<p>Compro e revendo produtos de uma ou mais marcas, vendo produtos no atacado</p>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3>Indústria / Manufatura</h3>
					</div>
					<div class="panel-body">
						<p>Fabrico produtos em geral</p>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3>Outros</h3>
					</div>
					<div class="panel-body">
						<p>Sou vendedor consignado, prestador de serviços, consultor, varejista e afins</p>
					</div>
				</div>
			</div>
		</div>
	</form>
@endsection

{{-- @include('layouts.vendor.steps') --}}

@section('styles')
	@parent
	<style type="text/css">

	.simple-steps {
		overflow: hidden;
		position: relative;
		margin-top: 20px;
	}

	.simple-progress {
		position: absolute;
		top: 24px;
		left: 0;
		width: 100%;
		height: 1px;
		background: #ddd;
	}

	.simple-progress-line {
		position: absolute;
		top: 0;
		left: 0;
		height: 1px;
		background: #48a867;
	}

	.simple-step {
		position: relative;
		float: left;
		width: 25%;
		padding: 0 5px;
	}

	.simple-step-icon {
		display: inline-block;
		width: 40px;
		height: 40px;
		margin-top: 4px;
		background: #ddd;
		font-size: 16px;
		color: #fff;
		line-height: 40px;
		-moz-border-radius: 50%;
		-webkit-border-radius: 50%;
		border-radius: 50%;
	}

	.simple-step.activated .simple-step-icon {
		background: #fff;
		border: 1px solid #48a867;
		color: #48a867;
		line-height: 38px;
	}

	.simple-step.active .simple-step-icon {
		width: 48px;
		height: 48px;
		margin-top: 0;
		background: #48a867;
		font-size: 22px;
		line-height: 48px;
	}


	</style>
@endsection
