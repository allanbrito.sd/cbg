@extends('layouts.pesquisa')

@section('title', 'Entrar')

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading no-padding">
		<div class="col-md-8 col-md-offset-2">
			<div class="simple-steps text-center">
				<div class="simple-progress">
					<div class="simple-progress-line" style="width: 37.5%;"></div>
				</div>
				<div class="simple-step activated">
					<div class="simple-step-icon"><i class="fa fa-user"></i></div>
					<p>{{ Auth::user()->pessoa->primeiroNome }}</p>
				</div>
				<div class="simple-step active">
					<div class="simple-step-icon"><i class="fa fa-suitcase"></i></div>
					<p>Atividade</p>
				</div>
				<div class="simple-step">
					<div class="simple-step-icon"><i class="fa fa-users"></i></div>
					<p>Equipe</p>
				</div>
				<div class="simple-step">
					<div class="simple-step-icon"><i class="fa fa-bank"></i></div>
					<p>Empresa</p>
				</div>
			</div>
		</div>
	</div>

	<br/>
	<h2 class="text-center">Qual o tipo de atividade da sua empresa?</h2>
	<br/>
	<form class="m-t" role="form" method="POST" action="{{ route('equipes.cadastrar') }}">
		{{ csrf_field() }}

		<input type="hidden" name="nova_empresa[atividade_id]" id="atividade_id" />

		<div class="row equal" style="padding: 0 30px">
			@foreach($atividades as $atividade)
				<div class="col-md-3">
					<div class="panel panel-primary pointer" onClick="$('#atividade_id').val({{ $atividade->id }}); $('form').submit();">
						<div class="panel-heading">
							<h3>{{ $atividade->nome }}</h3>
						</div>
						<div class="panel-body">
							<p>{{ $atividade->descricao }}</p>
						</div>
					</div>
				</div>

				@if($loop->iteration % 4 == 0)
					<div class="col-md-12 hidden-xs hidden-sm">
						&nbsp;
					</div>
				@endif
			@endforeach
		</div>
	</form>
@endsection

@include('layouts.vendor.simplestep')
