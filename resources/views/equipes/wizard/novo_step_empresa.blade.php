@extends('layouts.pesquisa')

@section('title', 'Entrar')

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading no-padding">
		<div class="col-md-8 col-md-offset-2">
			<div class="simple-steps text-center">
				<div class="simple-progress">
					<div class="simple-progress-line" style="width: 87.5%;"></div>
				</div>
				<div class="simple-step activated">
					<div class="simple-step-icon"><i class="fa fa-user"></i></div>
					<p>{{ Auth::user()->pessoa->primeiroNome }}</p>
				</div>
				<div class="simple-step activated">
					<div class="simple-step-icon pointer" onClick="$('#atividade_id').val(''); $('#numero_vendedores_id').val(''); $('form').submit();">
						<i class="fa fa-suitcase"></i>
					</div>
					<p>Atividade</p>
				</div>
				<div class="simple-step activated">
					<div class="simple-step-icon pointer" onClick="$('#numero_vendedores_id').val(''); $('form').submit();">
						<i class="fa fa-users"></i>
					</div>
					<p>Equipe</p>
				</div>
				<div class="simple-step active">
					<div class="simple-step-icon"><i class="fa fa-bank"></i></div>
					<p>Empresa</p>
				</div>
			</div>
		</div>
	</div>

	<br/>
	<h2 class="text-center">Como podemos chamar sua empresa?</h2>
	<br/>
	<div class="middle-box loginscreen animated fadeInDown no-padding">
	<form class="m-t" role="form" method="POST" action="{{ route('equipes.cadastrar') }}">
		{{ csrf_field() }}

		<input type="hidden" name="nova_empresa[atividade_id]" id="atividade_id" value="{{ $novaEmpresa['atividade_id'] }}"/>
		<input type="hidden" name="nova_empresa[numero_vendedores_id]" id="numero_vendedores_id" value="{{ $novaEmpresa['numero_vendedores_id'] }}"/>
		<input type="hidden" name="nova_empresa[pessoa][tipo]" value="{{ $pessoaJuridica }}">

		<div class="form-group{{ $errors->has('nova_empresa.pessoa.nome') ? ' has-error' : '' }}">
			<label>Nome</label>
			<input id="nome" type="text" class="form-control" name="nova_empresa[pessoa][nome]" value="{{ old('nova_empresa.pessoa.nome') }}" required autofocus>
			@if ($errors->has('nova_empresa.pessoa.nome'))
				<span class="help-block">
					<strong>{{ $errors->first('nova_empresa.pessoa.nome') }}</strong>
				</span>
			@endif
		</div>
		<div class="form-group{{ $errors->has('nova_empresa.pessoa.razao_social') ? ' has-error' : '' }}">
			<label>Razão Social</label>
			<input id="razao_social" type="text" class="form-control" name="nova_empresa[pessoa][razao_social]" value="{{ old('nova_empresa.pessoa.razao_social') }}" autofocus>
			@if ($errors->has('nova_empresa.pessoa.razao_social'))
				<span class="help-block">
					<strong>{{ $errors->first('nova_empresa.pessoa.razao_social') }}</strong>
				</span>
			@endif
		</div>
		<div class="form-group{{ $errors->has('nova_empresa.pessoa.cnpj') ? ' has-error' : '' }}">
			<label>CNPJ</label>
			<input id="cnpj" type="text" class="form-control" name="nova_empresa[pessoa][cnpj]" value="{{ old('nova_empresa.pessoa.cnpj') }}" autofocus>
			@if ($errors->has('nova_empresa.pessoa.cnpj'))
				<span class="help-block">
					<strong>{{ $errors->first('nova_empresa.pessoa.cnpj') }}</strong>
				</span>
			@endif
		</div>

		<br/>

		<button type="submit" class="btn btn-primary block full-width m-b">Ir para o sistema</button>
	</form>
	</div>
@endsection

{{-- @include('layouts.vendor.steps') --}}

@section('styles')
	@parent
	<style type="text/css">

	.simple-steps {
		overflow: hidden;
		position: relative;
		margin-top: 20px;
	}

	.simple-progress {
		position: absolute;
		top: 24px;
		left: 0;
		width: 100%;
		height: 1px;
		background: #ddd;
	}

	.simple-progress-line {
		position: absolute;
		top: 0;
		left: 0;
		height: 1px;
		background: #48a867;
	}

	.simple-step {
		position: relative;
		float: left;
		width: 25%;
		padding: 0 5px;
	}

	.simple-step-icon {
		display: inline-block;
		width: 40px;
		height: 40px;
		margin-top: 4px;
		background: #ddd;
		font-size: 16px;
		color: #fff;
		line-height: 40px;
		-moz-border-radius: 50%;
		-webkit-border-radius: 50%;
		border-radius: 50%;
	}

	.simple-step.activated .simple-step-icon {
		background: #fff;
		border: 1px solid #48a867;
		color: #48a867;
		line-height: 38px;
	}

	.simple-step.active .simple-step-icon {
		width: 48px;
		height: 48px;
		margin-top: 0;
		background: #48a867;
		font-size: 22px;
		line-height: 48px;
	}


	</style>
@endsection
