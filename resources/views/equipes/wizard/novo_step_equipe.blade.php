@extends('layouts.pesquisa')

@section('title', 'Entrar')

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading no-padding">
		<div class="col-md-8 col-md-offset-2">
			<div class="simple-steps text-center">
				<div class="simple-progress">
					<div class="simple-progress-line" style="width: 62.5%;"></div>
				</div>
				<div class="simple-step activated">
					<div class="simple-step-icon"><i class="fa fa-user"></i></div>
					<p>{{ Auth::user()->pessoa->primeiroNome }}</p>
				</div>
				<div class="simple-step activated">
					<div class="simple-step-icon pointer" onClick="$('#atividade_id').val(''); $('form').submit();">
						<i class="fa fa-suitcase"></i>
					</div>
					<p>Atividade</p>
				</div>
				<div class="simple-step active">
					<div class="simple-step-icon"><i class="fa fa-users"></i></div>
					<p>Equipe</p>
				</div>
				<div class="simple-step">
					<div class="simple-step-icon"><i class="fa fa-bank"></i></div>
					<p>Empresa</p>
				</div>
			</div>
		</div>
	</div>

	<br/>
	<h2 class="text-center">Quantos vendedores sua empresa possui?</h2>
	<br/>
	<form class="m-t" role="form" method="POST" action="{{ route('equipes.cadastrar') }}">
		{{ csrf_field() }}

		<input type="hidden" name="nova_empresa[atividade_id]" id="atividade_id" value="{{ $novaEmpresa['atividade_id'] }}"/>
		<input type="hidden" name="nova_empresa[numero_vendedores_id]" id="numero_vendedores_id" />

		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				@foreach($numeroVendedores as $vendedores)
					<button type="submit" class="btn btn-default  block full-width m-b text-left"
						onClick="$('#numero_vendedores_id').val({{ $vendedores->id }}); $('form').submit()"
						style="text-align: left;padding: 18px; border-radius: 0;margin-bottom: -1px;{{ $loop->first ? 'border-radius: 5px 5px 0 0;' : '' }}{{ $loop->last ? 'border-radius: 0 0 5px 5px;' : '' }}">
						<i class="fa fa-users" style="font-size: 125%"></i> &nbsp; &nbsp; {{ $vendedores->quantidade }}
					</button>
				@endforeach
			</div>
		</div>
	</form>
@endsection

@include('layouts.vendor.simplestep')
