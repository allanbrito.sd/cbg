@extends('layouts.app')

@section('title', 'Página não encontrada')

@section('migalha')
@parent
<li class="active"><strong>Página não encontrada</strong></li>
@endsection

@section('content')
<div class="middle-box text-center animated fadeInDown">
	<h1>404</h1>
	<h3 class="font-bold">Desculpe, a página não foi encontrada.</h3>

		<div class="error-desc">
			O sistema ainda está em desenvolvimento e é normal que várias funcionalidades ainda não estejam disponíveis para uso.
			<form class="form-inline m-t hidden" role="form">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Search for page">
				</div>
				<button type="submit" class="btn btn-primary">Search</button>
			</form>
		</div>
	</div>
</div>
@endsection
