@if ($errors->has($errorFor))
	<span class="help-block">
		<strong>{{ $errors->first($errorFor) }}</strong>
	</span>
@endif