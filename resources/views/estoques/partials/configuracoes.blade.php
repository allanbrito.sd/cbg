@define($prefixoInput = !empty($prefixo) ? $prefixo.'[estoqueConfiguracao]'  : 'estoqueConfiguracao')
@define($prefixo = !empty($prefixo) ? $prefixo.'.estoqueConfiguracao.'  : 'estoqueConfiguracao.')

<div class="estoqueConfiguracao_container">
	<div class="row">
		<div class="form-group col-md-3{{ $errors->has($prefixo.'unidade') ? ' has-error' : '' }}">
			<label class="control-label">Unidade</label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[unidade]" value="{{ old($prefixo.'unidade') ?? $estoqueConfiguracao->unidade ?? '' }}" placeholder = "Ex.: Kg, Cx, Un, etc.">
			@showError($prefixo.'unidade')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'minimo') ? ' has-error' : '' }}">
			<label class="control-label">Estoque mínimo</label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[minimo]" value="{{ old($prefixo.'minimo') ?? $estoqueConfiguracao->minimo ?? '' }}">
			@showError($prefixo.'minimo')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'maximo') ? ' has-error' : '' }}">
			<label class="control-label">Estoque máximo</label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[maximo]" value="{{ old($prefixo.'maximo') ?? $estoqueConfiguracao->maximo ?? '' }}">
			@showError($prefixo.'maximo')
		</div>
	</div>
</div>