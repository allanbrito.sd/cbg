@extends('layouts.app')

@section('title', 'Editar '.__('modulos.evento'))

@section('migalha')
@parent
<li><a href="{{ route('evento.consultar') }}">{{ ucfirst(__('modulos.eventos')) }}</a></li>
<li><a href="{{ route('evento.visualizar', ['id' => $evento->id]) }}">{{ $evento->nome }}</a></li>
<li class="active"><strong>Editar</strong></li>
@endsection

@section('acoes')
<a class="btn" href="{{ route('evento.consultar') }}"><i class="fa fa-mail-reply"></i> Voltar</a>
<a class="btn btn-success" role="submit" href="{{ route('evento.atualizar', ['id' => $evento->id]) }}"><i class="fa fa-save"></i> Salvar</a>
@endsection

@section('content')
<form role="form" method="POST" action="{{ route('evento.atualizar', ['id' => $evento->id]) }}">
	@method('PUT')

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content">
						@define($prefixo = 'formulario')
						@include('eventos.partials.formulario')
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

