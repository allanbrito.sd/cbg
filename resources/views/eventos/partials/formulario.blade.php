@define($evento = $evento ?? null)
@define($prefixoInput = !empty($prefixo) ? $prefixo.'[evento]'  : 'evento')
@define($prefixo = !empty($prefixo) ? $prefixo.'.evento.'  : 'evento.')

<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-body">
		<div class="row">
	        <input type="hidden" name="{{ $prefixoInput }}[id]" value="{{ old($prefixo.'evento.id') ?? $evento->id ?? '' }}">
	        <div class="form-group col-md-3{{ $errors->has($prefixo.'evento.nome') ? ' has-error' : '' }}">
	            <label class="control-label">Nome <span style="color:#f00">*</span></label>
	            <input type="text" class="form-control" name="{{ $prefixoInput }}[nome]" value="{{ old($prefixo.'evento.nome') ?? $evento->nome ?? '' }}">
	            @showError($prefixo.'evento.nome')
	        </div>

			<div class="form-group col-md-3{{ $errors->has($prefixo.'data_inicio') || $errors->has($prefixo.'data_fim') ? ' has-error' : '' }}">
				<label class="control-label">Período <span style="color:#f00">*</span></label>
				<div class="daterange input-group">
					<input type="text" class="form-control data" name="{{ $prefixoInput }}[data_inicio]" value="{{ old($prefixo.'data_inicio') ?? (!empty($evento->data_inicio) ? $evento->data_inicio->format('d/m/Y') : null) ?? '' }}">
	                <span class="input-group-addon">até</span>
					<input type="text" class="form-control data" name="{{ $prefixoInput }}[data_fim]" value="{{ old($prefixo.'data_fim') ?? (!empty($evento->data_fim) ? $evento->data_fim->format('d/m/Y') : null) ?? '' }}">
	            </div>
				@showError($prefixo.'data_inicio')
				@showError($prefixo.'data_fim')
			</div>

			<div class="form-group col-md-6{{ $errors->has($prefixo.'evento.local') ? ' has-error' : '' }}">
	            <label class="control-label">Local</label>
	            <input type="text" class="form-control" name="{{ $prefixoInput }}[local]" value="{{ old($prefixo.'evento.local') ?? $evento->local ?? '' }}">
	            @showError($prefixo.'evento.local')
	        </div>
		</div>
	</div>
</div>