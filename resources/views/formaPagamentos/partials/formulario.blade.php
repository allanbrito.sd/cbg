@define($formaPagamento = $formaPagamento ?? null)
@define($prefixoInput = !empty($prefixo) ? $prefixo.'[formaPagamento]'  : 'formaPagamento')
@define($prefixo = !empty($prefixo) ? $prefixo.'.formaPagamento.'  : 'formaPagamento.')

<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-body">
		<div class="row">
	        <div class="form-group col-md-3{{ $errors->has($prefixo.'nome') ? ' has-error' : '' }}">
	            <label class="control-label">Nome <span style="color:#f00">*</span></label>
	            <input type="text" class="form-control" name="{{ $prefixoInput }}[nome]" value="{{ old($prefixo.'nome') ?? $formaPagamento->nome ?? '' }}">
	            @showError($prefixo.'nome')
	        </div>
		</div>
	</div>
</div>