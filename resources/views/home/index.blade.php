@extends('layouts.app')

@section('title', 'Visão geral')

@section('migalha')
@parent
<li class="active"><a href="{{ route('home') }}"><strong>Visão geral</strong></a></li>
@endsection

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="text-center m-t-lg">
					<h1>
						Gráficos!
					</h1>
				</div>
			</div>
		</div>
	</div>
@endsection
