@extends('layouts.app')

@include('layouts.vendor.datatable')

@section('title', ucfirst(__('modulos.lancamentos')))

@section('migalha')
@parent
<li class="active"><a href="{{ route('lancamento.consultar') }}">{{ ucfirst(__('modulos.lancamentos')) }}</a></li>
<li><strong>Consultar</strong></li>
@endsection

@section('acoes')
<a class="btn btn-success" href="{{ route('lancamento.novo') }}"><i class="fa fa-plus-square"></i> Novo</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="ibox">
				<div class="ibox-content">
					@if($lancamentos->count() > 0)
						@include('lancamentos.partials.tabela')
					@else
						@include('lancamentos.partials.sem_registro')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	@parent
	<script type="text/javascript">

		$(document).ready(function(){
			var $columns = $('thead th:not(.dt-no-export)', $('.tableLancamento'));
			var tableLancamento = $('.tableLancamento').DataTable({
				columnDefs: [
					{'aTargets': [6], 'bSortable': false},
					{'aTargets': [1,2,6], 'searchable': false}
				],
				buttons: [
					{
		            	extend: 'csv',
		            	text: 'CSV',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'excel',
		            	text: 'Excel',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'pdf',
		            	text: 'PDF',
		            	exportOptions: {
				            columns: $columns
				        }
		            }
		        ]
			});

			$(document).on('keyup', '.filtroLancamento', function() {
				tableLancamento.search($(this).val()).draw() ;
			});
		});

	</script>

@endsection
