@extends('layouts.app')

@section('title', 'Editar '.__('lancamento'))

@section('migalha')
@parent
<li><a href="{{ route('lancamento.consultar') }}">{{ ucfirst(__('modulos.lancamentos')) }}</a></li>
<li><a href="{{ route('lancamento.visualizar', ['id' => $lancamento->id]) }}">{{ $lancamento->pessoa->nomeESobrenome }}</a></li>
<li class="active"><strong>Editar</strong></li>
@endsection

@section('acoes')
<a class="btn" href="{{ route('lancamento.consultar') }}"><i class="fa fa-mail-reply"></i> Voltar</a>
<a class="btn btn-info" target="_blank" href="{{ route('recibo.visualizar', ['id' => $lancamento->pagamento->recibo->id ?? null]) }}"><i class="fa fa-file-text-o"></i> Recibo</a>
<a class="btn btn-success" role="submit" href="{{ route('lancamento.atualizar', ['id' => $lancamento->id]) }}"><i class="fa fa-save"></i> Salvar</a>
@endsection

@section('content')
<form role="form" method="POST" action="{{ route('lancamento.atualizar', ['id' => $lancamento->id]) }}">
	@method('PUT')

	<div class="wrapper wrapper-content animated fadeInRight">
		<ul class="nav nav-tabs">
			@if(!empty($lancamento->pagamento->recibo->gerado_em))
				<li class="active"><a data-toggle="tab" href="#tab-recibo">Recibo</a></li>
			@endif
			<li class="{{ empty($lancamento->pagamento->recibo->gerado_em) ? 'active' : ''}}"><a data-toggle="tab" href="#tab-financeiro">Financeiro</a></li>
			<li class=""><a data-toggle="tab" href="#tab-recebedor-pagador">Pagador / Recebedor</a></li>
			<li class=""><a data-toggle="tab" href="#tab-referente">Referente a</a></li>
			@if(empty($lancamento->pagamento->recibo->gerado_em))
				<li class=""><a data-toggle="tab" href="#tab-recibo">Recibo</a></li>
			@endif
		</ul>
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content">
						<div class="tab-content">
							@define($prefixo = 'formulario')
							@define($buscarPessoa = true)
							<div id="tab-financeiro" class="tab-pane {{ empty($lancamento->pagamento->recibo->gerado_em) ? 'active' : ''}}">
								@if(!empty($lancamento->pagamento->recibo->gerado_em) && empty($lancamento->pagamento->recibo->cancelado_em))
									<div class="alert alert-info">Nenhuma informação pode ser alterada após um recibo gerado. <a class="alert-link cancelar-recibo" data-id="{{ $lancamento->pagamento->recibo->id }}" href="javascript:;">Cancele o recibo</a> para alterar os dados.</div>
								@endif
								@include('lancamentos.partials.formulario')
							</div>
							<div id="tab-recebedor-pagador" class="tab-pane">
								@if(!empty($lancamento->pagamento->recibo->gerado_em) && empty($lancamento->pagamento->recibo->cancelado_em))
									<div class="alert alert-info">Nenhuma informação pode ser alterada após um recibo gerado. <a class="alert-link cancelar-recibo" data-id="{{ $lancamento->pagamento->recibo->id }}" href="javascript:;">Cancele o recibo</a> para alterar os dados.</div>
								@endif
								<div class="pagamentoPessoaDiv">
									@define($prefixo = 'pagamentoPessoa')
									@define($pessoa = $lancamento->pagamento->pessoa)
									@include('pessoas.partials.formulario')
								</div>
								<div class="row">
									<div class="form-group col-md-3{{ $errors->has('pagamento.recibo_para_mesma_pessoa') ? ' has-error' : '' }}">
										<label class="control-label">Recibo deve ser gerado em nome do <span class="recebedorPagadorLabel">pagador</span>?</label><br/>
										<input type="checkbox" class="form-control reciboMesmaPessoaInput" {{ false == old('pagamento.recibo_para_mesma_pessoa') ?? $lancamento->pagamento->recibo_para_mesma_pessoa ?? null ? 'checked' : '' }} value="1"/><br/>
										<span class='mesmaPessoaLabel' style="font-weight: 400; font-size: 90%; display: none;">Mesma pessoa</span>
										<span class='outraPessoaLabel'  style="font-weight: 400; font-size: 90%; display: none;">Pessoas diferentes</span>
										@showError('pagamento.recibo_para_mesma_pessoa')
									</div>
								</div>
								<div class="reciboPessoaDiv" style="display: none">
									@define($prefixo = 'reciboPessoa')
									@define($pessoa = $lancamento->pessoa)
									@include('pessoas.partials.formulario')
								</div>
							</div>
							<div id="tab-referente" class="tab-pane">
								@if(!empty($lancamento->pagamento->recibo->gerado_em) && empty($lancamento->pagamento->recibo->cancelado_em))
									<div class="alert alert-info">Nenhuma informação pode ser alterada após um recibo gerado. <a class="alert-link cancelar-recibo" data-id="{{ $lancamento->pagamento->recibo->id }}" href="javascript:;">Cancele o recibo</a> para alterar os dados.</div>
								@endif
								@define($prefixo = 'formulario')
								@include('lancamentos.partials.referente')
							</div>
							<div id="tab-recibo" class="tab-pane {{ !empty($lancamento->pagamento->recibo->gerado_em) ? 'active' : ''}}">
								@define($prefixo = 'formulario')
								@include('lancamentos.partials.recibo')
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#reciboModal" style="display: none"></button>
<div class="modal inmodal" id="reciboModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-file-text modal-icon" style="margin-left: 10px"></i>
                <h4 class="modal-title">Recibo</h4>
                <small class="font-bold" style="font-size:110%">Deseja realmente gerar um recibo agora?</small>
            </div>
            <div class="modal-body">
                <p class="text-center" style="padding: 0 20px"><strong>Confira todas as informações.</strong><br/>Após gerado, nenhuma informação de pagamento poderá ser alterada.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Espere, vou conferir</button>
				<button type="button" data-role="submit" data-dismiss="modal" class="btn btn-primary">Gerar recibo</button>
            </div>
        </div>
    </div>
</div>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cancelarReciboModal" style="display: none"></button>
<div class="modal inmodal" id="cancelarReciboModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-file-text modal-icon" style="margin-left: 10px"></i>
                <h4 class="modal-title">Recibo</h4>
                <small class="font-bold" style="font-size:110%">Deseja realmente cancelar esse recibo?</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
				<button type="button" data-role="submit" data-dismiss="modal" class="btn btn-danger">Cancelar recibo</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@parent
	<script type="text/javascript">
		switcherylist = [];
		$(document).ready(function() {
			switcherylist.push(new Switchery($('.reciboMesmaPessoaInput')[0], { color: '#48a866', secondaryColor: '#ec5464'}));
			$('.reciboMesmaPessoaInput').on('change', changePessoaRecibo);
			changePessoaRecibo();
			@if(!empty($lancamento->pagamento->recibo->gerado_em) && empty($lancamento->pagamento->recibo->cancelado_em))
				$('form[role=form] input,form[role=form] textarea, form[role=form] button.btn,form[role=form] select').attr('disabled', true)
				$('form[role=form] select').trigger("chosen:updated");
				$('.btn-success[role=submit]').attr('disabled', true).attr('href', 'javascript:;');
				switcherylist.forEach(function(switchery) {
			      switchery.disable();
		        });
			@endif
		});
		
		function changePessoaRecibo() {
			if ($('.reciboMesmaPessoaInput').is(':checked')) {
				$('.mesmaPessoaLabel').show();
				$('.outraPessoaLabel,.reciboPessoaDiv').hide();
			} else {
				$('.outraPessoaLabel,.reciboPessoaDiv').show();
				$('.mesmaPessoaLabel').hide();
			}
		}
	</script>
@endsection