@extends('layouts.app')

@section('title', "Cadastrar ".__('modulos.lancamento'))

@section('migalha')
@parent
<li><a href="{{ route('lancamento.consultar') }}">{{ ucfirst(__('modulos.lancamentos')) }}</a></li>
<li class="active"><strong>Novo</strong></li>
@endsection

@section('acoes')
<a class="btn" href="{{ route('lancamento.consultar') }}"><i class="fa fa-mail-reply"></i> Voltar</a>
<a class="btn btn-success" role="submit" href="{{ route('lancamento.cadastrar') }}"><i class="fa fa-save"></i> Salvar</a>
@endsection

@section('content')
<form role="form" method="POST" action="{{ route('lancamento.cadastrar') }}">
	@method('POST')
	<div class="wrapper wrapper-content animated fadeInRight">
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#tab-financeiro">Financeiro</a></li>
			<li class=""><a data-toggle="tab" href="#tab-recebedor-pagador">Pagador / Recebedor</a></li>
			<li class=""><a data-toggle="tab" href="#tab-referente">Referente a</a></li>
		</ul>
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content">
						<div class="tab-content">
							@define($prefixo = 'formulario')
							@define($buscarPessoa = true)
							<div id="tab-financeiro" class="tab-pane active">
								@include('lancamentos.partials.formulario')
							</div>
							<div id="tab-recebedor-pagador" class="tab-pane">
								<div class="pagamentoPessoaDiv">
									@define($prefixo = 'pagamentoPessoa')
									@include('pessoas.partials.formulario')
								</div>
								<div class="row">
									<div class="form-group col-md-3{{ $errors->has('pagamento.recibo_para_mesma_pessoa') ? ' has-error' : '' }}">
										<label class="control-label">Recibo deve ser gerado em nome do <span class="recebedorPagadorLabel">pagador</span>?</label><br/>
										<input type="checkbox" class="form-control reciboMesmaPessoaInput" {{ false == old('pagamento.recibo_para_mesma_pessoa') ?? $lancamento->pagamento->recibo_para_mesma_pessoa ?? null ? 'checked' : '' }} value="1"/><br/>
										<span class='mesmaPessoaLabel' style="font-weight: 400; font-size: 90%; display: none;">Mesma pessoa</span>
										<span class='outraPessoaLabel'  style="font-weight: 400; font-size: 90%; display: none;">Pessoas diferentes</span>
										@showError('pagamento.recibo_para_mesma_pessoa')
									</div>
								</div>
								<div class="reciboPessoaDiv" style="display: none">
									@define($prefixo = 'reciboPessoa')
									@include('pessoas.partials.formulario')
								</div>
							</div>
							<div id="tab-referente" class="tab-pane">
								@define($prefixo = 'formulario')
								@include('lancamentos.partials.referente')
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

@section('scripts')
@parent
	<script type="text/javascript">
		$(document).ready(function() {
			switcherylist = [];
			new Switchery($('.reciboMesmaPessoaInput')[0], { color: '#48a866', secondaryColor: '#ec5464'});
			$('.reciboMesmaPessoaInput').on('change', changePessoaRecibo);
			changePessoaRecibo();
		});
		
		function changePessoaRecibo() {
			if ($('.reciboMesmaPessoaInput').is(':checked')) {
				$('.mesmaPessoaLabel').show();
				$('.outraPessoaLabel,.reciboPessoaDiv').hide();
			} else {
				$('.outraPessoaLabel,.reciboPessoaDiv').show();
				$('.mesmaPessoaLabel').hide();
			}
		}
	</script>
@endsection