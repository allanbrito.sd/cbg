@define($prefixoInput = $prefixo ?? null)
@define($prefixo = $prefixo ?? null)
@define($figLicense = $figLicense ?? null)
@define($uniqid = uniqid())

<div class="fig_license_div">
	<input type="hidden" name="{{ $prefixoInput }}[fig_license][{{ $uniqid }}][id]" value="{{ $figLicense->id ?? '' }}">
	<div class="form-group col-md-3{{ $errors->has($prefixo.'.fig_license.'.$uniqid.'.atleta_id') ? ' has-error' : '' }}">
		<label class="control-label">Atleta</label>
		<select name="{{ $prefixoInput }}[fig_license][{{ $uniqid }}][atleta_id]" data-placeholder="Selecione uma opção" class="chosen-select-search">
	        <option value="">Selecione uma opção</option>
	        @foreach($atletas ?? [] as $atleta)
	        	<option {{ $atleta->id == (old($prefixo.'.fig_license.'.$uniqid.'.atleta_id') ?? $figLicense->atleta_id ?? '') ? 'selected' : '' }} value="{{ $atleta->id }}">{{ $atleta->nome }}</option>
	        @endforeach
	    </select>
		@showError($prefixo.'.fig_license.'.$uniqid.'.atleta_id')
	</div>
	<div class="form-group col-md-3{{ $errors->has($prefixo.'.fig_license.modalidade_id') ? ' has-error' : '' }}">
		<label class="control-label">Modalidade</label>
		<select name="{{ $prefixoInput }}[fig_license][{{ $uniqid }}][modalidade_id]" data-placeholder="Selecione uma opção" class="chosen-select-search">
	        <option value="">Selecione uma opção</option>
	        @foreach($modalidades ?? [] as $modalidade)
	        	<option {{ $modalidade->id == (old($prefixo.'.fig_license.modalidade_id') ?? $figLicense->modalidade_id ?? '') ? 'selected' : '' }} value="{{ $modalidade->id }}">{{ $modalidade->nome }}</option>
	        @endforeach
	    </select>
		@showError($prefixo.'.fig_license.'.$uniqid.'.modalidade_id')
	</div>
	<div class="form-group col-md-3{{ $errors->has($prefixo.'.fig_license.'.$uniqid.'.clube_id') ? ' has-error' : '' }}">
		<label class="control-label">Clube</label>
		<select name="{{ $prefixoInput }}[fig_license][{{ $uniqid }}][clube_id]" data-placeholder="Selecione uma opção" class="chosen-select-search">
	        <option value="">Selecione uma opção</option>
	        @foreach($clubes ?? [] as $clube)
	        	<option {{ $clube->id == (old($prefixo.'.fig_license.'.$uniqid.'.clube_id') ?? $figLicense->clube_id ?? '') ? 'selected' : '' }} value="{{ $clube->id }}">{{ $clube->nome }}</option>
	        @endforeach
	    </select>
		@showError($prefixo.'.fig_license.'.$uniqid.'.clube_id')
	</div>
	<div class="form-group col-md-3">
		<br/>
		<a href="javascript:;" class="btn btn-success btn_adicionar_license" title="Adicionar outro" style="margin-top: 5px;  display: none"><i class="fa fa-plus"></i></a>
		<a href="javascript:;" class="btn btn-danger btn_remover_license" title="Remover" style="margin-top: 5px;"><i class="fa fa-times"></i></a>
	</div>
</div>
