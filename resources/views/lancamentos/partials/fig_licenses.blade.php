@define($figLicenses = $figLicenses ?? [])

<div class="fig_licenses_div">
	@foreach($figLicenses as $figLicense)
		@include('lancamentos.partials.fig_license')
	@endforeach
	@define($figLicense = [])
	@include('lancamentos.partials.fig_license')
</div>

@section('scripts')
@parent
	<script type="text/javascript">
		$(document).ready(function(){
			changeLicense();
			$(document).on('click', '.btn_adicionar_license', addFigLicense);
			$(document).on('click', '.btn_remover_license', removeFigLicense);
		});

		function removeFigLicense() {
			$(this).closest('.fig_license_div').remove();
		}

		function addFigLicense() {
			$.ajax({
				headers: {
			        'X-CSRF-TOKEN': '{{ csrf_token() }}'
			    },
				method: "GET",
			    url: "{{ URL::to('figlicense')}}",
			    success: function(html){
			    	$('.fig_licenses_div').append(html);
					$('select.chosen-select-search').chosen({width: "100%"});
					$('select.chosen-select').chosen({width: "100%", disable_search: true});
			    	changeLicense();
			    }
			});
		}

		function changeLicense() {
			$('.fig_license_div .btn_remover_license').show();
			$('.fig_license_div .btn_adicionar_license').hide();
			$ultimaLicensa = $($('.fig_license_div')[$('.fig_license_div').length - 1]);
			$ultimaLicensa.find('.btn_remover_license').hide();
			$ultimaLicensa.find('.btn_adicionar_license').show();
		}
	</script>
@endsection