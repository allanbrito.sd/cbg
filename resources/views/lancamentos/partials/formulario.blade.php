@include('layouts.vendor.switchery')
@define($prefixoInput = $prefixo ?? null)
@define($prefixo = $prefixo ?? null)

<div class="widget-box transparent ui-sortable-handle">
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			<input type="hidden" name="{{ $prefixoInput }}[lancamento][id]" value="{{ old($prefixo.'.lancamento.id') ?? $lancamento->id ?? '' }}">
			<input type="hidden" name="{{ $prefixoInput }}[pagamento][id]" value="{{ old($prefixo.'.pagamento.id') ?? $lancamento->pagamento->id ?? '' }}">
			<input type="hidden" name="{{ $prefixoInput }}[nota_fiscal][id]" value="{{ old($prefixo.'.nota_fiscal.id') ?? $lancamento->nota_fiscal->id ?? '' }}">
			<div class="row">
				<div class="form-group col-md-3{{ $errors->has($prefixo.'.nota_fiscal.codigo') ? ' has-error' : '' }}">
					<label class="control-label">Nº Nota Fiscal / Fatura</label>
					<input type="text" class="form-control" name="{{ $prefixoInput }}[nota_fiscal][codigo]" value="{{ old($prefixo.'.nota_fiscal.codigo') ?? $lancamento->notaFiscal->codigo ?? '' }}">
					@showError($prefixo.'.nota_fiscal.codigo')
				</div>

				<div class="form-group col-md-6{{ $errors->has($prefixo.'.lancamento.historico') ? ' has-error' : '' }}">
					<label class="control-label">Histórico <span style="color:#f00">*</span></label>
					<input type="text" class="form-control" name="{{ $prefixoInput }}[lancamento][historico]" value="{{ old($prefixo.'.lancamento.historico') ?? $lancamento->historico ?? '' }}">
					@showError($prefixo.'.lancamento.historico')
				</div>
				
				<div class="col-md-3{{ $errors->has($prefixo.'.lancamento.credito') ? ' has-error' : '' }}">
					<label class="control-label">Tipo <span style="color:#f00">*</span></label><br/>
					<input type="checkbox" class="form-control creditoInput" name="{{ $prefixoInput }}[lancamento][credito]" {{ (old($prefixo.'.lancamento.credito') ?? $lancamento->credito ?? null) == 1 ? 'checked' : '' }} value="1"/><br/>
					<span class='creditoLabel' style="font-weight: 400; font-size: 90%; display: none;">Crédito</span>
					<span class='debitoLabel'  style="font-weight: 400; font-size: 90%; display: none;">Débito</span>
					@showError($prefixo.'.lancamento.credito')
				</div>

				<div class="form-group col-md-3{{ $errors->has($prefixo.'.lancamento.valor') ? ' has-error' : '' }}">
					<label class="control-label">Valor <span style="color:#f00">*</span></label>
					<input type="text" class="form-control valor" name="{{ $prefixoInput }}[lancamento][valor]" value="{{ old($prefixo.'.lancamento.valor') ?? $lancamento->valor ?? '' }}">
					@showError($prefixo.'.lancamento.valor')
				</div>

				<div class="form-group col-md-3{{ $errors->has($prefixo.'.lancamento.vencimento') ? ' has-error' : '' }}">
					<label class="control-label">Vencimento</label>
					<input type="text" class="form-control data" name="{{ $prefixoInput }}[lancamento][vencimento]" value="{{ old($prefixo.'.lancamento.vencimento') ?? (!empty($lancamento->vencimento) ? $lancamento->vencimento->format('d/m/Y') : null) ?? date('t/m/Y') }}">
					@showError($prefixo.'.lancamento.vencimento')
				</div>

				<div class="form-group col-md-3{{ $errors->has($prefixo.'.lancamento.competencia') ? ' has-error' : '' }}">
					<label class="control-label">Mês de referência</label>
					<input type="text" class="form-control mesano" name="{{ $prefixoInput }}[lancamento][competencia]" value="{{ old($prefixo.'.lancamento.competencia') ?? (!empty($lancamento->competencia) ? $lancamento->competencia->format('m/Y') : null) ?? date('m/Y') }}">
					@showError($prefixo.'.lancamento.competencia')
				</div>
			</div>
		</div>
	</div>
</div>

<div class="widget-box transparent ui-sortable-handle">
	<div class="widget-header">
		<h2 class="widget-title lighter">Pagamento</h2>
	</div>
	<div class="widget-body">
		<div class="row">
			<div class="widget-main padding-6 no-padding-left no-padding-right">
				<div class="form-group col-md-3{{ $errors->has($prefixo.'.pagamento.conta_id') ? ' has-error' : '' }}">
					<label class="control-label">Fonte de pagamento</label>
					<select name="{{ $prefixoInput }}[pagamento][conta_id]" data-placeholder="Selecione uma opção" class="chosen-select-search">
		                <option value="">Selecione uma opção</option>
		                @foreach($contas ?? [] as $conta)
		                	<option {{ $conta->id == (old($prefixo.'.pagamento.conta_id') ?? $lancamento->pagamento->conta_id ?? '') ? 'checked' : '' }} value="{{ $conta->id }}">{{ $conta->nome }}</option>
		                @endforeach
		            </select>
					@showError($prefixo.'.pagamento.conta_id')
				</div>
			</div>

			<div class="widget-main padding-6 no-padding-left no-padding-right">
				<div class="form-group col-md-3{{ $errors->has($prefixo.'.pagamento.plano_contas_id') ? ' has-error' : '' }}">
					<label class="control-label">Plano de contas</label>
					<select name="{{ $prefixoInput }}[pagamento][plano_contas_id]" data-placeholder="Selecione uma opção" class="chosen-select-search">
		                <option value="">Selecione uma opção</option>
		                @foreach($planoContas ?? [] as $planoConta)
		                	<option {{ $planoConta->id == (old($prefixo.'.pagamento.plano_contas_id') ?? $lancamento->pagamento->plano_contas_id ?? '') ? 'checked' : '' }} value="{{ $planoConta->id }}">{{ $planoConta->codigo.' - '.$planoConta->nome }}</option>
		                @endforeach
		            </select>
					@showError($prefixo.'.pagamento.plano_contas_id')
				</div>
			</div>

			<div class="widget-main padding-6 no-padding-left no-padding-right">
				<div class="form-group col-md-3{{ $errors->has($prefixo.'.pagamento.pago') ? ' has-error' : '' }}">
					<label class="control-label">Situação do pagamento</label><br/>
					<input type="checkbox" class="form-control pagoInput" name="{{ $prefixoInput }}[pagamento][pago]" {{ (old($prefixo.'.pagamento.pago') ?? $lancamento->pagamento->pago ?? null) == 1 ? 'checked' : '' }} value="1"/><br/>
					<span class='pendenteLabel' style="font-weight: 400; font-size: 90%; display: none;">Em Aberto</span>
					<span class='pagoLabel'  style="font-weight: 400; font-size: 90%; display: none;">Efetuado</span>
					@showError($prefixo.'.pagamento.pago')
				</div>
			</div>
			<div class="form-group col-md-3{{ $errors->has($prefixo.'.pagamento.devolvido') ? ' has-error' : '' }}">
				<label class="control-label">Pagamento devolvido</label><br/>
				<input type="checkbox" class="form-control devolvidoInput" name="{{ $prefixoInput }}[pagamento][devolvido]" {{ (old($prefixo.'.pagamento.devolvido') ?? $lancamento->pagamento->devolvido ?? null) == 1 ? 'checked' : '' }} value="1"/><br/>
				<span class='naoDevolvidoLabel' style="font-weight: 400; font-size: 90%; display: none;">Não</span>
				<span class='devolvidoLabel'  style="font-weight: 400; font-size: 90%; display: none;">Sim</span>
				@showError($prefixo.'.pagamento.devolvido')
			</div>
		</div>
		<div class="row">

			<div class="form-group col-md-3{{ $errors->has($prefixo.'.pagamento.data') ? ' has-error' : '' }}">
				<label class="control-label">Data de pagamento</label>
				<input type="text" class="form-control data" name="{{ $prefixoInput }}[pagamento][data]" value="{{ old($prefixo.'.pagamento.data') ?? (!empty($lancamento->pagamento->data) ? $lancamento->pagamento->data->format('d/m/Y') : null) ?? '' }}">
				@showError($prefixo.'.pagamento.data')
			</div>

			<div class="form-group col-md-3{{ $errors->has($prefixo.'.pagamento.forma_pagamento_id') ? ' has-error' : '' }}">
				<label class="control-label">Forma de pagamento </label>
				<select name="{{ $prefixoInput }}[pagamento][forma_pagamento_id]" data-placeholder="Selecione uma opção" class="chosen-select-search">
	                @foreach($formasPagamento ?? [] as $formaPagamento)
	                	<option {{ $formaPagamento->id == (old($prefixo.'.pagamento.forma_pagamento_id') ?? $lancamento->pagamento->forma_pagamento_id ?? '') ? 'checked' : '' }} value="{{ $formaPagamento->id }}">{{ $formaPagamento->nome }}</option>
	                @endforeach
	            </select>
				@showError($prefixo.'.pagamento.forma_pagamento_id')
			</div>

			<div class="form-group col-md-3{{ $errors->has($prefixo.'.pagamento.comprovante') ? ' has-error' : '' }}">
				<label class="control-label">Nº comprovante</label>
				<input type="text" class="form-control" name="{{ $prefixoInput }}[pagamento][comprovante]" value="{{ old($prefixo.'.pagamento.comprovante') ?? $lancamento->pagamento->comprovante ?? '' }}">
				@showError($prefixo.'.pagamento.comprovante')
			</div>

		</div>
	</div>
</div>
<div class="widget-box transparent ui-sortable-handle mt-5">
	<div class="widget-header">
		<h2 class="widget-title lighter"></h2>
	</div>
	<div class="widget-body">
		<div class="row">
			<div class="form-group col-md-6{{ $errors->has($prefixo.'.pagamento.detalhamento') ? ' has-error' : '' }}">
				<label class="control-label">Detalhamento para a prestação de contas</label>
				<textarea class="form-control" name="{{ $prefixoInput }}[pagamento][detalhamento]">{{ old($prefixo.'.pagamento.detalhamento') ?? $lancamento->pagamento->detalhamento ?? '' }}</textarea>
				@showError($prefixo.'.pagamento.detalhamento')
			</div>
			<div class="form-group col-md-6{{ $errors->has($prefixo.'.nota_fiscal.descricao') ? ' has-error' : '' }}">
				<label class="control-label">Descrição da nota fiscal</label>
				<textarea class="form-control" name="{{ $prefixoInput }}[nota_fiscal][descricao]">{{ old($prefixo.'.nota_fiscal.descricao') ?? $lancamento->notaFiscal->descricao ?? '' }}</textarea>
				@showError($prefixo.'.nota_fiscal.descricao')
			</div>
		</div>
	</div>
</div>

@section('scripts')
@parent
	<script type="text/javascript">
		$(document).ready(function() {
			switcherylist.push(new Switchery($('.creditoInput')[0], { color: '#48a866', secondaryColor: '#ec5464'}));
			$('.creditoInput').on('change', changeDebito);
			changeDebito();

			switcherylist.push(new Switchery($('.pagoInput')[0], { color: '#48a866', secondaryColor: '#ec5464'}));
			$('.pagoInput').on('change', changePago);
			changePago();

			switcherylist.push(new Switchery($('.devolvidoInput')[0], { color: '#48a866', secondaryColor: '#ec5464'}));
			$('.devolvidoInput').on('change', changeDevolvido);
			changeDevolvido();

		});
		
		function changeDebito() {
			if ($('.creditoInput').is(':checked')) {
				$('.creditoLabel').show();
				$('.debitoLabel').hide();
				$('[href="#tab-recebedor-pagador"],.recebedorPagadorLabel').html("Pagador");
			} else {
				$('.debitoLabel').show();
				$('.creditoLabel').hide();
				$('[href="#tab-recebedor-pagador"],.recebedorPagadorLabel').html("Recebedor");
			}
		}

		function changePago() {
			if ($('.pagoInput').is(':checked')) {
				$('.pagoLabel').show();
				$('.pendenteLabel').hide();
			} else {
				$('.pendenteLabel').show();
				$('.pagoLabel').hide();
			}
		}

		function changeDevolvido() {
			if ($('.devolvidoInput').is(':checked')) {
				$('.devolvidoLabel').show();
				$('.naoDevolvidoLabel').hide();
			} else {
				$('.naoDevolvidoLabel').show();
				$('.devolvidoLabel').hide();
			}
		}
	</script>
@endsection