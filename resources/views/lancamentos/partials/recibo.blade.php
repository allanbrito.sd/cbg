@define($prefixoInput = $prefixo ?? null)
@define($prefixo = $prefixo ?? null)

<div class="widget-box transparent ui-sortable-handle">
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover mt-0 tableLancamento">
					<thead>
						<tr>
							<th>Status</th>
							<th>Gerado em</th>
							<th>Cancelado em</th>
							<th class="dt-no-export">Ações</th>
						</tr>
					</thead>
					<tbody>
					@foreach($lancamento->pagamento->recibos ?? [] as $recibo)
						<tr class="pointer visualizarLancamento">
							<td>{{ !empty($recibo->cancelado_em) ? 'Cancelado' : (!empty($recibo->gerado_em) ? 'Gerado' : 'Rascunho') }}</td>
							<td>{{ !empty($recibo->gerado_em) ? $recibo->gerado_em->format('d/m/Y H:i') : '' }}</td>
							<td>{{ !empty($recibo->cancelado_em) ? $recibo->cancelado_em->format('d/m/Y H:i') : '' }}</td>
							<td class="text-center acoes" nowrap width="5%">
								<a target="_blank" href="{{ route('recibo.visualizar', ['id' => $recibo->id]) }}"><i class="fa fa-file-text-o fa-1-4x has-tooltip text-info" data-placement="bottom" title="Recibo"></i></a>
								@if(empty($recibo->gerado_em) && empty($recibo->cancelado_em))
									<i class="fa fa-check fa-1-4x has-tooltip text-success gerar-recibo" data-id="{{ $recibo->id }}" data-placement="bottom" title="Gerar Recibo"></i>
								@endif
								@if(!empty($recibo->gerado_em) && empty($recibo->cancelado_em))
									<i class="fa fa-1-4x fa-trash text-danger cancelar-recibo" data-id="{{ $recibo->id }}" data-placement ="bottom" title="Cancelar"></i>
								@endif
								@if(!empty($recibo->cancelado_em))
									<i class="fa fa-1-4x fa-times" style="visibility: hidden"></i>
								@endif
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@section('scripts')
@parent
	<script type="text/javascript">
		$(document).ready(function() {
			$('.gerar-recibo').on('click', function() {
				var reciboId = $(this).data('id');
				$('[data-target="#reciboModal"]').click();
				$(document).on('click', "#reciboModal [data-role='submit']", function() {
					$.ajax({
						headers: {
					        'X-CSRF-TOKEN': '{{ csrf_token() }}'
					    },
						method: "PUT",
					    url: "{{ URL::to('recibo')}}/gerar/"+reciboId,
					    success: function(json){
					    	swal({
							    title: "",
							    text: "Recibo gerado com sucesso!",
							    type: "success"
							},
							function(){
							    window.open('{{ URL::to('recibo')}}/visualizar/'+reciboId, '_blank');
							    window.location.href = window.location.href
							});
					    }
					});
				});
			});

			$('.cancelar-recibo').on('click', function() {
				var reciboId = $(this).data('id');
				$('[data-target="#cancelarReciboModal"]').click();
				$(document).on('click', "#cancelarReciboModal [data-role='submit']", function() {
					$.ajax({
						headers: {
					        'X-CSRF-TOKEN': '{{ csrf_token() }}'
					    },
						method: "PUT",
					    url: "{{ URL::to('recibo')}}/cancelar/"+reciboId,
					    success: function(json){
					    	swal({
							    title: "",
							    text: "Recibo cancelado com sucesso!",
							    type: "success"
							},
							function(){
							    window.open('{{ URL::to('recibo')}}/visualizar/'+reciboId, '_blank');
								window.location.href = window.location.href
							});
					    }
					});
				});
			});

		});
	</script>
@endsection