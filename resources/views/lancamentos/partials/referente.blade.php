@define($prefixoInput = $prefixo ?? null)
@define($prefixo = $prefixo ?? null)

<div class="widget-box transparent ui-sortable-handle">
	<div class="widget-header">
		<h2 class="widget-title lighter">Evento</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			<div class="row">
				<div class="form-group col-md-6{{ $errors->has($prefixo.'.lancamento.evento_id') ? ' has-error' : '' }}">
					<label class="control-label">Evento</label>
					<select name="{{ $prefixoInput }}[lancamento][evento_id]" data-placeholder="Selecione uma opção" class="chosen-select-search">
		                <option value="">Selecione uma opção</option>
		                @foreach($eventos ?? [] as $evento)
		                	<option {{ $evento->id == (old($prefixo.'.lancamento.evento_id') ?? $lancamento->evento_id ?? '') ? 'selected' : '' }} value="{{ $evento->id }}">{{ $evento->nomeExibicao }}</option>
		                @endforeach
		            </select>
					@showError($prefixo.'.lancamento.evento_id')
				</div>

				<div class="form-group col-md-12{{ $errors->has($prefixo.'.lancamento_evento_itens.*.evento_item_id') ? ' has-error' : '' }}">
					<label class="control-label">Complementos</label>
					<select multiple name="{{ $prefixoInput }}[lancamento_evento_itens][]" data-placeholder="Selecione um ou mais" class="chosen-select-search">
		                @foreach($eventoItens ?? [] as $item)
		                	<option {{ in_array($item->id, (old($prefixo.'.lancamento_evento_itens.*.evento_item_id') ?? (!empty($lancamento->eventoItens) ? $lancamento->eventoItens->pluck('evento_item_id')->toArray() : null) ?? [])) ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->nome }}</option>
		                @endforeach
		            </select>
					@showError($prefixo.'.lancamento_evento_itens.*.evento_item_id')
				</div>
			</div>
		</div>
	</div>
</div>
<div class="widget-box transparent ui-sortable-handle">
	<div class="widget-header">
		<h2 class="widget-title lighter">Anuidade {{ (!empty($lancamento->anuidade->first()) ? $lancamento->anuidade->first()->ano : null) ?? date('Y') }}</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			<div class="row">
				<input type="hidden" name="{{ $prefixoInput }}[anuidade][ano]" value="{{ date('Y') }}">
				<div class="form-group col-md-12{{ $errors->has($prefixo.'.anuidade.modalidade_id') ? ' has-error' : '' }}">
					<label class="control-label">Modalidades</label>
					<select multiple name="{{ $prefixoInput }}[anuidade][modalidade_id][]" data-placeholder="Selecione uma ou mais" class="chosen-select-search">
		                <option value="">Selecione uma ou mais</option>
		                @foreach($modalidades ?? [] as $modalidade)
		                	<option {{ in_array($modalidade->id, (old($prefixo.'.anuidade.modalidade_id') ?? (!empty($lancamento->anuidade) ? $lancamento->anuidade->pluck('modalidade_id')->toArray() : null) ?? [])) ? 'selected' : '' }} value="{{ $modalidade->id }}">{{ $modalidade->nome }}</option>
		                @endforeach
		            </select>
					@showError($prefixo.'.anuidade.modalidade_id')
				</div>
			</div>
		</div>
	</div>
</div>
<div class="widget-box transparent ui-sortable-handle">
	<div class="widget-header">
		<h2 class="widget-title lighter">FIG License</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			<div class="row">
				@define($figLicenses = $lancamento->figLicenses ?? null)
				@include('lancamentos.partials.fig_licenses')
			</div>
		</div>
	</div>
</div>
<div class="widget-box transparent ui-sortable-handle">
	<div class="widget-header">
		<h2 class="widget-title lighter">Transferência de atleta</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
				@define($transferencias = $lancamento->transferencias ?? null)
				@include('lancamentos.partials.transferencias')
		</div>
	</div>
</div>