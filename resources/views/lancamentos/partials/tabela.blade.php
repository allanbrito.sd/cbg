<div class="row">
	<div class="col-md-12">
		<div class="input-group">
			<input type="text" placeholder="Pesquisar" class="input form-control filtroLancamento">
			<span class="input-group-btn">
				<button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i></button>
			</span>
		</div>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-sm-12">
		<div class="full-height-scroll">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover mt-0 tableLancamento">
					<thead>
						<tr>
							<th>Histórico</th>
							<th>Valor</th>
							<th>Dt. Vencimento</th>
							<th>Dt. Pagamento</th>
							<th>Tipo</th>
							<th>Pago</th>
							<th class="dt-no-export">Ações</th>
						</tr>
					</thead>
					<tbody>
					@foreach($lancamentos as $lancamento)
						<tr class="pointer visualizarLancamento">
							<td>{{ $lancamento->historico }}</td>
							<td>R$ {{ str_replace('.', ',', $lancamento->valor) }}</td>
							<td>{{ $lancamento->vencimento->format('d/m/Y') }}</td>
							<td>{{ !empty($lancamento->pagamento->data) ? $lancamento->pagamento->data->format('d/m/Y') : '' }}</td>
							<td><span class="label label-{{ $lancamento->credito ? 'primary' : 'danger' }}">{{ $lancamento->credito ? 'Crédito' : 'Débito' }}</span></td>
							<td><span class="label label-{{ $lancamento->pagamento->pago ? 'primary' : 'danger' }}">{{ $lancamento->pagamento->pago ? 'Pago' : 'Em aberto' }}</span></td>
							<td class="text-center acoes" nowrap width="5%">
								<a target="_blank" href="{{ route('recibo.visualizar', ['id' => $lancamento->pagamento->recibo->id ?? null]) }}"><i class="fa fa-file-text-o fa-1-4x has-tooltip text-info" data-placement="bottom" title="Recibo"></i></a>
								<i class="fa fa-search fa-1-4x has-tooltip hidden" data-placement="bottom" title="Visualizar"></i>
								<a href="{{ route('lancamento.editar', ['id' => $lancamento->id]) }}"><i class="fa fa-edit fa-1-4x has-tooltip" data-placement="bottom" title="Editar"></i></a>
								<form method="POST" action="{{ route('lancamento.apagar', ['id' => $lancamento->id]) }}">
									@method('DELETE')
									<a role="submit"><i class="fa fa-1-4x fa-trash text-danger" data-placement="bottom" title="Apagar"></i></a>
								</form>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>