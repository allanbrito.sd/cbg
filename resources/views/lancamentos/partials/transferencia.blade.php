@define($prefixoInput = $prefixo ?? null)
@define($prefixo = $prefixo ?? null)
@define($transferencia = $transferencia ?? null)
@define($uniqid = uniqid())

<div class="transferencia_div" style="border:1px solid #cacaca; border-radius: 10px; padding: 10px; margin-bottom: 10px">
	<div class="row">
		<input type="hidden" name="{{ $prefixoInput }}[transferencia][{{ $uniqid }}][id]" value="{{ $transferencia->id ?? '' }}">
		<div class="form-group col-md-3{{ $errors->has($prefixo.'.transferencia.'.$uniqid.'.atleta_id') ? ' has-error' : '' }}">
			<label class="control-label">Atleta</label>
			<select name="{{ $prefixoInput }}[transferencia][{{ $uniqid }}][atleta_id]" data-placeholder="Selecione uma opção" class="chosen-select-search">
	            <option value="">Selecione uma opção</option>
	            @foreach($atletas ?? [] as $atleta)
	            	<option {{ $atleta->id == (old($prefixo.'.transferencia.'.$uniqid.'.atleta_id') ?? $transferencia->atleta_id ?? '') ? 'selected' : '' }} value="{{ $atleta->id }}">{{ $atleta->nome }}</option>
	            @endforeach
	        </select>
			@showError($prefixo.'.transferencia.'.$uniqid.'.atleta_id')
		</div>
		<div class="form-group col-md-3{{ $errors->has($prefixo.'.transferencia.'.$uniqid.'.modalidade_id') ? ' has-error' : '' }}">
			<label class="control-label">Modalidade</label>
			<select name="{{ $prefixoInput }}[transferencia][{{ $uniqid }}][modalidade_id]" data-placeholder="Selecione uma opção" class="chosen-select-search">
	            <option value="">Selecione uma opção</option>
	            @foreach($modalidades ?? [] as $modalidade)
	            	<option {{ $modalidade->id == (old($prefixo.'.transferencia.'.$uniqid.'.modalidade_id') ?? $transferencia->modalidade_id ?? '') ? 'selected' : '' }} value="{{ $modalidade->id }}">{{ $modalidade->nome }}</option>
	            @endforeach
	        </select>
			@showError($prefixo.'.transferencia.'.$uniqid.'.modalidade_id')
		</div>
		<div class="form-group col-md-3{{ $errors->has($prefixo.'.transferencia.'.$uniqid.'.clube_origem_id') ? ' has-error' : '' }}">
			<label class="control-label">Clube de origem</label>
			<select name="{{ $prefixoInput }}[transferencia][{{ $uniqid }}][clube_origem_id]" data-placeholder="Selecione uma opção" class="chosen-select-search">
	            <option value="">Selecione uma opção</option>
	            @foreach($clubes ?? [] as $clube)
	            	<option {{ $clube->id == (old($prefixo.'.transferencia.'.$uniqid.'.clube_origem_id') ?? $transferencia->clube_origem_id ?? '') ? 'selected' : '' }} value="{{ $clube->id }}">{{ $clube->nome }}</option>
	            @endforeach
	        </select>
			@showError($prefixo.'.transferencia.'.$uniqid.'.clube_origem_id')
		</div>
	</div>
	<div class="row">
		
		<div class="form-group col-md-3{{ $errors->has($prefixo.'.transferencia.'.$uniqid.'.principal_titulo_atual') ? ' has-error' : '' }}">
			<label class="control-label">Principal título atual</label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[transferencia][{{ $uniqid }}][principal_titulo_atual]" value="{{ old($prefixo.'.transferencia.'.$uniqid.'.principal_titulo_atual') ?? $transferencia->principal_titulo_atual ?? '' }}">
			@showError($prefixo.'.transferencia.'.$uniqid.'.principal_titulo_atual')
		</div>
		<div class="form-group col-md-3"></div>
		<div class="form-group col-md-3{{ $errors->has($prefixo.'.transferencia.'.$uniqid.'.clube_destino_id') ? ' has-error' : '' }}">
			<label class="control-label">Clube de destino</label>
			<select name="{{ $prefixoInput }}[transferencia][{{ $uniqid }}][clube_destino_id]" data-placeholder="Selecione uma opção" class="chosen-select-search">
	            <option value="">Selecione uma opção</option>
	            @foreach($clubes ?? [] as $clube)
	            	<option {{ $clube->id == (old($prefixo.'.transferencia.'.$uniqid.'.clube_destino_id') ?? $transferencia->clube_destino_id ?? '') ? 'selected' : '' }} value="{{ $clube->id }}">{{ $clube->nome }}</option>
	            @endforeach
	        </select>
			@showError($prefixo.'.transferencia.'.$uniqid.'.clube_destino_id')
		</div>
		<div class="form-group col-md-3">
			<a href="javascript:;" class="btn btn-success btn_adicionar_transaferencia" title="Adicionar outro" style="margin-top: 5px;  display: none"><i class="fa fa-plus"></i></a>
			<a href="javascript:;" class="btn btn-danger btn_remover_transaferencia" title="Remover" style="margin-top: 5px;"><i class="fa fa-times"></i></a>
		</div>
	</div>
</div>