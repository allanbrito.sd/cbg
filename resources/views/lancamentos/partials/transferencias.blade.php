@define($transferencias = $transferencias ?? [])

<div class="transferencias_div">
	@foreach($transferencias as $transferencia)
		@include('lancamentos.partials.transferencia')
	@endforeach
	@define($transferencia = [])
	@include('lancamentos.partials.transferencia')
</div>

@section('scripts')
@parent
	<script type="text/javascript">
		$(document).ready(function(){
			changetransaferencia();
			$(document).on('click', '.btn_adicionar_transaferencia', addFigtransaferencia);
			$(document).on('click', '.btn_remover_transaferencia', removeFigtransaferencia);
		});

		function removeFigtransaferencia() {
			$(this).closest('.transferencia_div').remove();
		}

		function addFigtransaferencia() {
			$.ajax({
				headers: {
			        'X-CSRF-TOKEN': '{{ csrf_token() }}'
			    },
				method: "GET",
			    url: "{{ URL::to('transferencia')}}",
			    success: function(html){
			    	$('.transferencias_div').append(html);
					$('select.chosen-select-search').chosen({width: "100%"});
					$('select.chosen-select').chosen({width: "100%", disable_search: true});
			    	changetransaferencia();
			    }
			});
		}

		function changetransaferencia() {
			$('.transferencia_div .btn_remover_transaferencia').show();
			$('.transferencia_div .btn_adicionar_transaferencia').hide();
			$ultimaLicensa = $($('.transferencia_div')[$('.transferencia_div').length - 1]);
			$ultimaLicensa.find('.btn_remover_transaferencia').hide();
			$ultimaLicensa.find('.btn_adicionar_transaferencia').show();
		}
	</script>
@endsection