@extends('layouts.blank')

<!-- TODO: Remover -->
{{-- @include('layouts/vendor/safetowork') --}}

@section('beforecontent')
	<!-- Wrapper-->
	<div id="wrapper">

		<!-- Navigation -->
		@include('layouts.themes.app.navigation')

		<!-- Page wraper -->
		<div id="page-wrapper" class="gray-bg">

			<!-- Page wrapper -->
			@include('layouts.themes.app.navbar')

			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-sm-6 hidden-xs">
					<h2>@yield('title')</h2>
					<ol class="breadcrumb">
						<li><a href="{{ route('home') }}">Início</a></li>
						@yield('migalha')
					</ol>
				</div>
				<div class="col-sm-6 col-xs-12 text-right acoes">
					<div class="visible-xs" style="margin-top: 20px"></div>
					<h3 class="hidden-xs">&nbsp;</h3>
					@yield('acoes')
				</div>
			</div>
			<div class="row visible-xs">
				<div class="col-lg-12">
					<h3 style="margin-top: 22.75px;margin-left: 4px;margin-bottom: 2.75px">@yield('title')</h3>
				</div>
			</div>
@endsection

@section('aftercontent')
			<!-- Footer -->
			@include('layouts.themes.app.footer')
		</div>
		<!-- End page wrapper-->

		{{-- @include('layouts.themes.app.sidebar') --}}
	</div>
	<!-- End wrapper-->
@endsection

@section('scripts')
@parent
	@define($notifications = session('notifications'))
	@if (!empty($notifications));
		@define($notifications = array_reverse($notifications))
		<script type="text/javascript">
			$(document).ready(function() {

				@foreach($notifications as $tipo => $mensagens)
					@foreach((array) $mensagens as $mensagem)
						toastr.{{$tipo}}('{{$mensagem}}');
					@endforeach
				@endforeach
			});
		</script>
	@endif
@endsection
