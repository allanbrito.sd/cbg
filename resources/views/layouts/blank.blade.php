<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{ env('APP_NAME') }} | @yield('title') </title>


	<link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
	<link rel="stylesheet" href="{!! asset('css/app.css') !!}" />
	<link rel="stylesheet" href="{!! asset('css/plugins/datapicker/datepicker3.css') !!}" />

	@yield('styles')

</head>
<body class="md-skin {{ request()->cookies->get('navbar_collapsed') ? 'mini-navbar' : '' }}@yield('body-class')">
	@yield('afterBody')

	@yield('beforecontent')
	@yield('content')
	@yield('aftercontent')

	<script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>
	<script src="{!! asset('js/plugins/datapicker/bootstrap-datepicker.js') !!}" type="text/javascript"></script>

	@yield('scripts')

</body>
</html>
