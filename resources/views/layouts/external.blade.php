@extends('layouts.blank')

@include('layouts.themes.grey')

@section('scripts')
@parent
	@define($notifications = session('notifications'))
	@if (!empty($notifications));
		@define($notifications = array_reverse($notifications))
		<script type="text/javascript">
			$(document).ready(function() {
				toastr.options.positionClass = "toast-top-center";

				@foreach($notifications as $tipo => $mensagens)
					@foreach((array) $mensagens as $mensagem)
						toastr.{{$tipo}}('{{$mensagem}}');
					@endforeach
				@endforeach
			});
		</script>
	@endif
@endsection