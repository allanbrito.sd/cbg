<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav metismenu" id="side-menu">
			<li class="nav-header">
				<div class="dropdown profile-element text-center">
					<a data-toggle="dropdown" class="dropdown-toggle" href="#">
						<div class = "gravatar" style="margin: 0 auto">
							<img src="{{{ asset(Auth::user()->pessoa->foto) }}}" />
						</div>
						<span class="clear name"> <span class="block m-t-xs"> <strong class="font-bold">{{{ Auth::user()->pessoa->nomeESobrenome }}}</strong> <b class="caret"></b> </span>
					</a>
					<ul class="dropdown-menu animated fadeInRight m-t-xs">
						<li><a href="{{ route('404') ?? route('usuario.visualizar', ['id' => Auth::user()->id ]) }}">Seus dados</a></li>
						<li class="divider"></li>
						<li><a href="{{ route('logout') }}">Sair</a></li>
					</ul>
				</div>
			</li>
			<!-- request()->route()->getPrefix() -->
			<li class="{{ request()->is('home*') ? 'active' : ''}}">
				<a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> <span class="nav-label">{{ ucfirst(__('modulos.home')) }}</span></a>
			</li>
			<li class="{{ request()->is('recibo*') || request()->is('lancamento*') || request()->is('conta*') || request()->is('planoConta*') || request()->is('formaPagamento*') ? 'active' : '' }}">
				<a href="{{ route('404') }}"><i class="fa fa-money"></i> <span class="nav-label">Financeiro</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					{{-- <li class="{{ request()->is('recibo*') ? 'active' : '' }}">
						<a href="{{ route('recibo.consultar') }}"><span class="nav-label">{{ ucfirst(__('modulos.recibos')) }}</span></a>
					</li> --}}
					<li class="{{ request()->is('lancamento*') ? 'active' : '' }}">
						<a href="{{ route('lancamento.consultar') }}"><span class="nav-label">{{ ucfirst(__('modulos.lancamentos')) }}</span></a>
					</li>
					<li class="{{ request()->is('conta*') ? 'active' : '' }}">
						<a href="{{ route('conta.consultar') }}"><span class="nav-label">{{ ucfirst(__('modulos.contas')) }}</span></a>
					</li>
					<li class="{{ request()->is('planoConta*') ? 'active' : '' }}">
						<a href="{{ route('planoConta.consultar') }}"><span class="nav-label">{{ ucfirst(__('modulos.planoContas')) }}</span></a>
					</li>
					<li class="{{ request()->is('formaPagamento*') ? 'active' : '' }}">
						<a href="{{ route('formaPagamento.consultar') }}"><span class="nav-label">{{ ucfirst(__('modulos.formaPagamentos')) }}</span></a>
					</li>
				</ul>
			</li>
			<li class="{{ request()->is('atleta*') ? 'active' : '' }}">
				<a href="{{ route('404') }}"><i class="fa fa-users"></i> <span class="nav-label">Pessoas</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="{{ request()->is('atleta*') ? 'active' : '' }}">
						<a href="{{ route('atleta.consultar') }}"><span class="nav-label">{{ ucfirst(__('modulos.atletas')) }}</span></a>
					</li>
					<li class="{{ request()->is('clube*') ? 'active' : '' }}">
						<a href="{{ route('clube.consultar') }}"> <span class="nav-label">{{ ucfirst(__('modulos.clubes')) }}</span></a>
					</li>
					<li class="{{ request()->is('pessoa*') ? 'active' : '' }}">
						<a href="{{ route('pessoa.consultar') }}"> <span class="nav-label">Avulsas</span></a>
					</li>
				</ul>
			</li>

			<li class="{{ request()->is('evento*') ? 'active' : '' }}">
				<a href="{{ route('404') }}"><i class="fa fa-cog"></i> <span class="nav-label">Configurações</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="{{ request()->is('evento*') ? 'active' : '' }}">
						<a href="{{ route('evento.consultar') }}"><span class="nav-label">{{ ucfirst(__('modulos.eventos')) }}</span></a>
					</li>
					<li class="{{ request()->is('modalidade*') ? 'active' : '' }}">
						<a href="{{ route('modalidade.consultar') }}"> <span class="nav-label">{{ ucfirst(__('modulos.modalidades')) }}</span></a>
					</li>
				</ul>
			</li>

			<li class="{{ request()->is('404*') ? 'active' : '' }}">
				<a href="{{ route('404') }}"><i class="fa fa-print"></i> <span class="nav-label">{{ ucfirst(__('modulos.relatórios')) }}</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
				</ul>
			</li>

			<li class="{{ request()->is('usuario*') ? 'active' : '' }}">
				<a href="{{ route('404') }}"><i class="fa fa-unlock-alt"></i> <span class="nav-label">{{ ucfirst(__('modulos.segurança')) }}</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li><a href="{{ route('404') }}">{{ ucfirst(__('modulos.usuários')) }}</a></li>
					{{-- <li><a href="{{ route('404') }}">{{ ucfirst(__('modulos.permissões')) }}</a></li> --}}
				</ul>
			</li>
		</ul>

	</div>
</nav>
