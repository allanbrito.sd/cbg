@include('layouts.themes.top-navigation')

<div class="row border-bottom white-bg">
	<nav class="navbar navbar-static-top" role="navigation">
		<div class="navbar-header">
			<button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
				<i class="fa fa-reorder"></i>
			</button>
			<a href="#" class="navbar-brand">{{ env('APP_NAME') }}</a>
		</div>
		<div class="navbar-collapse collapse" id="navbar">
			<ul class="nav navbar-top-links navbar-right">
				<li class="active">
					<a>Seja bem-vindo {{ Auth::user()->pessoa->primeiroNome }}!&nbsp;</a>
				</li>
				<li class="hidden-sm" style="margin-left: -21px">
					<a class="no-padding-left">Responda as perguntas para prepararmos o sistema ideal para você</a>
				</li>
				</li class="hidden-sm"><a>&nbsp;</a></li>
			</ul>
		</div>
	</nav>
</div>