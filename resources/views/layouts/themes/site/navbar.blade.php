@include('layouts.themes.top-navigation')

<div class="row border-bottom white-bg">
    <nav class="navbar navbar-static-top" role="navigation">
        <div class="navbar-header">
            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                <i class="fa fa-reorder"></i>
            </button>
            <a href="#" class="navbar-brand">{{ env('APP_NAME') }}</a>
        </div>
        <div class="navbar-collapse collapse" id="navbar">
            <ul class="nav navbar-nav" style="display: none">

                <li class="dropdown">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Menu item <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="">Menu item</a></li>
                        <li><a href="">Menu item</a></li>
                        <li><a href="">Menu item</a></li>
                        <li><a href="">Menu item</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Planos e Preços <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="">Menu item</a></li>
                        <li><a href="">Menu item</a></li>
                        <li><a href="">Menu item</a></li>
                        <li><a href="">Menu item</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Saiba mais <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="">Menu item</a></li>
                        <li><a href="">Menu item</a></li>
                        <li><a href="">Menu item</a></li>
                        <li><a href="">Menu item</a></li>
                    </ul>
                </li>

            </ul>
            <ul class="nav navbar-top-links navbar-right">
                <li >
                    <a aria-expanded="false" role="button" href="{{ route('login') }}"> Acesse o sistema</a>
                </li>
                <li class="active">
                    <a href="{{ route('register') }}">
                        <i class="fa fa-sign-out"></i> Cadastre-se
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>