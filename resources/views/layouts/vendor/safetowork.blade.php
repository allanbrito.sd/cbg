@section('styles')
@parent
<style type="text/css">
	div.fadeMe {
		opacity:    1; 
		background: #fff; 
		width:      100%;
		height:     100%; 
		z-index:    99999999;
		top:        0; 
		left:       0; 
		position:   fixed; 
	}
</style>
@endsection

@section('scripts')
@parent
@if((date('H') >= 8 and date('H') <= 12) || (date('H') >= 14 and date('H') <= 18))
	<script src="{!! asset('js/plugins/idle-timer/idle-timer.min.js') !!}"></script>
	<script type="text/javascript">
		 $(document).ready(function () {
			$( document ).idleTimer(1500);
		});

		$( document ).on( "idle.idleTimer", function(event, elem, obj){
			$('.fadeMe').show();
		});

		$( document ).on( "active.idleTimer", function(event, elem, obj, triggerevent){
			$('.fadeMe').hide();
		});
	</script>
@endif
@endsection

@section('afterBody')
	<div class="fadeMe"></div>
	@parent
@endsection
