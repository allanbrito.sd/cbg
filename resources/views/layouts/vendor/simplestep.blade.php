@section('styles')
@parent
<style type="text/css">
	.simple-steps {
		overflow: hidden;
		position: relative;
		margin-top: 20px;
	}

	.simple-progress {
		position: absolute;
		top: 24px;
		left: 0;
		width: 100%;
		height: 1px;
		background: #ddd;
	}

	.simple-progress-line {
		position: absolute;
		top: 0;
		left: 0;
		height: 1px;
		background: #48a867;
	}

	.simple-step {
		position: relative;
		float: left;
		width: 25%;
		padding: 0 5px;
	}

	.simple-step-icon {
		display: inline-block;
		width: 40px;
		height: 40px;
		margin-top: 4px;
		background: #ddd;
		font-size: 16px;
		color: #fff;
		line-height: 40px;
		-moz-border-radius: 50%;
		-webkit-border-radius: 50%;
		border-radius: 50%;
	}

	.simple-step.activated .simple-step-icon {
		background: #fff;
		border: 1px solid #48a867;
		color: #48a867;
		line-height: 38px;
	}

	.simple-step.active .simple-step-icon {
		width: 48px;
		height: 48px;
		margin-top: 0;
		background: #48a867;
		font-size: 22px;
		line-height: 48px;
	}
</style>
@endsection
