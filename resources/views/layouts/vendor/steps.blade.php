@section('styles')
	@parent
	<link rel="stylesheet" href="{!! asset('css/plugins/steps/jquery.steps.css') !!}" />
@endsection

@section('scripts')
	@parent
	<script src="{!! asset('js/plugins/steps/jquery.steps.min.js') !!}" type="text/javascript"></script>
@endsection