@section('styles')
	@parent
	<link rel="stylesheet" href="{!! asset('css/plugins/switchery/switchery.css') !!}" />
@endsection

@section('scripts')
	@parent
	<script src="{!! asset('js/plugins/switchery/switchery.js') !!}" type="text/javascript"></script>
@endsection