@extends('layouts.app')

@include('layouts.vendor.datatable')

@section('title', ucfirst(__('modulos.modalidades')))

@section('migalha')
@parent
<li class="active"><a href="{{ route('modalidade.consultar') }}">{{ ucfirst(__('modulos.modalidades')) }}</a></li>
<li><strong>Consultar</strong></li>
@endsection

@section('acoes')
<a class="btn btn-success" href="{{ route('modalidade.novo') }}"><i class="fa fa-plus-square"></i> Novo</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="ibox">
				<div class="ibox-content">
					@if($modalidades->count() > 0)
						@include('modalidades.partials.tabela')
					@else
						@include('modalidades.partials.sem_registro')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	@parent
	<script type="text/javascript">

		$(document).ready(function(){
			var $columns = $('thead th:not(.dt-no-export)', $('.tableModalidade'));
			var tableModalidade = $('.tableModalidade').DataTable({
				columnDefs: [
					{'aTargets': [1], 'bSortable': false},
					{'aTargets': [1], 'searchable': false}
				],
				buttons: [
					{
		            	extend: 'csv',
		            	text: 'CSV',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'excel',
		            	text: 'Excel',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'pdf',
		            	text: 'PDF',
		            	exportOptions: {
				            columns: $columns
				        }
		            }
		        ]
			});

			$(document).on('keyup', '.filtroModalidade', function() {
				tableModalidade.search($(this).val()).draw() ;
			});
		});

	</script>

@endsection
