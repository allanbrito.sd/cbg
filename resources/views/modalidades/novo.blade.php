@extends('layouts.app')

@section('title', "Cadastrar ".__('modulos.modalidade'))

@section('migalha')
@parent
<li><a href="{{ route('modalidade.consultar') }}">{{ ucfirst(__('modulos.modalidades')) }}</a></li>
<li class="active"><strong>Novo</strong></li>
@endsection

@section('acoes')
<a class="btn" href="{{ route('modalidade.consultar') }}"><i class="fa fa-mail-reply"></i> Voltar</a>
<a class="btn btn-success" role="submit" href="{{ route('modalidade.cadastrar') }}"><i class="fa fa-save"></i> Salvar</a>
@endsection

@section('content')
<form role="form" method="POST" action="{{ route('modalidade.cadastrar') }}">
	@method('POST')

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content">
						@define($prefixo = 'formulario')
						@include('modalidades.partials.formulario')
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

