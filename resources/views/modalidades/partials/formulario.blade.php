@define($modalidade = $modalidade ?? null)
@define($prefixoInput = $prefixo ?? null)
@define($prefixo = $prefixo ?? null)

<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-body">
		<div class="row">
	        <input type="hidden" name="{{ $prefixoInput }}[modalidade][id]" value="{{ old($prefixo.'modalidade.id') ?? $modalidade->id ?? '' }}">
	        <div class="form-group col-md-3{{ $errors->has($prefixo.'modalidade.nome') ? ' has-error' : '' }}">
	            <label class="control-label">Nome <span style="color:#f00">*</span></label>
	            <input type="text" class="form-control" name="{{ $prefixoInput }}[modalidade][nome]" value="{{ old($prefixo.'modalidade.nome') ?? $modalidade->nome ?? '' }}">
	            @showError($prefixo.'modalidade.nome')
	        </div>
		</div>
	</div>
</div>