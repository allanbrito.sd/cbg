@extends('layouts.app')

@include('layouts.vendor.datatable')

@section('title', 'Pedidos')

@section('migalha')
@parent
<li class="active"><a href="{{ route('pedido.consultar') }}">Pedidos</a></li>
<li><strong>Consultar</strong></li>
@endsection

@section('acoes')
<a class="btn btn-success" href="{{ route('pedido.novo') }}"><i class="fa fa-plus-square"></i> Novo</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-md-12">
			<div class="ibox">
				@if($pedidos->count() > 0)
					@include('pedidos.partials.tabela')
				@else
					@include('pedidos.partials.sem_registro')
				@endif
			</div>
		</div>
	</div>
</div>
@endsection
