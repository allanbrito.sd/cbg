@define($prefixoInput = !empty($prefixo) ? $prefixo.'[pedido]'  : 'pedido')
@define($prefixo = !empty($prefixo) ? $prefixo.'.pedido.'  : 'pedido.')

<div class="pedido_container">
	<div class="row">

		<div class="form-group col-md-3{{ $errors->has($prefixo.'vendedor') ? ' has-error' : '' }}">
			<label class="control-label">{{ ucfirst(__('modulos.vendedor')) }}</label>
			<select class="chosen-select" name="{{ $prefixoInput }}[vendedor_id]">
					<option value="">Selecione uma opção</option>
				@foreach($vendedores ?? [] as $vendedor)
					<option value="{{ $vendedor->id }}" {{ (old($prefixo.'vendedor_id') ?? $pedido->vendedor->id ?? Auth()->user()->pessoa->vendedor->id ??null) == $vendedor->id ? 'selected=selected' : '' }}>
						{{ $vendedor->pessoa->nome }}
					</option>
				@endforeach
			</select>
			@showError($prefixo.'vendedor_id')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'cliente') ? ' has-error' : '' }}">
			<label class="control-label">{{ ucfirst(__('modulos.cliente')) }}</label>
			<select class="chosen-select" name="{{ $prefixoInput }}[cliente_id]">
					<option value="">Selecione uma opção</option>
				@foreach($clientes as $cliente)
					<option value="{{ $cliente->id }}" {{ (old($prefixo.'cliente_id') ?? $pedido->cliente->id ?? null) == $cliente->id ? 'selected=selected' : '' }}>
						{{ $cliente->pessoa->nome }}
					</option>
				@endforeach
			</select>
			@showError($prefixo.'cliente_id')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'tipo') ? ' has-error' : '' }}">
			<label class="control-label">Tipo</label>
			<select class="chosen-select" name="{{ $prefixoInput }}[tipo]">
				@foreach($pedidoTipos as $id => $tipo)
					<option value="{{ $id }}" data-value="{{ $tipo }}" {{ (old($prefixo.'tipo') ?? $pedido->tipo ?? $pedidoTipos->search('Orçamento')) == $id ? 'selected=selected' : '' }}>{{ $tipo }}</option>
				@endforeach
			</select>
			@showError($prefixo.'tipo')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'representada') ? ' has-error' : '' }}">
			<label class="control-label">{{ ucfirst(__('modulos.representada')) }}</label>
			<select class="chosen-select" name="{{ $prefixoInput }}[representada_id]">
					<option value="">Selecione uma opção</option>
				@foreach($representadas as $representada)
					<option value="{{ $representada->id }}" {{ (old($prefixo.'representada_id') ?? $pedido->representada->id ?? null) == $representada->id ? 'selected=selected' : '' }}>
						{{ $representada->pessoa->nome }}
					</option>
				@endforeach
			</select>
			@showError($prefixo.'cliente_id')
		</div>

	</div>
</div>

@section('styles')
@parent
	<style type="text/css">
	</style>
@endsection

@section('scripts')
@parent
	<script type="text/javascript">
	</script>
@endsection
