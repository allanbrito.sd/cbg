<div class="ibox float-e-margins">
	<div class="ibox-content text-center">
		<svg class="sem-registro"><use href="{{ asset('images/general.svg#spreadsheet-check') }}"></use></svg>
		<h2 class="sem-registro text-primary">Nenhum <span class="">{{ __('modulos.pedido') }}</span> cadastrado</h3>
		<div class="row text-left">
			<div class="col-md-12">
				<h3 class="text-info">Recursos</h3>
					<p><i class="fa fa-check"></i> Gerencie os {{ __('modulos.pedidos') }} de venda de sua {{ __('modulos.representada') }} de forma rápida e prática</p>
					<p><i class="fa fa-check"></i> Faça lançamentos de contas a receber</p>
					<p><i class="fa fa-check"></i> Controle seu estoque</p>
					<p><i class="fa fa-check"></i> Agrupe seus {{ __('modulos.pedidos') }}</p>
					<p><i class="fa fa-check"></i> Envie os {{ __('modulos.pedidos') }} por e-mail para seus {{ __('modulos.clientes') }}</p>
			</div>
		</div>
	</div>
</div>