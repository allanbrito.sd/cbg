@extends('layouts.app')

@include('layouts.vendor.datatable')

@section('title', 'Pessoas')

@section('migalha')
@parent
<li class="active"><a href="{{ route('pessoa.consultar') }}">Pessoas</a></li>
<li><strong>Consultar</strong></li>
@endsection

@section('acoes')
<a class="btn btn-success" href="{{ route('pessoa.novo') }}"><i class="fa fa-plus-square"></i> Novo</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="ibox">
				<div class="ibox-content">
					<div class="row">
						<div class="col-md-12">
							<div class="input-group">
								<input type="text" placeholder="Pesquisar por nome ou cpf " class="input form-control filtroPessoaFisica">
								<span class="input-group-btn">
									<button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i></button>
								</span>
							</div>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-sm-12">
							<div>
								<ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#tab-pessoa-fisica" id="tab-pessoaFisica"><i class="fa fa-user"></i> Pessoas</a></li>
									<li class=""><a data-toggle="tab" href="#tab-pessoa-juridica" id="tab-pessoaJuridica"><i class="fa fa-briefcase"></i> Empresas</a></li>
								</ul>
								<div class="tab-content">
									<div id="tab-pessoa-fisica" class="tab-pane active">
										<div class="full-height-scroll">
											<div class="table-responsive">
												<table class="table table-striped table-bordered table-hover mt-0 tablePessoaFisica">
													<thead>
														<tr>
															<th>Nome</th>
															<th>CPF</th>
															<th>Telefones</th>
															<th>Situação</th>
															<th class="dt-no-export">Ações</th>
														</tr>
													</thead>
													<tbody>
													@foreach($pessoasFisicas as $pessoa)
														<tr class="pointer visualizarPessoa">
															<td><img class="gravatar" src="{{ asset($pessoa->foto) }}"> {{ $pessoa->nomeESobrenome }}</td>
															<td> {{ $pessoa->cpf }}</td>
															<td><i class="fa fa-phone"> </i> {{ $pessoa->telefones ?? 'Nenhum informado'}}</td>
															<td class="client-status"><span class="label label-primary">Ativo</span></td>
															<td class="text-center acoes" nowrap width="5%">
																<i class="fa fa-search fa-1-4x has-tooltip" data-placement="bottom" title="Visualizar"></i>
																<a href="{{ route('pessoa.editar', ['id' => $pessoa->id]) }}"><i class="fa fa-edit fa-1-4x has-tooltip" data-placement="bottom" title="Editar"></i></a>
																<form method="POST" action="{{ route('pessoa.apagar', ['id' => $pessoa->id]) }}">
																	@method('DELETE')
																	<a role="submit"><i class="fa fa-1-4x fa-trash text-danger" data-placement="bottom" title="Apagar"></i></a>
																</form>
															</td>
														</tr>
													@endforeach
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div id="tab-pessoa-juridica" class="tab-pane">
										<div class="full-height-scroll">
											<div class="table-responsive">
												<table class="table table-striped table-bordered table-hover mt-0 tablePessoaJuridica">
													<thead>
														<tr>
															<th>Nome</th>
															<th>CNPJ</th>
															<th>Telefones</th>
															<th>Situação</th>
														</tr>
													</thead>
													<tbody>
													@foreach($pessoasJuridicas as $pessoa)
														<tr>
															<td><img class="gravatar" src="{{ asset($pessoa->foto) }}"> {{ $pessoa->nome }}</td>
															<td> {{ $pessoa->cnpj }}</td>
															<td><i class="fa fa-phone"> </i> {{ $pessoa->telefones ?? 'Nenhum informado'}}</td>
															<td class="client-status"><span class="label label-primary">Ativo</span></td>
														</tr>
													@endforeach
													</tbody>
												</table>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	@parent
	<script type="text/javascript">

		$(document).ready(function(){
			var $columns = $('thead th:not(.dt-no-export)', $('.tablePessoaFisica'));
	
			var tablePessoaFisica = $('.tablePessoaFisica').DataTable({
				columnDefs: [
					{'aTargets': [1,2,4], 'bSortable': false},
					{'aTargets': [2,4], 'searchable': false}
				],
				buttons: [
					{
		            	extend: 'csv',
		            	text: 'CSV',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'excel',
		            	text: 'Excel',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'pdf',
		            	text: 'PDF',
		            	exportOptions: {
				            columns: $columns
				        }
		            }
		        ]
			});
			var $columns = $('thead th:not(.dt-no-export)', $('.tablePessoaJuridica'));
	
			var tablePessoaJuridica = $('.tablePessoaJuridica').DataTable({
				columnDefs: [
					{'aTargets': [1,2/*,4*/], 'bSortable': false},
					{'aTargets': [2/*,4*/], 'searchable': false},
				],
				buttons: [
					{
		            	extend: 'csv',
		            	text: 'CSV',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'excel',
		            	text: 'Excel',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'pdf',
		            	text: 'PDF',
		            	exportOptions: {
				            columns: $columns
				        }
		            }
		        ]
			});

			$(document).on('keyup', '.filtroPessoaFisica', function() {
				tablePessoaFisica.search($(this).val()).draw() ;
			});

			$(document).on('keyup', '.filtroPessoaJuridica', function() {
				tablePessoaJuridica.search($(this).val()).draw() ;
			});
		});

	</script>

@endsection
