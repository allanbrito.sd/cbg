@extends('layouts.app')

@section('title', "Cadastrar ".__('modulos.pessoa'))

@section('migalha')
@parent
<li><a href="{{ route('pessoa.consultar') }}">{{ ucfirst(__('modulos.pessoas')) }}</a></li>
<li class="active"><strong>Novo</strong></li>
@endsection

@section('acoes')
<a class="btn" href="{{ route('pessoa.consultar') }}"><i class="fa fa-mail-reply"></i> Voltar</a>
<a class="btn btn-success" role="submit" href="{{ route('pessoa.cadastrar') }}"><i class="fa fa-save"></i> Salvar</a>
@endsection

@section('content')
<form role="form" method="POST" action="{{ route('pessoa.cadastrar') }}">
	@method('POST')

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content">
						@define($prefixo = 'formulario')
						@include('pessoas.partials.formulario')
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

