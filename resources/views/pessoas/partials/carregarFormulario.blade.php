@include('pessoas.partials.formularioContent')

<script type="text/javascript">
	$('select.chosen-select-search').chosen({width: "100%"});
	$('select.chosen-select').chosen({width: "100%", disable_search: true});

	$('.data').mask("00/00/0000", {/*placeholder: "__/__/____", */selectOnFocus: true, clearIfNotMatch: true});
	$('.hora').mask('00:00:00', {/*placeholder: "__:__:__", */selectOnFocus: true, clearIfNotMatch: true});
	$('.data_hora').mask('00/00/0000 00:00:00', {/*placeholder: "__/__/____ __:__:__", */selectOnFocus: true, clearIfNotMatch: true});
	$('.cep').mask('00.000-000', {/*placeholder: "__.___-___", */selectOnFocus: true, clearIfNotMatch: true});
	var telefoneMaskBehavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
		onKeyPress: function(val, e, field, options) {
		}
	};
	$('.telefone').mask(telefoneMaskBehavior, spOptions);
	$('.cpf').mask('000.000.000-00', {/*placeholder: '___.___.___-__', */reverse: true});
	$('.cnpj').mask('00.000.000/0000-00', {/*placeholder: '__.___.___/____-__', */reverse: true});
	$('.valor').mask('000.000.000.000.000,00', {reverse: true});
	$('.valor2').mask("#.##0,00", {reverse: true});
	$('.porcentagem').mask('##0,00%', {reverse: true});
</script>