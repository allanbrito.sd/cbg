@define($prefixoInput = !empty($prefixo) ? $prefixo.'[contato][]'  : 'contato[]')
@define($prefixo = !empty($prefixo) ? $prefixo.'.contato.*.'  : 'contato.*.')

<div class="contato_container">
	<div class="row">
		<div class="form-group col-md-3{{ $errors->has($prefixo.'nome') ? ' has-error' : '' }}">
			<label class="control-label">Pessoa de contato</label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[nome]" value="{{ old($prefixo.'nome') ?? $contato->nome ?? '' }}">
			@showError($prefixo.'nome')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'telefone') ? ' has-error' : '' }}">
			<label class="control-label">Telefone</label>
			<input i type="text" class="form-control" name="{{ $prefixoInput }}[telefone]" value="{{ old($prefixo.'telefone') ?? $contato->telefone ?? '' }}">
			@showError($prefixo.'telefone')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'celular') ? ' has-error' : '' }}">
			<label class="control-label">Celular</label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[celular]" value="{{ old($prefixo.'celular') ?? $contato->celular ?? '' }}">
			@showError($prefixo.'celular')
		</div>

		<div class="form-group col-md-2{{ $errors->has($prefixo.'ramal') ? ' has-error' : '' }}">
			<label class="control-label">Ramal</label>
			<input i type="text" class="form-control" name="{{ $prefixoInput }}[ramal]" value="{{ old($prefixo.'ramal') ?? $contato->ramal ?? '' }}">
			@showError($prefixo.'ramal')
		</div>
	</div>

	<div class="row">
		<div class="form-group col-md-3{{ $errors->has($prefixo.'fax') ? ' has-error' : '' }}">
			<label class="control-label">Fax</label>
			<input i type="text" class="form-control" name="{{ $prefixoInput }}[fax]" value="{{ old($prefixo.'fax') ?? $contato->fax ?? '' }}">
			@showError($prefixo.'fax')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'email') ? ' has-error' : '' }}">
			<label class="control-label">Email</label>
			<input i type="text" class="form-control" name="{{ $prefixoInput }}[email]" value="{{ old($prefixo.'email') ?? $contato->email ?? '' }}">
			@showError($prefixo.'email')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'website') ? ' has-error' : '' }}">
			<label class="control-label">Website</label>
			<input i type="text" class="form-control" name="{{ $prefixoInput }}[website]" value="{{ old($prefixo.'website') ?? $contato->website ?? '' }}">
			@showError($prefixo.'website')
		</div>
	</div>
</div>