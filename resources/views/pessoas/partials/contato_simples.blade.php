@define($prefixoInput = !empty($prefixo) ? $prefixo.'[contato]'  : 'contato')
@define($prefixo = !empty($prefixo) ? $prefixo.'.contato.'  : 'contato.')

<div class="contato_simples_container">
    <div class="row">
        <input type="hidden" name="{{ $prefixoInput }}[id]" value="{{ old($prefixo.'id') ?? $contato->id ?? '' }}">
        <div class="form-group col-md-3{{ $errors->has($prefixo.'telefone1') ? ' has-error' : '' }}">
            <label class="control-label">Telefone 1</label>
            <input type="text" class="form-control telefone" name="{{ $prefixoInput }}[telefone1]" value="{{ old($prefixo.'telefone1') ?? $contato->telefone1 ?? '' }}">
            @showError($prefixo.'telefone1')
        </div>

        <div class="form-group col-md-3{{ $errors->has($prefixo.'telefone2') ? ' has-error' : '' }}">
            <label class="control-label">Telefone 2</label>
            <input type="text" class="form-control telefone" name="{{ $prefixoInput }}[telefone2]" value="{{ old($prefixo.'telefone2') ?? $contato->telefone2 ?? '' }}">
            @showError($prefixo.'telefone2')
        </div>

        <div class="form-group col-md-3{{ $errors->has($prefixo.'email') ? ' has-error' : '' }}">
            <label class="control-label">Email</label>
            <input type="mail" class="form-control email" name="{{ $prefixoInput }}[email]" value="{{ old($prefixo.'email') ?? $contato->email ?? '' }}">
            @showError($prefixo.'email')
        </div>
	</div>
</div>
