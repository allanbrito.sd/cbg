@define($prefixoInput = !empty($prefixo) ? $prefixo.'[dadosBancarios]'  : 'dadosBancarios')
@define($prefixo = !empty($prefixo) ? $prefixo.'.dadosBancarios.'  : 'dadosBancarios.')

<div class="contato_container">
	<div class="row">
        <input type="hidden" name="{{ $prefixoInput }}[id]" value="{{ old($prefixo.'id') ?? $dadosBancarios->id ?? '' }}">
		<div class="form-group col-md-3{{ $errors->has($prefixo.'numero_banco') ? ' has-error' : '' }}">
			<label class="control-label">N &ordm; Banco</label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[numero_banco]" value="{{ old($prefixo.'numero_banco') ?? $dadosBancarios->numero_banco ?? '' }}">
			@showError($prefixo.'numero_banco')
		</div>
		<div class="form-group col-md-3{{ $errors->has($prefixo.'nome_banco') ? ' has-error' : '' }}">
			<label class="control-label">Banco</label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[nome_banco]" value="{{ old($prefixo.'nome_banco') ?? $dadosBancarios->nome_banco ?? '' }}">
			@showError($prefixo.'nome_banco')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'agencia') ? ' has-error' : '' }}">
			<label class="control-label">Agência</label>
			<input i type="text" class="form-control" name="{{ $prefixoInput }}[agencia]" value="{{ old($prefixo.'agencia') ?? $dadosBancarios->agencia ?? '' }}">
			@showError($prefixo.'agencia')
		</div>
		<div class="form-group col-md-3{{ $errors->has($prefixo.'operacao') ? ' has-error' : '' }}">
			<label class="control-label">Operação</label>
			<input i type="text" class="form-control" name="{{ $prefixoInput }}[operacao]" value="{{ old($prefixo.'operacao') ?? $dadosBancarios->operacao ?? '' }}">
			@showError($prefixo.'operacao')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'conta') ? ' has-error' : '' }}">
			<label class="control-label">Conta corrente</label>
			<input i type="text" class="form-control" name="{{ $prefixoInput }}[conta]" value="{{ old($prefixo.'conta') ?? $dadosBancarios->conta ?? '' }}">
			@showError($prefixo.'conta')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'tipo') ? ' has-error' : '' }}">
			<label class="control-label">Tipo</label>
			<input i type="text" class="form-control" name="{{ $prefixoInput }}[tipo]" value="{{ old($prefixo.'tipo') ?? $dadosBancarios->tipo ?? '' }}">
			@showError($prefixo.'tipo')
		</div>
	</div>
</div>
