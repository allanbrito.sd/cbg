@define($prefixoInput = !empty($prefixo) ? $prefixo.'[endereco]'  : 'endereco')
@define($prefixo = !empty($prefixo) ? $prefixo.'.endereco.'  : 'endereco.')

<div class="endereco_container">
    <div class="row">
        <input type="hidden" name="{{ $prefixoInput }}[id]" value="{{ old($prefixo.'id') ?? $endereco->id ?? '' }}">
		<div class="form-group col-md-3{{ $errors->has($prefixo.'cep') ? ' has-error' : '' }}">
			<label class="control-label">CEP</label>
			<div class="input-group">
				<input type="text" class="form-control cep" name="{{ $prefixoInput }}[cep]" value="{{ old($prefixo.'cep') ?? $endereco->cep ?? '' }}">
				<span class="input-group-btn"><button type="button" class="btn btn-primary consultarCep"><i class="fa fa-search"></i></button></span>
			</div>
			@showError($prefixo.'cep')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'estado_id') ? ' has-error' : '' }}">
			<label class="control-label">Estado</label>
			<select class="chosen-select-search form-control estado" name="{{ $prefixoInput }}[estado_id]">
				<option value="">Selecione</option>
				@foreach($estados as $estado)
					<option data-sigla = "{{ $estado->sigla }}" value="{{ $estado->id }}" {{ (old($prefixo.'estado_id') ?? $endereco->estado_id ?? '') == $estado->id ? 'selected=selected' : '' }}>{{ $estado->nome }}</option>
				@endforeach
			</select>
			@showError($prefixo.'estado_id')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'cidade') ? ' has-error' : '' }}">
			<label class="control-label">Cidade</label>
			<input type="text" class="form-control cidade" name="{{ $prefixoInput }}[cidade]" value="{{ old($prefixo.'cidade') ?? $endereco->cidade ?? '' }}">
			@showError($prefixo.'cidade')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'bairro') ? ' has-error' : '' }}">
			<label class="control-label">Bairro</label>
			<input type="text" class="form-control bairro" name="{{ $prefixoInput }}[bairro]" value="{{ old($prefixo.'bairro') ?? $endereco->bairro ?? '' }}">
			@showError($prefixo.'bairro')
		</div>
	</div>

	<div class="row">
		<div class="form-group col-md-5{{ $errors->has($prefixo.'logradouro') ? ' has-error' : '' }}">
			<label class="control-label">Endereço</label>
			<input type="text" class="form-control logradouro" name="{{ $prefixoInput }}[logradouro]" value="{{ old($prefixo.'logradouro') ?? $endereco->logradouro ?? '' }}">
			@showError($prefixo.'logradouro')
		</div>
		<div class="form-group col-md-1{{ $errors->has($prefixo.'numero') ? ' has-error' : '' }}">
			<label class="control-label">Número</label>
			<input type="text" class="form-control numero" name="{{ $prefixoInput }}[numero]" value="{{ old($prefixo.'numero') ?? $endereco->numero ?? '' }}">
			@showError($prefixo.'numero')
		</div>
		<div class="form-group col-md-6{{ $errors->has($prefixo.'complemento') ? ' has-error' : '' }}">
			<label class="control-label">Complemento</label>
			<input type="text" class="form-control complemento" name="{{ $prefixoInput }}[complemento]" value="{{ old($prefixo.'complemento') ?? $endereco->complemento ?? '' }}">
			@showError($prefixo.'complemento')
		</div>
	</div>
</div>

@section('styles')
@parent
	<style type="text/css">
		.endereco_container .juridica {
			display: none;
		}
	</style>
@endsection

@section('scripts')
@parent
	<script type="text/javascript">
		var Endereco_JS = Endereco_JS || {
			carregado: false,
			limpaFormularioCep: function($container) {
				$container.find();
				$container.find(".logradouro").val("");
				$container.find(".complemento").val("");
				$container.find(".bairro").val("");
				$container.find(".cidade").val("");
				$container.find(".estado").val("").trigger('chosen:updated');
			},
			carregarCep: function($container) {
				var cep = $container.find('.cep').val().replace(/\D/g, ''),
					self = this;

				if (cep == "") {
					// self.limpaFormularioCep();
					return;
				}

				var validacep = /^[0-9]{8}$/;

				if(!validacep.test(cep)) {
					self.limpaFormularioCep($container);
					return;
				}

				$container.find(".logradouro").val("...");
				$container.find(".complemento").val("...");
				$container.find(".bairro").val("...");
				$container.find(".cidade").val("...");
				// $container.find(".estado").val("...").trigger('chosen:updated');

				$.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
					if (!("erro" in dados)) {
						$container.find(".logradouro").val(dados.logradouro);
						$container.find(".complemento").val(dados.complemento);
						$container.find(".bairro").val(dados.bairro);
						$container.find(".cidade").val(dados.localidade);
						$container.find(".estado").val($container.find(".estado option[data-sigla='"+dados.uf+"']").val()).trigger('chosen:updated');
						a = $container.find(".estado");
						b = dados.uf;
					} else {
						self.limpaFormularioCep();
					}
				});
			},
			init: function() {
				if (this.carregado) {
					return;
				}

				var self = this;
	 			$(document).ready(function() {
					// self.carregarCep();
					$(document).on('click', '.endereco_container .consultarCep', function() {
						self.carregarCep($(this).parents('.endereco_container'));
					});
				});

				this.carregado = true;
			}
		};

		Endereco_JS.init();
	</script>
@endsection
