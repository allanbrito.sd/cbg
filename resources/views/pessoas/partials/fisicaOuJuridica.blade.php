@define($uniqid = uniqid())
@define($prefixoInput = !empty($prefixo) ? $prefixo.'[pessoa]'  : 'pessoa')
@define($prefixoOriginal = $prefixo ?? null)
@define($prefixo = !empty($prefixo) ? $prefixo.'.pessoa.'  : 'pessoa.')

<div class="pessoa_container">
	<input type="hidden" name="{{ $prefixoInput }}[id]" value="{{ old($prefixo.'id') ?? $pessoa->pessoa_id ?? $pessoa->id ?? '' }}">
	<div class="row">
		<div class="form-group col-md-3{{ $errors->has($prefixo.'tipo') ? ' has-error' : '' }}" style="{{ !empty($pessoaOnly) ? 'display:none' : '' }}">
 			<label class="control-label">Tipo de pessoa</label>
			<select class="chosen-select pessoaTipo" name="{{ $prefixoInput }}[tipo]">
				@foreach($pessoaTipos as $id => $tipo)
					<option value="{{ $id }}" data-value="{{ $tipo }}" {{ (old($prefixo.'tipo') ?? $pessoa->tipo ?? $pessoaOnly ?? $pessoaTipos->search('Física')) == $id ? 'selected=selected' : '' }}>{{ $tipo }}</option>
				@endforeach
			</select>
			@showError($prefixo.'tipo')
		</div>

		<div class="form-group col-md-{{ !empty($pessoaOnly) ? '3' : '6' }}{{ $errors->has($prefixo.'nome') ? ' has-error' : '' }}">
			<label class="control-label fisica">Nome <span style="color:#f00">*</span></label>
			<label class="control-label juridica">Nome/Razão Social <span style="color:#f00">*</span></label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[nome]" value="{{ old($prefixo.'nome') ?? $pessoa->nome ?? '' }}">
			@showError($prefixo.'nome')
		</div>
		<div class="form-group col-md-3 fisica{{ $errors->has($prefixo.'cpf') ? ' has-error' : '' }}">
			<label class="control-label">CPF {!! ($requiredInputs['cpf'] ?? false) ? '<span style="color:#f00">*</span>' : '' !!}</label>
			<input type="text" class="form-control cpf" name="{{ $prefixoInput }}[cpf]" value="{{ old($prefixo.'cpf') ?? $pessoa->cpf ?? '' }}">
			@showError($prefixo.'cpf')
		</div>
		<div class="form-group col-md-3 juridica{{ $errors->has($prefixo.'cnpj') ? ' has-error' : '' }}">
			<label class="control-label">CNPJ {!! ($requiredInputs['cnpj'] ?? false) ? '<span style="color:#f00">*</span>' : '' !!}</label>
			<input type="text" class="form-control cnpj" name="{{ $prefixoInput }}[cnpj]" value="{{ old($prefixo.'cnpj') ?? $pessoa->cnpj ?? '' }}">
			@showError($prefixo.'cnpj')
		</div>
		<div class="form-group col-md-3 fisica{{ $errors->has($prefixo.'data_nascimento') ? ' has-error' : '' }}">
			<label class="control-label">Data de Nascimento {!! ($requiredInputs['data_nascimento'] ?? false) ? '<span style="color:#f00">*</span>' : '' !!}</label>
			<input type="text" class="form-control data data_nascimento" name="{{ $prefixoInput }}[data_nascimento]" value="{{ old($prefixo.'data_nascimento') ?? (!empty($pessoa->data_nascimento) ? $pessoa->data_nascimento->format('d/m/Y') : null) ?? '' }}">
			@showError($prefixo.'data_nascimento')
		</div>
		<div class="form-group col-md-3 fisica{{ $errors->has($prefixo.'rg') ? ' has-error' : '' }}">
			<label class="control-label">RG {!! ($requiredInputs['rg'] ?? false) ? '<span style="color:#f00">*</span>' : '' !!}</label>
			<input type="text" class="form-control rg" name="{{ $prefixoInput }}[rg]" value="{{ old($prefixo.'rg') ?? $pessoa->rg ?? '' }}">
			@showError($prefixo.'rg')
		</div>
		<div class="form-group col-md-3 fisica{{ $errors->has($prefixo.'rg_orgao_expedidor') ? ' has-error' : '' }}">
			<label class="control-label">Orgão Expeditor {!! ($requiredInputs['rg_orgao_expedidor'] ?? false) ? '<span style="color:#f00">*</span>' : '' !!}</label>
			<input type="text" class="form-control rg_orgao_expedidor" name="{{ $prefixoInput }}[rg_orgao_expedidor]" value="{{ old($prefixo.'rg_orgao_expedidor') ?? $pessoa->rg_orgao_expedidor ?? '' }}">
			@showError($prefixo.'rg_orgao_expedidor')
		</div>
		<div class="form-group col-md-3 fisica{{ $errors->has($prefixo.'rg_data_expedicao') ? ' has-error' : '' }}">
			<label class="control-label">Data de Expedição {!! ($requiredInputs['rg_data_expedicao'] ?? false) ? '<span style="color:#f00">*</span>' : '' !!}</label>
			<input type="text" class="form-control data rg_data_expedicao" name="{{ $prefixoInput }}[rg_data_expedicao]" value="{{ old($prefixo.'rg_data_expedicao') ?? (!empty($pessoa->rg_data_expedicao) ? $pessoa->rg_data_expedicao->format('d/m/Y') : null) ?? '' }}">
			@showError($prefixo.'rg_data_expedicao')
		</div>
		<div class="form-group col-md-3 fisica{{ $errors->has($prefixo.'passaporte') ? ' has-error' : '' }}">
			<label class="control-label">Passaporte {!! ($requiredInputs['passaporte'] ?? false) ? '<span style="color:#f00">*</span>' : '' !!}</label>
			<input type="text" class="form-control passaporte" name="{{ $prefixoInput }}[passaporte]" value="{{ old($prefixo.'passaporte') ?? $pessoa->passaporte ?? '' }}" maxlength="8">
			@showError($prefixo.'passaporte')
		</div>
		<div class="form-group col-md-3 fisica{{ $errors->has($prefixo.'passaporte_data_validade') ? ' has-error' : '' }}">
			<label class="control-label">Validade {!! ($requiredInputs['passaporte_data_validade'] ?? false) ? '<span style="color:#f00">*</span>' : '' !!}</label>
			<input type="text" class="form-control data passaporte_data_validade" name="{{ $prefixoInput }}[passaporte_data_validade]" value="{{ old($prefixo.'passaporte_data_validade') ?? (!empty($pessoa->passaporte_data_validade) ? $pessoa->passaporte_data_validade->format('d/m/Y') : null) ?? '' }}">
			@showError($prefixo.'passaporte_data_validade')
		</div>



		@define($prefixoAtual = $prefixo)
		@define($prefixo = $prefixoOriginal)
        <!-- @include('pessoas.partials.contato_simples') -->
		@define($prefixo = $prefixoAtual)

	</div>
	<div class="row">
		@if($pessoaWith['inscricao_municipal'] ?? false)
			<div class="form-group col-md-3 juridica{{ $errors->has($prefixo.'inscricao_municipal') ? ' has-error' : '' }}">
				<label class="control-label">Inscrição municipal</label>
				<input i type="text" class="form-control" name="{{ $prefixoInput }}[inscricao_municipal]" value="{{ old($prefixo.'inscricao_municipal') ?? $pessoa->inscricao_municipal ?? '' }}">
				@showError($prefixo.'inscricao_municipal')
			</div>
		@endif
		@if($pessoaWith['inscricao_estadual'] ?? false)
			<div class="form-group col-md-3 juridica{{ $errors->has($prefixo.'inscricao_estadual') ? ' has-error' : '' }}">
				<label class="control-label">Inscrição estadual</label>
				<input i type="text" class="form-control" name="{{ $prefixoInput }}[inscricao_estadual]" value="{{ old($prefixo.'inscricao_estadual') ?? $pessoa->inscricao_estadual ?? '' }}">
				@showError($prefixo.'inscricao_estadual')
			</div>
		@endif

	</div>
</div>

@section('styles')
@parent
	<style type="text/css">
		.pessoa_container .juridica {
			display: none;
			/* A mesma coisa que o js faz (padrão), está no css também para não quebrar a tela até js carregar*/
		}
	</style>
@endsection

@section('scripts')
@parent
	<script type="text/javascript">
		var PessoaFisicaOuJuridica_JS = PessoaFisicaOuJuridica_JS || {
			carregado: false,
			changeTipoPessoa: function() {
				$('.pessoa_container').each(function(index, el) {
					$container = $(this);
					if ($container.find('.pessoaTipo option:selected').val() == $container.find('.pessoaTipo [data-value="Física"]').val()) {
						$container.find('.juridica').hide();
						$container.find('.fisica').show();
					} else {
						$container.find('.fisica').hide();
						$container.find('.juridica').show();
					}
				});
			},
			init: function() {
				if (this.carregado) {
					return;
				}

				var self = this;
	 			$(document).ready(function() {
					self.changeTipoPessoa();
					$('.pessoa_container .pessoaTipo').on('change', self.changeTipoPessoa);
				});

				this.carregado = true;
			}
		};

		PessoaFisicaOuJuridica_JS.init();
	</script>
@endsection
