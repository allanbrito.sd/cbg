<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-header">
		<h2 class="widget-title lighter">Dados pessoais
			@if(!empty($buscarPessoa))
				<button type="button" class="btn btn-primary btnBuscarPessoa" style="margin-left: 3px; margin-bottom: 1px; padding: 3px 8px 3px 8px;"><i class="fa fa-search"></i></button>
				{{-- <input type="text" class="form-control iptBuscarPessoa hidden" placeholder="Digite Nome ou CPF/CNPJ" style="display: inline-block;width: 0px;margin-left: -6px;"> --}}
				<select data-placeholder="Digite Nome ou CPF/CNPJ" class="iptBuscarPessoa hidden chosen-select-search">
	                <option value="">Digite Nome ou CPF/CNPJ</option>
	                @foreach($pessoas ?? [] as $pessoaCombo)
	                	<option value="{{ $pessoaCombo->id }}">{{ $pessoaCombo->nome.(!empty($pessoaCombo->cpf) ? ' - '.$pessoaCombo->cpf : ((!empty($pessoaCombo->cnpj) ? ' - '.$pessoaCombo->cnpj : ''))).(!empty($pessoaCombo->atleta) ? ' (Atleta)' : (!empty($pessoaCombo->clube) ? ' (Clube)' : ($pessoaCombo->tipo == $pessoaTipos->search('Jurídica') ? ' (P. Jurídica)' : ' (P. Física)'))) }}</option>
	                @endforeach
	            </select>
	        @endif
		</h2>
	</div>
</div>
<div class="formularioPessoa">
	@include('pessoas.partials.formularioContent')
</div>
@section('scripts')
@parent
	<script type="text/javascript">
		$(document).ready(function() {
			$(".{{ $prefixo}}Div .iptBuscarPessoa+.chosen-container").addClass('hidden').attr('style', '');
			$('.{{ $prefixo}}Div .btnBuscarPessoa').on('click', function() {
				if ($(".{{ $prefixo}}Div .iptBuscarPessoa+.chosen-container").hasClass('hidden')) {
					mostrarBuscaPessoa{{ $prefixo}}();
				} else {
					esconderBuscaPessoa{{ $prefixo}}();
				}
			});

			$(".{{ $prefixo}}Div .iptBuscarPessoa").on('change', function() {
				$('.{{ $prefixo}}Div .btnBuscarPessoa').click();
				$.ajax({
				  url: "{{ URL::to('pessoa')}}/"+$(this).val()+"/carregar/{{ $prefixo }}",
				  success: function(html){
				    $(".{{ $prefixo}}Div .formularioPessoa").html(html);
				  }
				});
				// alert($(this).val());
			});
		});
		
		function mostrarBuscaPessoa{{ $prefixo}}() {
			$(".{{ $prefixo}}Div .btnBuscarPessoa").animate({"padding": '+=3px'}, 300, 'swing', function() {
				$(".{{ $prefixo}}Div .btnBuscarPessoa i").removeClass('fa-search').addClass('fa-arrow-left');
				$(".{{ $prefixo}}Div .iptBuscarPessoa+.chosen-container").removeClass('hidden').trigger('mousedown').find('.chosen-search input').focus();
				$(".{{ $prefixo}}Div .iptBuscarPessoa+.chosen-container")[0].click();
				$(".{{ $prefixo}}Div .iptBuscarPessoa+.chosen-container").animate({"width": '+=300px'});
				$(".{{ $prefixo}}Div .iptBuscarPessoa+.chosen-container").focus();
			});
		}

		function esconderBuscaPessoa{{ $prefixo}}() {
			$(".{{ $prefixo}}Div .iptBuscarPessoa+.chosen-container").animate({"width": '-=300px'}, 400, 'swing', function() {
				$(".{{ $prefixo}}Div .btnBuscarPessoa i").removeClass('fa-arrow-left').addClass('fa-search');
				$(".{{ $prefixo}}Div .btnBuscarPessoa").animate({"padding": '-=3px'}, 300);
				$(".{{ $prefixo}}Div .iptBuscarPessoa+.chosen-container").addClass('hidden');
			});
		}
	</script>
@endsection

<style type="text/css">
	.iptBuscarPessoa+.chosen-container {
		display: inline-block;
		width: 0px;
		margin-left: -6px;
		margin-top: -1px;
	}
</style>