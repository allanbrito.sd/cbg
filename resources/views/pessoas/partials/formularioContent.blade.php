<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			@define($pessoaWith = ['inscricao_municipal' => true, 'inscricao_estadual' => true])
			@include('pessoas.partials.fisicaOuJuridica')
		</div>
	</div>
</div>

<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-header">
		<h2 class="widget-title lighter">Endereço</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			@define($endereco = $pessoa->endereco ?? null)
			@include('pessoas.partials.endereco')
		</div>
	</div>
</div>

<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-header">
		<h2 class="widget-title lighter">Contatos</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			@define($contato = $pessoa->contato ?? null)
            @include('pessoas.partials.contato_simples')
		</div>
	</div>
</div>

<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-header">
		<h2 class="widget-title lighter">Dados Bancários</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			@define($dadosBancarios = $pessoa->dadosBancarios ?? null)
            @include('pessoas.partials.dados_bancarios')
		</div>
	</div>
</div>