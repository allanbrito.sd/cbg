<ul class="list-unstyled">
	@if($pessoa->nome)
		<li>Nome{{ $pessoa->isJuridica() ? ' fantasia' : '' }}: {{ $pessoa->nome }}</li>
	@endif
	@if($pessoa->razao_social)
		<li>Razão social: {{ $pessoa->razao_social }}</li>
	@endif
	@if($pessoa->cpf)
		<li>CPF: {{ $pessoa->cpf }}</li>
	@endif
	@if($pessoa->cnpj)
		<li>CNPJ: {{ $pessoa->cnpj }}</li>
	@endif
</ul>
