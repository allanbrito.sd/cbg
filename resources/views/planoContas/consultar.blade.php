@extends('layouts.app')

@include('layouts.vendor.datatable')

@section('title', ucfirst(__('modulos.planoContas')))

@section('migalha')
@parent
<li class="active"><a href="{{ ucfirst(__('modulos.planoContas')) }}">{{ ucfirst(__('modulos.planoContas')) }}</a></li>
<li><strong>Consultar</strong></li>
@endsection

@section('acoes')
<a class="btn btn-success" href="{{ route('planoConta.novo') }}"><i class="fa fa-plus-square"></i> Novo</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="ibox">
				<div class="ibox-content">
					@if($planoContas->count() > 0)
						@include('planoContas.partials.tabela')
					@else
						@include('planoContas.partials.sem_registro')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	@parent
	<script type="text/javascript">

		$(document).ready(function(){
			var $columns = $('thead th:not(.dt-no-export)', $('.tableplanoConta'));
			var tableplanoConta = $('.tableplanoConta').DataTable({
				columnDefs: [
					{'aTargets': [2], 'bSortable': false},
					{'aTargets': [2], 'searchable': false}
				],
				buttons: [
					{
		            	extend: 'csv',
		            	text: 'CSV',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'excel',
		            	text: 'Excel',
		            	exportOptions: {
				            columns: $columns
				        }
		            }, {
		            	extend: 'pdf',
		            	text: 'PDF',
		            	exportOptions: {
				            columns: $columns
				        }
		            }
		        ]
			});

			$(document).on('keyup', '.filtroplanoConta', function() {
				tableplanoConta.search($(this).val()).draw() ;
			});
		});

	</script>

@endsection
