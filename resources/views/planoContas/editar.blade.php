@extends('layouts.app')

@section('title', 'Editar '.__('modulos.planoConta'))

@section('migalha')
@parent
<li><a href="{{ route('planoConta.consultar') }}">{{ ucfirst(__('modulos.planoContas')) }}</a></li>
<li><a href="{{ route('planoConta.visualizar', ['id' => $planoConta->id]) }}">{{ $planoConta->nome }}</a></li>
<li class="active"><strong>Editar</strong></li>
@endsection

@section('acoes')
<a class="btn" href="{{ route('planoConta.consultar') }}"><i class="fa fa-mail-reply"></i> Voltar</a>
<a class="btn btn-success" role="submit" href="{{ route('planoConta.atualizar', ['id' => $planoConta->id]) }}"><i class="fa fa-save"></i> Salvar</a>
@endsection

@section('content')
<form role="form" method="POST" action="{{ route('planoConta.atualizar', ['id' => $planoConta->id]) }}">
	@method('PUT')

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content">
						@define($prefixo = 'formulario')
						@include('planoContas.partials.formulario')
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

