@define($planoConta = $planoConta ?? null)
@define($prefixoInput = !empty($prefixo) ? $prefixo.'[planoConta]'  : 'planoConta')
@define($prefixo = !empty($prefixo) ? $prefixo.'.planoConta.'  : 'planoConta.')

<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-body">
		<div class="row">
	        <input type="hidden" name="{{ $prefixoInput }}[id]" value="{{ old($prefixo.'id') ?? $planoConta->id ?? '' }}">
	        <div class="form-group col-md-3{{ $errors->has($prefixo.'codigo') ? ' has-error' : '' }}">
	            <label class="control-label">Código <span style="color:#f00">*</span></label>
	            <input type="text" class="form-control" name="{{ $prefixoInput }}[codigo]" value="{{ old($prefixo.'codigo') ?? $planoConta->codigo ?? '' }}">
	            @showError($prefixo.'codigo')
	        </div>

	        <div class="form-group col-md-3{{ $errors->has($prefixo.'nome') ? ' has-error' : '' }}">
	            <label class="control-label">Nome <span style="color:#f00">*</span></label>
	            <input type="text" class="form-control" name="{{ $prefixoInput }}[nome]" value="{{ old($prefixo.'nome') ?? $planoConta->nome ?? '' }}">
	            @showError($prefixo.'nome')
	        </div>
		</div>
	</div>
</div>