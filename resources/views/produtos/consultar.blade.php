@extends('layouts.app')

@include('layouts.vendor.datatable')

@section('title', 'Produtos')

@section('migalha')
@parent
<li class="active"><a href="{{ route('produto.consultar') }}">Produtos</a></li>
<li><strong>Consultar</strong></li>
@endsection

@section('acoes')
<a class="btn btn-success" href="{{ route('produto.novo') }}"><i class="fa fa-plus-square"></i> Novo</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-md-12">
			<div class="ibox">
				<div class="ibox-content">
					@if($produtos->count() > 0)
						@include('produtos.partials.tabela')
					@else
						@include('produtos.partials.sem_registro')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
