@define($prefixoInput = !empty($prefixo) ? $prefixo.'[produto]'  : 'produto')
@define($prefixo = !empty($prefixo) ? $prefixo.'.produto.'  : 'produto.')


<div class="row">
	<div class="form-group col-md-3{{ $errors->has($prefixo.'comissao') ? ' has-error' : '' }}">
		<label class="control-label">Comissão</label>
		<input type="text" class="form-control" name="{{ $prefixoInput }}[comissao]" value="{{ old($prefixo.'comissao') ?? $produto->comissao ?? '' }}">
		@showError($prefixo.'comissao')
	</div>
</div>

<div class="row">
	<div class="form-group col-md-12{{ $errors->has($prefixo.'informacoes_adicionais') ? ' has-error' : '' }}">
		<label class="control-label">Observações</label>
		<textarea class="form-control" name="{{ $prefixoInput }}[informacoes_adicionais]" placeholder="Adicione aqui quaisquer informações adicionais sobre este {{ _('produto') }}.">{{ old($prefixo.'informacoes_adicionais') ?? $produto->informacoes_adicionais ?? '' }}</textarea>
		@showError($prefixo.'informacoes_adicionais')
	</div>
</div>