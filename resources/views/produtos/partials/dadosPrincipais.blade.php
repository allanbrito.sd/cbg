@define($prefixoInput = !empty($prefixo) ? $prefixo.'[produto]'  : 'produto')
@define($prefixo = !empty($prefixo) ? $prefixo.'.produto.'  : 'produto.')

<div class="produto_container">
	<div class="row">
		{{-- <input type="hidden" class="form-control" name="{{ $prefixoInput }}[representada_id]" value="{{ old($prefixo.'representada_id') ?? $produto->representada_id ?? '' }}"> --}}
		<div class="form-group col-md-6{{ $errors->has($prefixo.'nome') ? ' has-error' : '' }}">
			<label class="control-label">Nome</label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[nome]" value="{{ old($prefixo.'nome') ?? $produto->nome ?? '' }}">
			@showError($prefixo.'nome')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'codigo') ? ' has-error' : '' }}">
			<label class="control-label">Código</label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[codigo]" value="{{ old($prefixo.'codigo') ?? $produto->codigo ?? '' }}">
			@showError($prefixo.'codigo')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'categoria_id') ? ' has-error' : '' }}">
			<label class="control-label">Categoria</label>
			<select class="chosen-select" name="{{ $prefixoInput }}[categoria_id]">
				<option value="">Sem categoria</option>
				@foreach($categorias as $id => $categoria)
					<option value="{{ $id }}" {{ (old($prefixo.'categoria_id') ?? $produto->categoria_id ?? null) ? 'selected=selected' : '' }}>{{ $categoria }}</option>
				@endforeach
			</select>
			@showError($prefixo.'categoria_id')
		</div>
	</div>
	{{--<div class="row">
		<div class="form-group col-md-3{{ $errors->has($prefixo.'marca') ? ' has-error' : '' }}">
			<label class="control-label">Marca</label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[marca]" value="{{ old($prefixo.'marca') ?? $produto->marca ?? '' }}">
			@showError($prefixo.'marca')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'unidade') ? ' has-error' : '' }}">
			<label class="control-label">Unidade</label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[unidade]" value="{{ old($prefixo.'unidade') ?? $produto->unidade ?? '' }}" placeholder = "Ex.: Kg, Cx, Un, etc.">
			@showError($prefixo.'unidade')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'vender_em_multiplos') ? ' has-error' : '' }}">
			<label class="control-label">Vender em Múltiplos de</label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[vender_em_multiplos]" value="{{ old($prefixo.'vender_em_multiplos') ?? $produto->vender_em_multiplos ?? '' }}">
			@showError($prefixo.'vender_em_multiplos')
		</div>
	</div> --}}
</div>