<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-header">
		<h2 class="widget-title lighter">Dados principais</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			@include('produtos.partials.dadosPrincipais')
		</div>
	</div>

	<div class="widget-header">
		<h2 class="widget-title lighter">Preço</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			@define($tabelasPreco=$produto->tabelasPreco ?? collect([]))
			@include('produtos.partials.tabelasDePreco')
		</div>
	</div>

	<div class="widget-header">
		<h2 class="widget-title lighter">Estoque</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			@define($estoqueConfiguracao = $produto->estoque->configuracao ?? null)
			@include('estoques.partials.configuracoes')
			<input type="hidden" name="{{ $prefixo }}[produto][estoque_configuracao_id]" value="{{ old($prefixo.'.produto.estoque_configuracao_id') ?? $produto->estoque->id ?? '' }}">
		</div>
	</div>
	<div class="widget-header">
		<h2 class="widget-title lighter">Informações Adicionais</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			@include('produtos.partials.dadosAdicionais')
		</div>
	</div>
</div>