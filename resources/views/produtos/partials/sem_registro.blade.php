<div class="ibox-content text-center">
	<svg class="sem-registro"><use href="{{ asset('images/general.svg#box-normal') }}"></use></svg>
	<div class = "text-center">
		<h2 class="sem-registro text-primary">Nenhum <span class="">{{ __('modulos.produto') }}</span> cadastrado</h2>
	</div>
	<div class="row text-left">
		<div class="col-md-12">
			<h3 class="text-info">Recursos</h3>
				<p><i class="fa fa-check"></i> Cadastre seus {{ __('modulos.produtos') }} e serviços para facilitar suas vendas</p>
				<p><i class="fa fa-check"></i> Gerencie seu estoque</p>
				<p><i class="fa fa-check"></i> Defina suas tabelas de preço</p>
				<p><i class="fa fa-check"></i> Crie galeria de fotos</p>
				<p><i class="fa fa-check"></i> Categorize seus {{ __('modulos.produtos') }} e serviços</p>
				{{-- <p><i class="fa fa-check"></i> Imprima etiquetas</p> --}}
		</div>
	</div>
</div>
