<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover mt-0 tabelaProdutos">
				<thead>
					<tr>
						<th>Fotos</th>
						<th>Código</th>
						<th>Nome</th>
						<th nowrap width="5%">Comissão</th>
						@foreach($tabelasDePreco as $tabelaPreco)
							<th nowrap width="5%">{{ $tabelaPreco->nome }}</th>
						@endforeach
						<th class="dt-no-export">Ações</th>
					</tr>
				</thead>
				<tbody>
				@foreach($produtos ?? [] as $produto)
					<tr>
						<td nowrap width="5%"><img class="gravatar" src="{{ asset($produto->foto ?? 'images/no-photo.png') }}"> </td>
						<td>{{ $produto->codigo }}</td>
						<td>{{ $produto->nome }}</td>
						<td nowrap width="5%">{{ $produto->comissao ?? '' }}</td>
						@foreach($tabelasDePreco as $tabelaPreco)
							<td nowrap width="5%">R$ {{ $produto->present()->valor($tabelaPreco) }}</td>
						@endforeach
						<td class="text-center acoes" nowrap width="5%">
							<i class="fa fa-search fa-1-4x has-tooltip" data-placement="bottom" title="Visualizar"></i>
							<a href="{{ route('produto.editar', ['representada' => $representada->id, 'id' => $produto->id]) }}"><i class="fa fa-edit fa-1-4x has-tooltip" data-placement="bottom" title="Editar"></i></a>
							<form method="POST" action="{{ route('produto.apagar', ['representada' => $representada->id, 'id' => $produto->id]) }}">
								@method('DELETE')
								<a role="submit"><i class="fa fa-1-4x fa-trash text-danger" data-placement="bottom" title="Apagar"></i></a>
							</form>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@section('scripts')
	@parent
	<script type="text/javascript">

		$(document).ready(function(){
			var tabela = $('.tabelaProdutos').DataTable({
				columnDefs: [
					{'aTargets': [1,2], 'bSortable': false},
					{'aTargets': [2], 'searchable': false}
				]
			});

			$(document).on('keyup', '.filtroTabela', function() {
				tabela.search($(this).val()).draw() ;
			});
		});


	</script>

@endsection
