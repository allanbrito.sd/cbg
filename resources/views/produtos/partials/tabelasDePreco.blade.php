@define($prefixoInput = !empty($prefixo) ? $prefixo.'[tabelasDePreco]'  : 'tabelasDePreco')
@define($prefixo = !empty($prefixo) ? $prefixo.'.tabelasDePreco.'  : 'tabelasDePreco.')

@foreach($tabelasDePreco as $tabelaPreco)
	<div class="row tabelasDePreco">
		<div class="form-group col-md-3{{ $errors->has($prefixo.$tabelaPreco->id.'.valor') ? ' has-error' : '' }}">
			<label class="control-label">{{ $tabelaPreco->nome }}</label>
			<input type="text" class="form-control valor" name="{{ $prefixoInput }}[{{$tabelaPreco->id}}][valor]" value="{{ old($prefixo.$tabelaPreco->id.'.valor') ?? (isset($produto) ? $produto->present()->valor($tabelaPreco) : '') }}">
			@showError($prefixo.$tabelaPreco->id.'.valor')
		</div>
	</div>
@endforeach




		{{--<div class="row">
			<div class="form-group col-md-3{{ $errors->has($prefixo.'marca') ? ' has-error' : '' }}">
				<label class="control-label">Marca</label>
				<input type="text" class="form-control" name="{{ $prefixoInput }}[marca]" value="{{ old($prefixo.'marca') ?? $tabelaPreco->marca ?? '' }}">
				@showError($prefixo.'marca')
			</div>

			<div class="form-group col-md-3{{ $errors->has($prefixo.'unidade') ? ' has-error' : '' }}">
				<label class="control-label">Unidade</label>
				<input type="text" class="form-control" name="{{ $prefixoInput }}[unidade]" value="{{ old($prefixo.'unidade') ?? $tabelaPreco->unidade ?? '' }}" placeholder = "Ex.: Kg, Cx, Un, etc.">
				@showError($prefixo.'unidade')
			</div>

			<div class="form-group col-md-3{{ $errors->has($prefixo.'vender_em_multiplos') ? ' has-error' : '' }}">
				<label class="control-label">Vender em Múltiplos de</label>
				<input type="text" class="form-control" name="{{ $prefixoInput }}[vender_em_multiplos]" value="{{ old($prefixo.'vender_em_multiplos') ?? $tabelaPreco->vender_em_multiplos ?? '' }}">
				@showError($prefixo.'vender_em_multiplos')
			</div>
		</div> --}}