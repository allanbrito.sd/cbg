@extends('layouts.app')

@section('title',  ucfirst(__('modulos.representada')).' - '.$representada->pessoa->nomeESobrenome)

@section('migalha')
@parent
<li><a href="{{ route('representada.consultar') }}">{{ ucfirst(__('modulos.representadas')) }}</a></li>
<li><a href="{{ route('representada.visualizar', ['id' => $representada->id]) }}">{{ $representada->pessoa->nomeESobrenome }}</a></li>
<li class="active"><strong>Perfil</strong></li>
@endsection

@section('acoes')
<a class="btn" href="{{ route('representada.consultar') }}"><i class="fa fa-mail-reply"></i> Voltar</a>
	<form  method="POST" action="{{ route('representada.apagar', ['id' => $representada->id]) }}">
		@method('DELETE')
		<a class="btn btn-danger" role="submit"><i class="fa fa-trash"></i> Apagar</a>
	</form>
<a class="btn btn-success" href="{{ route('representada.editar', ['id' => $representada->id]) }}"><i class="fa fa-edit"></i> Editar</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row m-b-sm m-t-md">
		<div class="col-md-6">

			<div class="profile-image">
				<img src="{{ asset($representada->pessoa->foto)}}" class="img-circle circle-border m-b-md" alt="profile">
			</div>
			<div class="profile-info">
				<div class="">
					<div>
						<h2 class="no-margins">
							{{ $representada->pessoa->nomeESobrenome }}
						</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">

		<div class="col-lg-3">
			<div class="ibox">
				<div class="ibox-content">
					<h3>Dados cadastrais</h3>

					@define($pessoa = $representada->pessoa)
					@include('pessoas/partials/visualizar/dadosPrincipais')
					<div class="mostrar_mais">
						<span class="expandir"><a>Mostrar mais...</a></span>
						<span class="esconder hidden"><a>Esconder...</a></span>
					</div>
				</div>
			</div>
			<div id="vertical-timeline" class="{{-- vertical-container --}} light-timeline no-margins">
				<div class="vertical-timeline-block">
					<div class="vertical-timeline-icon navy-bg">
						<i class="fa fa-briefcase"></i>
					</div>

					<div class="vertical-timeline-content">
						<h2>Agenda</h2>
						<p>Não há compromissos marcados
						</p>
						<a href="#" class="btn btn-sm btn-primary"> Cadastrar</a>
							<span class="vertical-date">
								Today <br>
								<small>Dec 24</small>
							</span>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-9">
			<div class="ibox">
				<div class="ibox-content">
					<h3>Produtos</h3>
					@if($produtos->count() > 0)
						@include('produtos.partials.tabela')
					@else
						@include('produtos.partials.sem_registro')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
