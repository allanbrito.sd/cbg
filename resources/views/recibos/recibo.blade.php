<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style type="text/css">
		html,body{
		    height:297mm;
		    width:210mm;
		    font-size: 10pt;
		    font-family: Arial;
		    height:100%;
		}

		body {
			/*border: 1px solid #000;*/
		}

		.page {
			width: 100%;
		    height:100%;
			/*padding: 2cm 1cm;*/
			/*border: 1px solid #000;*/
		}

		.table {
			/*width: 190mm;*/
			width: 190mm;
			border-spacing: 0;
		}

		.section {
			font-size: 11pt;
			font-weight: bold;
			vertical-align: bottom;
		}
		
		.border-bottom {
			border-bottom: 1px solid #000; 
			line-height: 7pt;
			font-size: 7pt;
		}

		.title {
			font-size: 8pt;
			vertical-align: bottom;
		}

		.content {
			font-weight: bold;
			vertical-align: top;
		}

		.qr-code-td {
			width: 37.70mm;
			padding: 0;
			padding-left: 3mm;
			height: 46.76mm;
		}

		.box {
			display: table-cell;
			vertical-align: middle;
			background: #dadada;
			border: 1px solid #c7c7c7;
			width: 34.70mm;
			height: 46.76mm;
			text-align: center;
		}

		.qr-code-container {
			overflow:hidden;
			width: 90px;
			height: 90px;
			margin: 8px auto;
		}

		.qr-code {
			margin: -26.5px 0 0 -26.5px;
		}

		.footer {
		    clear: both;
		    position: fixed;
		    z-index: 10;
		    width: 100%;
		    bottom: 0;
		}

		/*
		dimen   = 714 x 1010
		proporc = 0,2941
		left    = 50
		right   = 50
		top     = 55
		bottom  = 67
		qr code = 118 x 159
		logo    = 166 x 69
		*/
	</style>
</head>
<body>
	<div class="page">
		<span style="float:right;margin-top: 13.5mm; margin-right: 32mm; font-size: 13pt">CONFEDERAÇÃO BRASILEIRA DE GINÁSTICA</span>
		<img src="<?php echo asset('images/logo.png'); ?>" width="180mm" style="margin-top: 7mm">
		<br/>
		<br/>
		<br/>
		<table class="table" border="0">
			<tr style="height: 4.5mm">
				<td class="title" colspan="4">Razão Social</td>
				<td class="qr-code-td" rowspan="11">
					<div class="box" style="line-height: 13pt">
						<span class="title">Número</span><br/>
						<span class="content" style="font-size: 14pt;"><?php echo str_pad($recibo->numero, 4, STR_PAD_LEFT, '0')."/".$recibo->ano; ?></span>
						<div class="qr-code-container">
						    <img class="qr-code" src="http://chart.googleapis.com/chart?chs=144x144&cht=qr&chl=<?php echo \URL::to('/verificar/'.$recibo->verificacao); ?>"/>
						</div>
						<span class="title">Código de verificação</span>
						<span class="content"><?php echo $recibo->codigo; ?></span>
					</div>
				</td>
			</tr>
			<tr style="height: 4.5mm">
				<td class="content" colspan="4"><?php echo mb_strtoupper($empresa->nome); ?></td>
			</tr>
			<tr style="height: 4.5mm">
				<td class="title" colspan="2">CNPJ</td>
				<td class="title">Inscrição Municipal</td>
				<td class="title">Inscrição Estadual</td>
			</tr>
			<tr style="height: 4.5mm">
				<td class="content" colspan="2"><?php echo $empresa->cnpj; ?></td>
				<td class="content"><?php echo $empresa->inscricao_municipal; ?></td>
				<td class="content"><?php echo $empresa->inscricao_estadual; ?></td>
			</tr>
			<tr style="height: 4.5mm">
				<td class="title" colspan="4">Endereço</td>
			</tr>
			<tr style="height: 4.5mm">
				<td class="content" colspan="4"><?php echo $empresa->endereco->logradouro; ?></td>
			</tr>
			<tr style="height: 4.5mm">
				<td class="title">CEP</td>
				<td class="title">Cidade</td>
				<td class="title">Estado</td>
				<td></td>
			</tr>
			<tr style="height: 4.5mm">
				<td class="content"><?php echo $empresa->endereco->cep; ?></td>
				<td class="content"><?php echo $empresa->endereco->cidade; ?></td>
				<td class="content"><?php echo $empresa->endereco->estado; ?></td>
				<td></td>
			</tr>
			<tr style="height: 4.5mm">
				<td class="title" colspan='2'>Telefone</td>
				<td class="title">Site</td>
				<td class="title" style="text-align: center">DATA</td>
			</tr>
			<tr style="height: 4.5mm">
				<td class="content" colspan='2'><?php echo $empresa->contato->telefones; ?></td>
				<td class="content"><?php echo $empresa->contato->site; ?></td>
				<td class="content" style="text-align: center"><?php echo !empty($recibo->pagamento->data) ? $recibo->pagamento->data->format('d/m/Y') : ''; ?></td>
			</tr>
			<tr>
				<td class="title border-bottom" colspan="4" style="line-height: 3pt; font-size: 3pt;">&nbsp;</td>
			</tr>


			<tr>
				<td class="title" colspan="5" style="line-height: 6pt; font-size: 6pt;">&nbsp;</td>
			</tr>
			<tr>
				<td class="section" colspan="5"><?php echo $recibo->pagamento->lancamento->credito ? 'RECEBEMOS' : 'PAGAMOS'; ?></td>
			</tr>
			<tr>
				<td class="title" colspan="4">Razão Social / Nome</td>
				<td class="title">CNPJ</td>
			</tr>
			<tr>
				<td class="content" colspan="4"><?php echo mb_strtoupper($recibo->pagamento->pessoa->nome); ?></td>
				<td class="content"><?php echo !empty($recibo->pagamento->pessoa->cnpj) ? $recibo->pagamento->pessoa->cnpj : $recibo->pagamento->pessoa->cpf; ?></td>
			</tr>
			<tr>
				<td class="title" colspan="4">Endereço</td>
				<td class="title">Complemento</td>
			</tr>
			<tr>
				<td class="content" colspan="4"><?php echo $recibo->pagamento->pessoa->endereco->logradouro; ?></td>
				<td class="content"><?php echo $recibo->pagamento->pessoa->endereco->complemento; ?></td>
			</tr>
			<tr>
				<td class="title">CEP</td>
				<td class="title">Cidade</td>
				<td class="title">Estado</td>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td class="content"><?php echo $recibo->pagamento->pessoa->endereco->cep; ?></td>
				<td class="content"><?php echo $recibo->pagamento->pessoa->endereco->cidade; ?></td>
				<td class="content"><?php echo $recibo->pagamento->pessoa->endereco->estado->nome ?? ''; ?></td>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td class="title" colspan='2'>Telefone</td>
				<td class="title" colspan='3'>Email</td>
			</tr>
			<tr>
				<td class="content" colspan='2'><?php echo $recibo->pagamento->pessoa->contato->telefones; ?></td>
				<td class="content" colspan="3"><?php echo $recibo->pagamento->pessoa->contato->email; ?></td>
			</tr>
			<tr>
				<td class="title border-bottom" colspan="5" style="line-height: 4pt; font-size: 4pt;">&nbsp;</td>
			</tr>

			<tr>
				<td class="title" colspan="5" style="line-height: 6pt; font-size: 6pt;">&nbsp;</td>
			</tr>

			<tr>
				<td class="section" colspan="5">REFERENTE</td>
			</tr>
			<tr>
				<td class="content" colspan="5" style="padding-left: 10px; line-height: 8.5pt; font-size: 8pt"><br/>
					<?php echo !empty($recibo->pagamento->lancamento->evento) ? 'Ao '.$recibo->pagamento->lancamento->evento->nome." realizando de ".$recibo->pagamento->lancamento->evento->data_inicio->format('d/m/Y').' a '.$recibo->pagamento->lancamento->evento->data_fim->format('d/m/Y').(!empty($recibo->pagamento->lancamento->evento->local) ? ' em '.$recibo->pagamento->lancamento->evento->local : '').'<br/><br/>' : ""; ?>
					<?php foreach ($recibo->pagamento->lancamento->eventoItens ?? [] as $eventoItem) {
						echo "✓ ".$eventoItem->item->nome."<br/>";
					} ?>
					<?php echo $recibo->pagamento->pessoa->id != $recibo->pagamento->lancamento->pessoa->id ? '<br/>'.$recibo->pagamento->lancamento->pessoa->nome.'<br/>' : ''; ?>

					<?php if (!empty($recibo->pagamento->lancamento->anuidade->first())) {
						echo '<br/>A Anuidade '.$recibo->pagamento->lancamento->anuidade->first()->ano.'<br/><br/>';

						echo 'Modalidades:<br/>';
						foreach ($recibo->pagamento->lancamento->anuidade as $anuidade) {
							echo "✓ ".$anuidade->modalidade->nome."<br/>";
						}
					} ?>

					<?php if (!empty($recibo->pagamento->lancamento->figLicenses->first())) {
						echo '<br/>Fig License <br/><br/>';

						echo 'Atletas:<br/><table>';
						foreach ($recibo->pagamento->lancamento->figLicenses as $figLicense) {
							echo "<tr>";
							echo "<td>✓ ".$figLicense->atleta->nome."<td/>";
							echo "<td>✓ ".$figLicense->modalidade->nome."<td/>";
							echo "<td>✓ ".$figLicense->clube->nome."<td/>";
							echo "</tr>";
						}
						echo "</table><br/>";
					} ?>

					<?php if (!empty($recibo->pagamento->lancamento->transferencias->first())) {
						echo 'Transferência de atleta <br/><br/>';

						foreach ($recibo->pagamento->lancamento->transferencias as $transferencia) {
							echo "✓ ".$transferencia->atleta->nome."&nbsp;&nbsp;&nbsp;&nbsp;".$transferencia->modalidade->nome."<br/><br/>";
							echo "Clube de origem:".$transferencia->clubeOrigem->nome."<br/>";
							echo "Clube de destino:".$transferencia->clubeOrigem->nome."<br/><br/>";
						}
						echo "<br/><br/>";
					} ?>

				</td>
			</tr>
		</table>
		<div class='footer'>
			<div class="title border-bottom">&nbsp;</div>
			<div style="height: 12mm; width: 175mm; text-align: right; display: table-cell; vertical-align: middle; font-weight: bold;">
				VALOR TOTAL &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $recibo->valor; ?>
			</div>
			<div class="title border-bottom" style="line-height: 0px">&nbsp;</div>
			<br/>

			<div style="font-size: 7pt; height: 23mm; width: 90mm; border: 1px solid #000; border-radius: 15px; text-align: center; display: table-cell; vertical-align: middle; font-weight: bold;">
				Pagamento efetuado através de depósito no Banco do Brasil<br/>
				Agência 1224-6 conta corrente 200.006-7
			</div>
			<div style="display: table-cell; width: 2mm">&nbsp;</div>
			<div style="font-size: 7pt; height: 23mm; width: 90mm; border: 1px solid #000; border-radius: 15px; text-align: center; display: table-cell; font-weight: bold;">
				OUTRAS INFORMAÇÕES
			</div>

			<div class="title border-bottom" style="line-height: 10pt">&nbsp;</div>
			<span style="font-size: 6.5pt;">Para verificação deste, acesse: <?php echo \URL::to('/verificar/'); ?>
				<?php echo !empty($dataVisualizacao) ? '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizado em: '.date('d/m/Y H:i:s') : '' ?>
			</span>
			<span style="font-size: 7pt; margin-left: {{ !empty($dataVisualizacao) ? 40 : 95 }}mm">SIG <?php echo date('dmy');?> </span>
			<br/>
			<br/>
			<br/>
		</div>
	</div>
	<?php if (empty($recibo->gerado_em)) { ?>
		<img src="<?php echo asset('images/rascunho.jpg'); ?>" style="position: fixed; left: 30mm; top: 70mm; z-index: -1">
	<?php } else if (!empty($recibo->cancelado_em)) { ?>
		<img src="<?php echo asset('images/cancelado.png'); ?>" style="position: fixed; left: 11mm; top: 45mm; z-index: 999">
	<?php } ?>
</body>
</html>