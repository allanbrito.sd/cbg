@extends('layouts.external')

@section('title', 'Verificar recibo')

@section('content')
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<div class="middle-box loginscreen animated fadeInDown">
		<div>
			<h2 class="text-center">Verificação de autenticidade</h2>
			<br/>
			<form class="m-t" role="form" method="POST" action="{{ route('recibo.validar') }}">
				{{ csrf_field() }}

				{{-- <div class="form-group{{ $errors->has('cnpfcnpj') ? ' has-error' : '' }}">
					<label>CNPJ/CPF</label>
					<input type="text" maxlength='11' maxlength='14' class="form-control numero" name="cnpfcnpj" value="{{ old('cnpfcnpj') }}" required autofocus>
					@showError('cnpfcnpj')
				</div> --}}
				<div class="form-group{{ $errors->has('numero') ? ' has-error' : '' }}">
					<label>Número</label>
					<input class="form-control numero_recibo" name="numero" required>
					@showError('numero')
				</div>
				<div class="form-group{{ $errors->has('codigo') ? ' has-error' : '' }}">
					<label>Código de Verificação</label>
					<input minlength="6" maxlength="6" class="form-control" name="codigo" required>
					@showError('codigo')
				</div>

				<button type="submit" class="btn btn-primary block full-width m-b">Buscar</button>
			</form>
		</div>
	</div>
@endsection

{{-- <div class="middle-box loginscreen animated fadeInDown" style="width: 400px">
	<div style="border: 1px solid #000; background-color: #d9d9d9">
		<h3 class="text-center" style="color: #000; font-weight: bold">Verificação de autenticidade</h3>
	</div>
	<div style="border: 1px solid #000; padding: 50px;">
		<form class="m-t" role="form" method="POST" action="{{ route('register') }}">
			{{ csrf_field() }}

			<div class="form-group{{ $errors->has('cnpfcnpj') ? ' has-error' : '' }}">
				<label>CNPJ/CPF</label>
				<input type="text" maxlength='11' maxlength='14' class="form-control numero" name="cnpfcnpj" value="{{ old('cnpfcnpj') }}" required autofocus>
				@showError('cnpfcnpj')
			</div>
			<div class="form-group{{ $errors->has('numero') ? ' has-error' : '' }}">
				<label>Número</label>
				<input class="form-control numero_recibo" name="numero" required>
				@showError('numero')
			</div>
			<div class="form-group{{ $errors->has('codigo') ? ' has-error' : '' }}">
				<label>Código de Verificação</label>
				<input minlength="6" maxlength="6" class="form-control" name="codigo" required>
				@showError('codigo')
			</div>

			<button type="submit" class="btn btn-primary block full-width m-b">Buscar</button>
		</form>
	</div>
</div> --}}

@section('scripts')
@parent
	<script type="text/javascript">
		$(document).ready(function(){
			$('.numero_recibo').mask('0000/00', {clearIfNotMatch: true});
		});
	</script>
@endsection