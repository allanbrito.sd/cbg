@extends('layouts.app')

@include('layouts.vendor.datatable')

@section('title', 'Representadas')

@section('migalha')
@parent
<li class="active"><a href="{{ route('representada.consultar') }}">Representadas</a></li>
<li><strong>Consultar</strong></li>
@endsection

@section('acoes')
<a class="btn btn-success" href="{{ route('representada.novo') }}"><i class="fa fa-plus-square"></i> Novo</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-md-12">
			<div class="ibox">
				<div class="ibox-content">
					@if($representadas->count() > 0)
						@include('representadas.partials.tabela')
					@else
						@include('representadas.partials.sem_registro')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
