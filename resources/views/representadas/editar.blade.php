@extends('layouts.app')

@section('title', ucfirst(__('representada'))." - ".$representada->pessoa->nomeESobrenome)

@section('migalha')
@parent
<li><a href="{{ route('representada.consultar') }}">{{ ucfirst(__('modulos.representadas')) }}</a></li>
<li><a href="{{ route('representada.visualizar', ['id' => $representada->id]) }}">{{ $representada->pessoa->nomeESobrenome }}</a></li>
<li class="active"><strong>Editar</strong></li>
@endsection

@section('acoes')
<a class="btn" href="{{ route('representada.consultar') }}"><i class="fa fa-mail-reply"></i> Voltar</a>
<a class="btn btn-success" role="submit" href="{{ route('representada.atualizar', ['id' => $representada->id]) }}"><i class="fa fa-save"></i> Salvar</a>
@endsection

@section('content')
<form role="form" method="POST" action="{{ route('representada.atualizar', ['id' => $representada->id]) }}">
	@method('PUT')

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content">
						@define($prefixo = 'formulario')
						@include('representadas.partials.formulario')
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

