<svg class="sem-registro"><use href="{{ asset('images/building.svg#Layer_1') }}"></use></svg>
<div class = "text-center">
	<h2 class="sem-registro text-primary">Nenhuma <span class="">{{ __('modulos.representada') }}</span> cadastrada</h2>
</div>
<div class="row text-left">
	<div class="col-md-12">
		<h3 class="text-info">Recursos</h3>
			<p><i class="fa fa-check"></i> Tenha os dados de suas {{ __('modulos.representadas') }} sempre à mão</p>
			<p><i class="fa fa-check"></i> Facilite o seu contato e agilize o processo de compra e venda dos seus produtos</p>
			{{-- <p><i class="fa fa-check"></i> Registro de ocorrências</p> --}}
	</div>
</div>
