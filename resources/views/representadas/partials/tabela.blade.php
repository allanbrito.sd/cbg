@include('representadas.partials.filtro')
<br/>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover mt-0 tabelaRepresentadas">
				<thead>
					<tr>
						<th>Nome</th>
						<th>CPF</th>
						<th>Telefones</th>
						<th>Situação</th>
						<th class="dt-no-export">Ações</th>
					</tr>
				</thead>
				<tbody>
				@foreach($representadas as $representada)
					<tr class="pointer table_link" data-id="{{ $representada->id }}" data-href="{{ route('representada.visualizar', ['id' => $representada->id]) }}">
						<td><img class="gravatar" src="{{ asset($representada->pessoa->foto) }}"> {{ $representada->pessoa->nomeESobrenome }}</td>
						<td> {{ $representada->pessoa->cpf }}</td>
						<td><i class="fa fa-phone"> </i> {{ $representada->pessoa->telefones ?? 'Nenhum informado'}}</td>
						<td class="client-status"><span class="label label-primary">Ativo</span></td>
						<td class="text-center acoes" nowrap width="5%">
							<i class="fa fa-search fa-1-4x has-tooltip" data-placement="bottom" title="Visualizar"></i>
							<a href="{{ route('representada.editar', ['id' => $representada->id]) }}"><i class="fa fa-edit fa-1-4x has-tooltip" data-placement="bottom" title="Editar"></i></a>
							<form method="POST" action="{{ route('representada.apagar', ['id' => $representada->id]) }}">
								@method('DELETE')
								<a role="submit"><i class="fa fa-1-4x fa-trash text-danger" data-placement="bottom" title="Apagar"></i></a>
							</form>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@section('scripts')
	@parent
	<script type="text/javascript">

		$(document).ready(function(){
			var tabela = $('.tabelaRepresentadas').DataTable({
				columnDefs: [
					{'aTargets': [1,2], 'bSortable': false},
					{'aTargets': [2], 'searchable': false}
				]
			});

			$(document).on('keyup', '.filtroTabela', function() {
				tabela.search($(this).val()).draw() ;
			});
		});


	</script>

@endsection
