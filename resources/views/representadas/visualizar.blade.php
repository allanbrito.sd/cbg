@extends('layouts.app')

@section('title',  ucfirst(__('modulos.representada')).' - '.$representada->pessoa->nomeESobrenome)

@section('migalha')
@parent
<li><a href="{{ route('representada.consultar') }}">{{ ucfirst(__('modulos.representadas')) }}</a></li>
<li><a href="{{ route('representada.visualizar', ['id' => $representada->id]) }}">{{ $representada->pessoa->nomeESobrenome }}</a></li>
<li class="active"><strong>Perfil</strong></li>
@endsection

@section('acoes')
<a class="btn" href="{{ route('representada.consultar') }}"><i class="fa fa-mail-reply"></i> Voltar</a>
	<form  method="POST" action="{{ route('representada.apagar', ['id' => $representada->id]) }}">
		@method('DELETE')
		<a class="btn btn-danger" role="submit"><i class="fa fa-trash"></i> Apagar</a>
	</form>
<a class="btn btn-success" href="{{ route('representada.editar', ['id' => $representada->id]) }}"><i class="fa fa-edit"></i> Editar</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="ibox">
		<div class="ibox-content">
			<div class="row">
				<div class="col-md-6">

					<div class="profile-image">
						<img src="{{ asset($representada->pessoa->foto)}}" class="img-circle circle-border m-b-md" alt="profile">
					</div>
					<div class="profile-info">
						<div class="">
							<div>
								<h2 class="no-margins">
									{{ $representada->pessoa->nomeESobrenome }}
								</h2>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<table class="table small m-b-xs">
						<tbody>
						<tr>
							<td>
								<strong>142</strong> Vendas concretizadas
							</td>

						</tr>
						<tr>
							<td>
								<strong>13</strong> Orçamentos em aberto
							</td>

						</tr>
						<tr>
							<td>
								<strong>22</strong> Clientes ativos
							</td>
						</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-3">
					<small>Vendas da última semana</small>
					<h2 class="no-margins">R$ 206.480,00</h2>
					<div id="sparkline1"></div>
				</div>
				<div class="col-md-3">
					<small>Comissão do mês passado</small>
					<h2 class="no-margins">R$ 2.480,00</h2>
					<div id="sparkline1"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-9">
			<div class="ibox">
				<div class="ibox-content">
					<div class="text-right acoes">
						<h3 class="pull-left">Produtos</h3>
						<a class="btn btn-success" href="{{ route('produto.novo', ['representada' => $representada->id]) }}"><i class="fa fa-plus-square"></i> Novo</a>
					</div>

					@if($representada->produtos->count() > 0)
						@define($produtos = $representada->produtos)
						@include('produtos.partials.filtro')
						<br/>

						@include('produtos.partials.tabela')
					@else
						@include('produtos.partials.sem_registro')
					@endif
				</div>
			</div>
		</div>

		<div class="col-lg-3">
			<div class="ibox hidden">
				<div class="ibox-content">
					<h3>Representada</h3>
					{{-- <div class="gravatar">
						<img src="{{ asset($representada->pessoa->foto)}}" class="img-circle circle-border m-b-md" alt="profile">
					</div> --}}
					@define($pessoa = $representada->pessoa)
					@include('pessoas/partials/visualizar/dadosPrincipais')
					<div class="mostrar_mais">
						<span class="expandir"><a>Mostrar mais...</a></span>
						<span class="esconder hidden"><a>Esconder...</a></span>
					</div>
				</div>
			</div>

			<div id="vertical-timeline" class="{{-- vertical-container --}} light-timeline no-margins">
				<div class="vertical-timeline-block">
					<div class="vertical-timeline-icon navy-bg">
						<i class="fa fa-briefcase"></i>
					</div>

					<div class="vertical-timeline-content">
						<h2>Agenda</h2>
						<p>Não há compromissos marcados
						</p>
						<a href="#" class="btn btn-sm btn-primary"> Cadastrar</a>
					</div>
				</div>
			</div>
			<br/>
		</div>
	</div>
</div>
@endsection
