@extends('layouts.app')

@include('layouts.vendor.datatable')

@section('title', 'Transportadoras')

@section('migalha')
@parent
<li class="active"><a href="{{ route('transportadora.consultar') }}">Transportadoras</a></li>
<li><strong>Consultar</strong></li>
@endsection

@section('acoes')
<a class="btn btn-success" href="{{ route('transportadora.novo') }}"><i class="fa fa-plus-square"></i> Novo</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-md-12">
			<div class="ibox">
				@if($transportadoras->count() > 0)
					@include('transportadoras.partials.tabela')
				@else
					@include('transportadoras.partials.sem_registro')
				@endif
			</div>
		</div>
	</div>
</div>
@endsection
