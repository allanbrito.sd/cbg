@extends('layouts.app')

@section('title', 'Editar '.__('transportadora'))

@section('migalha')
@parent
<li><a href="{{ route('transportadora.consultar') }}">{{ ucfirst(__('modulos.transportadoras')) }}</a></li>
<li><a href="{{ route('transportadora.visualizar', ['id' => $transportadora->id]) }}">{{ $transportadora->pessoa->nomeESobrenome }}</a></li>
<li class="active"><strong>Editar</strong></li>
@endsection

@section('acoes')
<a class="btn" href="{{ route('transportadora.consultar') }}"><i class="fa fa-mail-reply"></i> Voltar</a>
<a class="btn btn-success" role="submit" href="{{ route('transportadora.atualizar', ['id' => $transportadora->id]) }}"><i class="fa fa-save"></i> Salvar</a>
@endsection

@section('content')
<form role="form" method="POST" action="{{ route('transportadora.atualizar', ['id' => $transportadora->id]) }}">
	@method('PUT')

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content">
						@define($prefixo = 'formulario')
						@include('transportadoras.partials.formulario')
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

