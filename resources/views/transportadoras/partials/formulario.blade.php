<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-header">
		<h2 class="widget-title lighter">Dados pessoais</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			@define($pessoa = $transportadora->pessoa ?? null)
			@include('pessoas.partials.fisicaOuJuridica')
		</div>
	</div>
</div>

<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-header">
		<h2 class="widget-title lighter">Endereço</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			@define($endereco = $transportadora->pessoa->endereco ?? null)
			@include('pessoas.partials.endereco')
		</div>
	</div>
</div>

<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
	<div class="widget-header">
		<h2 class="widget-title lighter">Contato</h2>
	</div>
	<div class="widget-body">
		<div class="widget-main padding-6 no-padding-left no-padding-right">
			@include('pessoas.partials.contato')
		</div>
	</div>
</div>
