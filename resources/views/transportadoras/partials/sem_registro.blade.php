<div class="ibox float-e-margins">
	<div class="ibox-content text-center">
		<svg class="sem-registro"><use href="{{ asset('images/general.svg#car-box') }}"></use></svg>
		<h2 class="sem-registro text-primary">Nenhuma <span class="">{{ __('modulos.transportadora') }}</span> cadastrada</h3>
		<div class="row text-left">
			<div class="col-md-12">
				<h3 class="text-info">Recursos</h3>
					<p><i class="fa fa-check"></i> Tenha os dados de suas {{ __('modulos.transportadoras') }} sempre à mão</p>
					<p><i class="fa fa-check"></i> Facilite o seu contato e agilize o processo de compra e venda dos seus produtos</p>
					{{-- <p><i class="fa fa-check"></i> Registro de ocorrências</p> --}}
			</div>
		</div>
	</div>
</div>