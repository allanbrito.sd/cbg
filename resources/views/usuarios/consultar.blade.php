@extends('layouts.app')

@include('layouts.vendor.datatable')

@section('title', ucfirst(__('modulos.usuários')))

@section('migalha')
@parent
<li><a href="{{ route('usuarios') }}">{{ ucfirst(__('modulos.usuários')) }}</a></li>
<li class="active"><strong>Consultar</strong></li>
@endsection

@section('acoes')
<a class="btn btn-success" href="{{ route('usuario.novo') }}"><i class="fa fa-plus-square"></i> Novo</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		@foreach ($usuarios as $usuario)
			<div class="col-lg-3 visible-xs hidden-sm hidden-md visible-lg">
				<div class="contact-box center-version">
					<a href="{{ route('usuario.visualizar', ['id' => $usuario->id]) }}">

						<img alt="image" class="img-circle" src="{{ asset($usuario->pessoa->foto) }}">


						<h3 class="m-b-xs"><strong>{{ $usuario->pessoa->nomeESobrenome }}</strong></h3>

						<div class="font-bold"><i class="fa fa-circle"></i> Online a {{ $usuario->created_at->diffForHumans() }}</div>

						<address class="m-t-md text-left">
							<i class="fa fa-map-marker"></i> <strong>{{ $usuario->pessoa->contato->endereco->cidadeEstado ?? 'Aracaju/SE' }}</strong><br>
							<i class="fa fa-phone"></i> {{ $usuario->pessoa->contato->endereco->telefone->numero ?? '(79) 99956-1339' }}<br/>
							<i class="fa fa-envelope"></i> <span class="usuario_email" style="word-break: break-all">{{ $usuario->email}}</span><br/>
						</address>
					</a>
				</div>
			</div>

			<div class="col-lg-3 hidden-xs visible-sm visible-md">
				<div class="contact-box">
					<a href="{{ url('usuario/'.$usuario->id) }}">
						<div class="col-sm-4">
							<div class="text-center">
								<img alt="image" class="img-circle m-t-xs img-responsive" src="{{ asset($usuario->pessoa->foto) }}" style="max-height: 100px">
								<div class="m-t-xs font-bold text-left has-tooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{ $usuario->created_at->diffForHumans() }}"><i class="fa fa-circle"></i> Online</div>
							</div>
						</div>
						<div class="col-sm-8">
							<h3><strong>{{ $usuario->pessoa->nomeESobrenome }}</strong></h3>
							<p><i class="fa fa-map-marker"></i> {{ $usuario->pessoa->contato->endereco->cidade ?? 'Aracaju' }}/{{ $usuario->pessoa->contato->endereco->uf ?? 'SE' }}</p>
							<p><i class="fa fa-phone"></i> {{ $usuario->pessoa->contato->endereco->telefone->numero ?? '(79) 99956-1339' }}</p>
							<p><i class="fa fa-envelope"></i> <span class="usuario_email" style="word-break: break-all">{{ $usuario->email}}</span></p>
						</div>
						<div class="clearfix"></div>
					</a>
				</div>
			</div>
		@endforeach
	</div>
@endsection
