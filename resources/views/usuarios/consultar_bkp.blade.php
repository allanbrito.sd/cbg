@extends('layouts.app')

@include('layouts.vendor.datatable')

@section('title', 'usuarios')

@section('migalha')
<li>
	<a href="{{ route('home') }}">Início</a>
</li>
<li>
	<a href="{{ route('usuarios') }}">usuarios</a>
</li>
<li class="active">
	<strong>Consultar</strong>
</li>
@endsection

@section('acoes')
<a class="btn btn-success" href="{{ route('cliente.novo') }}"><i class="fa fa-plus-square"></i> Novo</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover dataTables-example" >
							<thead>
								<tr>
									<th>Rendering engine</th>
									<th>Browser</th>
									<th>Platform(s)</th>
									<th>Engine version</th>
									<th>CSS grade</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($usuarios as $usuario)
									<tr>
										<td>{{ $usuario->pessoa->nome }}</td>
										<td>{{ $usuario->email }}</td>
										<td>PSP</td>
										<td class="center">-</td>
										<td class="center">C</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- @foreach ($usuarios as $usuario)
		{{ $usuario->pessoa->nome }}
		<br/>
	@endforeach
{{ $usuarios->links() }} -->
@endsection

@section('scripts')
	@parent
	<script type="text/javascript">

		$(document).ready(function(){
			$('.dataTables-example').DataTable({
				"language": {
						"sEmptyTable": "Nenhum registro encontrado",
						"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
						"sInfoFiltered": "(Filtrados de _MAX_ registros)",
						"sInfoPostFix": "",
						"sInfoThousands": ".",
						"sLengthMenu": "_MENU_ resultados por página",
						"sLoadingRecords": "Carregando...",
						"sProcessing": "Processando...",
						"sZeroRecords": "Nenhum registro encontrado",
						"sSearch": "Pesquisar",
						"oPaginate": {
						"sNext": "Próximo",
						"sPrevious": "Anterior",
						"sFirst": "Primeiro",
						"sLast": "Último"
					},
					"oAria": {
						"sSortAscending": ": Ordenar colunas de forma ascendente",
						"sSortDescending": ": Ordenar colunas de forma descendente"
					}
				},
				pageLength: 10,
				responsive: true,
				dom: '<"html5buttons"B>lTfgitp',
				buttons: [
					{extend: 'copy', text: 'Copiar'},
					{extend: 'csv'},
					{extend: 'excel', title: 'ExampleFile'},
					{extend: 'pdf', title: 'ExampleFile'},

					{extend: 'print', text: 'Imprimir', customize: function (win){
							$(win.document.body).addClass('white-bg');
							$(win.document.body).css('font-size', '10px');

							$(win.document.body).find('table')
							.addClass('compact')
							.css('font-size', 'inherit');
						}
					}
				]

			});

		});
	</script>

@endsection
