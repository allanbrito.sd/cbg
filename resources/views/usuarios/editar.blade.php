@extends('layouts.app')

@section('title', 'Vendedor')

@section('migalha')
@parent
<li><a href="{{ route('usuarios') }}">Vendedor</a></li>
<li>{{ $usuario->pessoa->nomeESobrenome }}</li>
<li class="active"><strong>Editar</strong></li>
@endsection

@section('acoes')
<a class="btn" href="{{ route('usuario.visualizar', ['id' => $usuario->id]) }}"><i class="fa fa-mail-reply"></i> Voltar</a>
<a class="btn btn-success" role="submit" href="{{ route('usuario.atualizar', ['id' => $usuario->id]) }}"><i class="fa fa-save"></i> Atualizar</a>
@endsection

@section('content')
<form role="form" method="POST" action="{{ route('usuario.atualizar', ['id' => $usuario->id]) }}">
	@method('PUT')

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content">
						@define($prefixo = 'vendedor')
						<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
							<div class="widget-header">
								<h2 class="widget-title lighter">Dados pessoais</h2>
							</div>
							<div class="widget-body">
								<div class="widget-main padding-6 no-padding-left no-padding-right">
									@define($pessoa = $usuario->pessoa)
									@include('pessoas.partials.fisicaOuJuridica')
								</div>
							</div>
						</div>
						<div class="widget-box transparent ui-sortable-handle" id="widget-box-12">
							<div class="widget-header">
								<h2 class="widget-title lighter">Endereço</h2>
							</div>
							<div class="widget-body">
								<div class="widget-main padding-6 no-padding-left no-padding-right">
									@define($endereco = $usuario->pessoa->endereco)
									@include('pessoas.partials.endereco')
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

