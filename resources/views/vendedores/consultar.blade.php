@extends('layouts.app')

@include('layouts.vendor.datatable')

@section('title', ucfirst(__('modulos.vendedores')))

@section('migalha')
@parent
<li><a href="{{ route('vendedor.consultar') }}">{{ ucfirst(__('modulos.vendedores')) }}</a></li>
<li class="active"><strong>Consultar</strong></li>
@endsection

@section('acoes')
<a class="btn btn-success" href="{{ route('vendedor.novo') }}"><i class="fa fa-plus-square"></i> Novo</a>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		@forelse($vendedores as $vendedor)
			@include('vendedores.partials.tabela')
		@empty
			@include('vendedores.partials.sem_registro')
		@endforelse
	</div>
@endsection
