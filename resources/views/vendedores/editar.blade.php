@extends('layouts.app')

@section('title', 'Editar '.__('vendedor'))

@section('migalha')
@parent
<li><a href="{{ route('vendedor.consultar') }}">{{ ucfirst(__('modulos.vendedores')) }}</a></li>
<li><a href="{{ route('vendedor.visualizar', ['id' => $vendedor->id]) }}">{{ $vendedor->pessoa->nomeESobrenome }}</a></li>
<li class="active"><strong>Editar</strong></li>
@endsection

@section('acoes')
<a class="btn" href="{{ route('vendedor.consultar') }}"><i class="fa fa-mail-reply"></i> Voltar</a>
<a class="btn btn-success" role="submit" href="{{ route('vendedor.atualizar', ['id' => $vendedor->id]) }}"><i class="fa fa-save"></i> Salvar</a>
@endsection

@section('content')
<form role="form" method="POST" action="{{ route('vendedor.atualizar', ['id' => $vendedor->id]) }}">
	@method('PUT')

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content">
						@define($prefixo = 'formulario')
						@include('vendedores.partials.formulario')
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

