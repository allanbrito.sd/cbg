@define($prefixoInput = !empty($prefixo) ? $prefixo  : '')
@define($prefixo = !empty($prefixo) ? $prefixo.'.'  : '')

<div class="contato_container">
	<div class="row">
		<div class="form-group col-md-3{{ $errors->has($prefixo.'comissao') ? ' has-error' : '' }}">
			<label class="control-label">Comissão</label>
			<input type="text" class="form-control" name="{{ $prefixoInput }}[comissao]" value="{{ old($prefixo.'comissao') ?? $dadosBancarios->comissao ?? '' }}">
			@showError($prefixo.'comissao')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'comissionamento') ? ' has-error' : '' }}">
			<label class="control-label">Comissionamento</label>
			<input i type="text" class="form-control" name="{{ $prefixoInput }}[comissionamento]" value="{{ old($prefixo.'comissionamento') ?? $dadosBancarios->comissionamento ?? '' }}">
			@showError($prefixo.'comissionamento')
		</div>

		{{-- <div class="form-group col-md-2{{ $errors->has($prefixo.'usuario_id') ? ' has-error' : '' }}">
			<label class="control-label">Vincular ao usuário</label>
			<input i type="text" class="form-control" name="{{ $prefixoInput }}[usuario_id]" value="{{ old($prefixo.'usuario_id') ?? $dadosBancarios->usuario_id ?? '' }}">
			@showError($prefixo.'usuario_id')
		</div>

		<div class="form-group col-md-3{{ $errors->has($prefixo.'ativo') ? ' has-error' : '' }}">
			<label class="control-label">Situação</label>
			<input i type="text" class="form-control" name="{{ $prefixoInput }}[ativo]" value="{{ old($prefixo.'ativo') ?? $dadosBancarios->ativo ?? '' }}">
			@showError($prefixo.'ativo')
		</div> --}}
	</div>
	<div class="row">
		<div class="form-group col-md-12{{ $errors->has($prefixo.'observacoes') ? ' has-error' : '' }}">
			<label class="control-label">Observações</label>
			<textarea class="form-control" name="{{ $prefixoInput }}[observacoes]">{{ old($prefixo.'observacoes') ?? $dadosBancarios->observacoes ?? '' }}</textarea>
			@showError($prefixo.'observacoes')
		</div>
	</div>
</div>