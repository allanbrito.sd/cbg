<div class="col-lg-3 visible-xs hidden-sm hidden-md visible-lg">
	<div class="contact-box center-version">
		<div class="pull-right acoes">
			<a href="{{ route('vendedor.editar', ['id' => $vendedor->id]) }}"><i class="fa fa-edit fa-1-4x has-tooltip" data-placement="bottom" title="Editar"></i></a>
			<form method="POST" action="{{ route('vendedor.apagar', ['id' => $vendedor->id]) }}">
				@method('DELETE')
				<a role="submit"><i class="fa fa-1-4x fa-trash text-danger" data-placement="bottom" title="Apagar"></i></a>
			</form>
		</div>

		<a href="{{ route('vendedor.visualizar', ['id' => $vendedor->id]) }}">
			<img alt="image" class="img-circle center-block" src="{{ asset($vendedor->pessoa->foto) }}">
			<h3 class="m-b-xs"><strong>{{ $vendedor->pessoa->nomeESobrenome }}</strong></h3>
			<div class="font-bold"><i class="fa fa-circle"></i> Online a {{ $vendedor->created_at->diffForHumans() }}</div>

			<address class="m-t-md text-left">
				<i class="fa fa-map-marker"></i> <strong>{{ $vendedor->pessoa->contato->endereco->cidadeEstado ?? 'Aracaju/SE' }}</strong><br>
				<i class="fa fa-phone"></i> {{ $vendedor->pessoa->contato->endereco->telefone->numero ?? 'Nenhum informado' }}<br/>
				<i class="fa fa-envelope"></i> <span class="vendedor_email" style="word-break: break-all">{{ $vendedor->email ?? 'Nenhum informado'}}</span><br/>
			</address>
		</a>
	</div>
</div>

<div class="col-lg-3 hidden-xs visible-sm visible-md">
	<div class="contact-box">
		<a href="{{ url('vendedor/'.$vendedor->id) }}">
			<div class="col-sm-4">
				<div class="text-center">
					<img alt="image" class="img-circle m-t-xs img-responsive" src="{{ asset($vendedor->pessoa->foto) }}" style="max-height: 100px">
					<div class="m-t-xs font-bold text-left has-tooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{ $vendedor->created_at->diffForHumans() }}"><i class="fa fa-circle"></i> Online</div>
				</div>
			</div>
			<div class="col-sm-8">
				<h3><strong>{{ $vendedor->pessoa->nomeESobrenome }}</strong></h3>
				<p><i class="fa fa-map-marker"></i> {{ $vendedor->pessoa->contato->endereco->cidade ?? 'Aracaju' }}/{{ $vendedor->pessoa->contato->endereco->uf ?? 'SE' }}</p>
				<p><i class="fa fa-phone"></i> {{ $vendedor->pessoa->contato->endereco->telefone->numero ?? '(79) 99956-1339' }}</p>
				<p><i class="fa fa-envelope"></i> <span class="vendedor_email" style="word-break: break-all">{{ $vendedor->email}}</span></p>
			</div>
			<div class="clearfix"></div>
		</a>
	</div>
</div>