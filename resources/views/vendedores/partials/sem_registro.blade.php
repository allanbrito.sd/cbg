<div class="col-lg-12">
	<div class="ibox float-e-margins">
		<div class="ibox-content text-center">
			<svg class="sem-registro"><use href="{{ asset('images/general.svg#people') }}"></use></svg>
			<h2 class="sem-registro text-primary">Nenhum <span class="">{{ __('modulos.vendedor') }}</span> cadastrado</h3>
			<div class="row text-left">
				<div class="col-md-12">
					<h3 class="text-info">Recursos</h3>
						<p><i class="fa fa-check"></i> Cadastre os {{ __('modulos.vendedores') }} ou {{ __('modulos.representantes') }} da sua empresa</p>
						<p><i class="fa fa-check"></i> Gerencie os dados pessoais, de contato e bancários</p>
						<p><i class="fa fa-check"></i> Controle o método e valor das comissões de cada {{ __('modulos.vendedor') }}</p>
				</div>
			</div>
		</div>
	</div>
</div>