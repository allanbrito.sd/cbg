<div class="ibox-content">
	@include('vendedores.partials.filtro')
	<br/>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover mt-0 tabelaVendedores">
					<thead>
						<tr>
							<th>Nome</th>
							<th>CPF</th>
							<th>Telefones</th>
							<th>Situação</th>
						</tr>
					</thead>
					<tbody>
					@foreach($vendedores as $vendedor)
						<tr class="pointer table_link" data-id="{{ $vendedor->id }}" data-href="{{ route('vendedor.visualizar', ['id' => $vendedor->id]) }}">
							<td><img class="gravatar" src="{{ asset($vendedor->pessoa->foto) }}"> {{ $vendedor->pessoa->nomeESobrenome }}</td>
							<td> {{ $vendedor->pessoa->cpf }}</td>
							<td><i class="fa fa-phone"> </i> {{ $vendedor->pessoa->telefones ?? 'Nenhum informado'}}</td>
							<td class="client-status"><span class="label label-primary">Ativo</span></td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@section('scripts')
	@parent
	<script type="text/javascript">

		$(document).ready(function(){
			var tabela = $('.tabelaVendedores').DataTable({
				columnDefs: [
					{'aTargets': [1,2], 'bSortable': false},
					{'aTargets': [2], 'searchable': false}
				]
			});

			$(document).on('keyup', '.filtroTabela', function() {
				tabela.search($(this).val()).draw() ;
			});
		});


	</script>

@endsection
