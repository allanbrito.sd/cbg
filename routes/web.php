<?php

use App\Models;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

function MakeCrudRoutes($modulo, $config = [])
{
	$prefixo = $config['prefixo'] ?? '';
	$prefixo .= $config['alias'] ?? $modulo;
	$controller = ucfirst($modulo) . "Controller";

	Route::get("$prefixo/", "$controller@" . ($config['consultar'] ?? "consultar"))->name("$modulo.consultar");
	Route::get("$prefixo/novo", "$controller@" . ($config['novo'] ?? "novo"))->name("$modulo.novo");
	Route::get("$prefixo/{" . $modulo . "}",
		"$controller@" . ($config['visualizar'] ?? "visualizar"))->name("$modulo.visualizar");
	Route::get("$prefixo/{" . $modulo . "}/editar",
		"$controller@" . ($config['editar'] ?? "editar"))->name("$modulo.editar");
	Route::put("$prefixo/{" . $modulo . "}",
		"$controller@" . ($config['atualizar'] ?? "atualizar"))->name("$modulo.atualizar");
	Route::post("$prefixo/", "$controller@" . ($config['cadastrar'] ?? "cadastrar"))->name("$modulo.cadastrar");
	Route::delete("$prefixo/{" . $modulo . "}",
		"$controller@" . ($config['apagar'] ?? "apagar"))->name("$modulo.apagar");
}

Route::group(['middleware' => ['auth']], function () {
	// MakeCrudRoutes(['modulo' => 'vendedor']);
	MakeCrudRoutes('usuario', ['visualizar' => 'editar']);
	MakeCrudRoutes('atleta', ['visualizar' => 'editar']);
	MakeCrudRoutes('pessoa', ['visualizar' => 'editar']);
	MakeCrudRoutes('clube', ['visualizar' => 'editar']);
	MakeCrudRoutes('usuario', ['visualizar' => 'editar']);
	MakeCrudRoutes('empresa', ['visualizar' => 'editar']);
	MakeCrudRoutes('lancamento', ['visualizar' => 'editar']);
	MakeCrudRoutes('conta', ['visualizar' => 'editar']);
	MakeCrudRoutes('planoConta', ['visualizar' => 'editar']);
	MakeCrudRoutes('formaPagamento', ['visualizar' => 'editar']);
	MakeCrudRoutes('evento', ['visualizar' => 'editar']);
	MakeCrudRoutes('modalidade', ['visualizar' => 'editar']);
});

Route::get('pessoa/{pessoa}/carregar/{prefixo}', 'PessoaController@carregarFormulario');
Route::get('recibo/visualizar/{recibo}', 'ReciboController@visualizar')->name('recibo.visualizar');
Route::put('recibo/gerar/{recibo}', 'ReciboController@gerar')->name('recibo.gerar');
Route::put('recibo/cancelar/{recibo}', 'ReciboController@cancelar')->name('recibo.cancelar');
Route::get('verificar', 'VerificarReciboController@verificar')->name('recibo.verificar');
Route::post('verificar', 'VerificarReciboController@validar')->name('recibo.validar');

Route::get('figlicense', 'LancamentoController@figLicensePartial');
Route::get('transferencia', 'LancamentoController@transferencia');

Route::get('teste', function() {
	Excel::create('Filename', function($excel) {

	    $excel->sheet('Diário', function($sheet) {
	    	$lancamentos = Models\Lancamento::all();
        	$row = 4;
	    	$sheet->row($row++,
	            array('Data Mov.', 'Nº Doc', 'HISTÓRICO', 'VALOR', 'D/C', 'SALDO', 'REFERENTE', 'Código de Evento', 'Nº Nota')
	        );
	        foreach ($lancamentos ?? [] as $lancamento) {
		    	$sheet->row($row++, array(
			    		!empty($lancamento->pagamenta->data) ? $lancamento->pagamento->data->format('m/Y') : '',
			    		$lancamento->codigo,
			    		$lancamento->historico,
			    		$lancamento->valor,
			    		$lancamento->credito ? 'C' : 'D',
			    		$lancamento->saldo ?? 0,
			    		'',
			    		$lancamento->pagamento->planoContas->codigo ?? null,
			    		$lancamento->notaFiscal->codigo
			    	)
		    	);
	        }
	    });
	})->export('xls');
});

include 'web/misc.php';
include 'web/auth.php';
include 'web/dashboard.php';

