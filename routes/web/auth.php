<?php

// Authentication Routes...
$this->get('acessar-sistema', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('acessar-sistema', 'Auth\LoginController@login');

$this->get('sair', 'Auth\LoginController@logout')->name('logout');
$this->post('sair', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
$this->get('experimente-gratis', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('experimente-gratis', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('senha/resetar', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('senha/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

$this->get('senha/resetar/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('senha/resetar', 'Auth\ResetPasswordController@reset');
