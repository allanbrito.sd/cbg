<?php

Route::group(['prefix' => 'cliente'], function () {
	Route::get('/', 'ClienteController@consultar')->name('clientes');
	Route::get('/novo', 'ClienteController@novo')->name('cliente.novo');
	Route::post('/', 'ClienteController@cadastrar');
	Route::get('/{id}', 'ClienteController@visualizar')->name('cliente.visualizar');
	Route::get('/{id}/editar', 'ClienteController@editar')->name('cliente.editar');
	Route::put('/{id}', 'ClienteController@atualizar');
	Route::delete('/{id}', 'ClienteController@apagar');
});
