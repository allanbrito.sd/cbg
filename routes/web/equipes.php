<?php

Route::group(['prefix' => 'equipe'], function () {
	Route::get('/escolher', 'EquipeController@escolher')->name('equipes.escolher');
	Route::get('/cadastrar', 'EquipeWizardController@novo')->name('equipes.novo');
	Route::post('/cadastrar', 'EquipeWizardController@cadastrar')->name('equipes.cadastrar');
});
