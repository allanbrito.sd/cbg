<?php

// Documentação do template
Route::get('template/doc', function () {
	return Redirect::to('/template/Static_Full_Version/index.html');
});

// Setar estado da navbar no cookie
Route::get('/navbar/expand', 'NavbarController@expand');
Route::get('/navbar/collapse', 'NavbarController@collapse');

Route::get('/pagina-nao-encontrada', function () {
	abort('404');
})->name('404');
