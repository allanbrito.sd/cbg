<?php

Route::group(['prefix' => 'usuario', 'middleware' => 'auth'], function () {
	Route::get('/', 'UsuarioController@consultar')->name('usuarios');
	Route::get('/novo', 'UsuarioController@novo')->name('usuario.novo');
	Route::post('/', 'UsuarioController@cadastrar')->name('usuario.cadastrar');
	Route::get('/{id}', 'UsuarioController@visualizar')->name('usuario.visualizar');
	Route::get('/{id}/editar', 'UsuarioController@editar')->name('usuario.editar');
	Route::put('/{id}', 'UsuarioController@atualizar')->name('usuario.atualizar');
	Route::delete('/{id}', 'UsuarioController@apagar')->name('usuario.apagar');
});
