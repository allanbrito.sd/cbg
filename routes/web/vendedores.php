<?php

Route::group(['prefix' => 'vendedor'], function () {
	Route::get('/', 'VendedorController@consultar')->name('vendedores');
	Route::get('/novo', 'VendedorController@novo')->name('vendedor.novo');
	Route::post('/', 'VendedorController@cadastrar')->name('vendedor.cadastrar');
	Route::get('/{vendedor}', 'VendedorController@visualizar')->name('vendedor.visualizar');
	Route::get('/{vendedor}/editar', 'VendedorController@editar')->name('vendedor.editar');
	Route::put('/{vendedor}', 'VendedorController@atualizar')->name('vendedor.atualizar');
	Route::delete('/{vendedor}', 'VendedorController@apagar')->name('vendedor.apagar');
});
