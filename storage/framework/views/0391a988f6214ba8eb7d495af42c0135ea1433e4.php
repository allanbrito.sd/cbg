<!-- TODO: Remover -->


<?php $__env->startSection('beforecontent'); ?>
	<!-- Wrapper-->
	<div id="wrapper">

		<!-- Navigation -->
		<?php echo $__env->make('layouts.themes.app.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		<!-- Page wraper -->
		<div id="page-wrapper" class="gray-bg">

			<!-- Page wrapper -->
			<?php echo $__env->make('layouts.themes.app.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-sm-6 hidden-xs">
					<h2><?php echo $__env->yieldContent('title'); ?></h2>
					<ol class="breadcrumb">
						
						<?php echo $__env->yieldContent('migalha'); ?>
					</ol>
				</div>
				<div class="col-sm-6 col-xs-12 text-right acoes">
					<h2 class="pull-left mb-0 hidden-sm visible-xs"><?php echo $__env->yieldContent('title'); ?></h2>
					<div class="hidden-sm visible-xs" style="margin-top: 20px"></div>
					<h3 class="hidden-xs">&nbsp;</h3>
					<?php echo $__env->yieldContent('acoes'); ?>
				</div>
			</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('aftercontent'); ?>
			<!-- Footer -->
			<?php echo $__env->make('layouts.themes.app.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		</div>
		<!-- End page wrapper-->

		
	</div>
	<!-- End wrapper-->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
	<?php $notifications = session('notifications'); ?>
	<?php if(!empty($notifications)): ?>;
		<script type="text/javascript">
			<?php $__currentLoopData = $notifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tipo => $mensagens): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php $__currentLoopData = (array) $mensagens; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mensagem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					toastr.<?php echo e($tipo); ?>('<?php echo e($mensagem); ?>');
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</script>
	<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.blank', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>